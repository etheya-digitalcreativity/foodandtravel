$(document).ready(function() {    
	$("#member_sub_number").keyup(function () {
		var value = $(this).val();
		if(value == ""){
			$("#group_id").val(8);
		} else {
			$("#group_id").val(9);
		}
	}).keyup();  

	$("#regsubmit").click(function() {
        var value = $("#member_sub_number").val();
  		if(value != "" ) {
	        if(value.length != 10){
			 	$.fancybox(
					'<section id="subpop"><h2>Wrong Subscription Number</h2><p>Unfortunately the subscription number you have entered has not been recognised or is wrong.</p><p>If you cant find it, please call 0207 501 0519 or email <a href="web@foodandtravel.com">web@foodandtravel.com</a>.</p><p>Our subscriber team will be happy to help you.</p><section>',
					{					
						'autoDimensions':	false, 		
						'transitionIn'	:	'fade',
						'transitionOut'	:	'fade',
						'speedIn'		:	600, 
						'speedOut'		:	600,
						'width'			: 	320,
						'height'		:	180,
						'overlayColor'	:	'#000',
						'overlayOpacity':	0.5,
						'padding'		: 	30
					}
				);
	            return false;
	        }
  		}

        return true;         
     });
  
 });
$(document).ready(function() {
	$("a[data-rel=example_group]")
	.attr('rel', 'gallery')
	.fancybox({
		beforeShow: function () {
            /* Disable right click */
            $.fancybox.wrap.bind("contextmenu", function (e) {
                    return false;
            });
        },
		'transitionIn'	:	'elastic',
		'transitionOut'	:	'elastic',
		'speedIn'		:	600,
		'speedOut'		:	300,
		'overlayShow'	:	false
	});
});



jQuery(document).ready(function() {
		
	
	$("a[rel=example_group]").fancybox({	
		'transitionIn'	:	'fade',
		'transitionOut'	:	'fade',
		'speedIn'		:	600, 
		'speedOut'		:	600,
		'autoDimensions':	true, 
		'overlayColor'	:	'#000',
		'overlayOpacity':	0.5,
		'padding'		: 	10
	});
	
});
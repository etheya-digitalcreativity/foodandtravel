jQuery(document).ready(function() {
		
	
	$("a.various").fancybox({	
		'hideOnContentClick': false,
		'transitionIn'	:	'fade',
		'transitionOut'	:	'fade',
		'speedIn'		:	600, 
		'speedOut'		:	600,
		'autoDimensions':	true, 
		'overlayColor'	:	'#000',
		'overlayOpacity':	0.5,
		'padding'		: 	10
	});
	
});
$(document).ready(function() {
	$('#thankyou').hide();
	$('#voting').hide();
	$(".buttonvote").click(function(e){
		e.preventDefault();
		$('#voting').show();
		var Poll = $("form#new_poll").not('.processed').first();
		if ( Poll.length > 0 ) {
			$.ajax({
				type:'POST',
				url:'/index.php',
				async:false,
				data:Poll.serialize(),
				success:function(){
				Poll.addClass('processed');
				$(".buttonvote").click();
			}});
		} else {
			$('.aw').hide();
			$('#thankyou').show();
			$('html, body').animate({scrollTop: '0px'}, 800);
		}
	});
});
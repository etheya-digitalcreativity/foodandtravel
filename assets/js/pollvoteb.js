$(document).ready(function() {
	$('#thankyou').hide();
	$('#voting').hide();
	$(".buttonvote").click(function(e){
		e.preventDefault();
		$('#voting').show();
		// Grab the first one, which does not have the 'processed' class
		var Poll = $("form#new_poll").not('.processed').first();
		// Are there any forms left?
		if ( Poll.length > 0 ) {
			// Lets execute the AJAX then
			$.ajax({
				type:'POST',
				url:'/votetest.php',
				async:false,
				data:Poll.serialize(),
				success:function(){
				// Mark it as processed
				Poll.addClass('processed');
				// Now lets click on the vote button again! To start over
				$(".buttonvote").click();
			}});
		} else {
			$("form.vote").each(function(){
			$.ajax({
				type: 'POST',
				url: $(this).attr("action"),
				data: $(this).serialize()
			})
			//alert('finished submitting');
			})



			// Since there are no polls to process, lets hide our indicator
			$('#voting').hide();
			$('.aw').hide();
			$('#thankyou').show();
			$('html, body').animate({scrollTop: '0px'}, 800);
		}
	});
});


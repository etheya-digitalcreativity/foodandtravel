		$(document).ready(function() {
		
	    	$('#page-rotator').cycle({		
		    delay:         100,      // additional delay (in ms) for first transition (hint: can be negative) 
	    	fx:           'fade',  // name of transition function 
		    height:       'auto',  // container height 
		    metaAttr:     'cycle', // data- attribute that holds the option data for the slideshow 
		    pause:         0,      // true to enable "pause on hover"     	
		    timeout:       4000,   // milliseconds between slide transitions (0 to disable auto advance) 
	    	speed:         1500,   // speed of the transition (any valid fx speed value) 
		    sync:          1,      // true if in/out transitions should occur simultaneously 	    
			after:     	function() {
	            		$('#banner').html(this.alt);
	        			}	    	    
		    });
		    
			$('#award-rotator').cycle({		
		    delay:         100,      // additional delay (in ms) for first transition (hint: can be negative) 
	    	fx:           'fade',  // name of transition function 
		    height:       'auto',  // container height 
		    metaAttr:     'cycle', // data- attribute that holds the option data for the slideshow 
		    pause:         0,      // true to enable "pause on hover"     	
		    timeout:       4000,   // milliseconds between slide transitions (0 to disable auto advance) 
	    	speed:         1500,   // speed of the transition (any valid fx speed value) 
		    sync:          1,      // true if in/out transitions should occur simultaneously 	    
			after:     	function() {
	            		$('#banner').html(this.alt);
	        			}	    	    
		    });		    
		    
		    
		    $('#food-rotator').cycle({
		
		    delay:         100,      
	    	fx:           'fade',  
		    height:       'auto',  
		    metaAttr:     'cycle', 
		    pause:         0,      
		    timeout:       4000,   
	    	speed:         1500,   
		    sync:          1,      
			after:     	function() {
	            		$('#banner').html(this.alt);
	        			}	    	    
		    });
		    
		    $('#gt-rotator').cycle({	
		    delay:         100,     
	    	fx:           'fade',  
		    height:       'auto',  
		    metaAttr:     'cycle', 
		    pause:         0,      
		    timeout:       4000,   
	    	speed:         1500,   
		    sync:          1,      
			after:     	function() {
	            		$('#banner').html(this.alt);
	        			}	    	    
		    });
		    
		    $('#news-rotator').cycle({
			
		    delay:         100,     
	    	fx:           'fade',  
		    height:       'auto',  
		    metaAttr:     'cycle', 
		    pause:         0,      
		    timeout:       4000,   
	    	speed:         1500,   
		    sync:          1,      
			after:     	function() {
	            		$('#banner').html(this.alt);
	        			}	    	    
		    });
 
		    
		});

		$(document).ready(function() {		
		
		// Hide subscriber fields
			$('#sub_num').hide();
	
			// When select list changes, do something
			$('#group_id').change(function() {
				$('#group_id').click(function() {				
					// Get value of select list	
			        var str = "";
			        $("#group_id option:selected").each(function () {
			                str += $(this).text();
			        });
				
					// Depending on value show/hide subscriber number fields
					if(str == 'Registered'){
						$('#sub_num').hide();
					} else if(str == 'Magazine Subscriber')  {
						$('#sub_num').show();
					}		         
				});
			});
		
		
		
			$("#subForm").validate();
		});
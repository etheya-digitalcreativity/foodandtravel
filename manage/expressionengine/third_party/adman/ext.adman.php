<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adman_ext {

	var $name = 'Adman';
	var $version = '1.0.7';
	var $settings_exist = 'y';
	var $docs_url = '';

	/**
	 * Extension Constructor
	 */
	function Adman_ext()
	{
		$this->EE =& get_instance();
	
	}
	
	
	// --------------------------------------------------------------------

	/**
	 * Activate Extension
	 */
	function activate_extension()
	{
		$data = array(
			'class' => 'Adman_ext',
			'hook'     => '',
			'method'   => '',
			'priority' => 10,
			'version'  => $this->version,
			'settings' => serialize($this->settings()),
			'enabled'  => 'y'
		);
		
		$this->EE->db->insert('extensions', $data);
		
		$data = array(
			'class'		=> 'Adman_mcp',
			'method'	=> 'log_click'
		);
		
		
		$this->EE->db->insert('actions', $data);
	}
	
	
	/**
	 * Update Extension
	 */
	function update_extension($current = FALSE)
	{
		if (! $current || $current == $this->version)
		{
			return FALSE;
		}

		$this->EE->db->where('class', 'Adman_mcp');
		$this->EE->db->update('extensions', array('version' => $this->version));
		
	}

	/**
	 * Disable Extension
	 */
	function disable_extension()
	{
		$this->EE->db->query('DELETE FROM exp_extensions WHERE class = "Adman_ext"');
		$this->EE->db->query('DELETE FROM exp_actions WHERE class = "Adman_mcp"');
	}
	
	// --------------------------------
	//  Settings
	// --------------------------------  

	function settings()
	{
		$settings = array();
		
		$settings['adman_currency']    = "";
		$settings['adman_path']    = $_SERVER['DOCUMENT_ROOT'] . "/ads/";
		$settings['adman_url']    = "http://" . $_SERVER['HTTP_HOST'] . "/ads/";
		$settings['adman_email_subject'] = 'Here is the stats report on your ads for {client_name}';
		$settings['adman_email_body'] = '<p>Dear Client,</p>

<p>Please find below the stats for all of your ads as of {current_time format="%j%S %F %Y"}.</p>

{ads}
<strong>{ad_title}</strong>
<table width="100%" cellspacing="1" cellpadding="10" border="0" valign="top" align="center" style="font-size:12px; font-family:arial, sans-serif; margin-bottom: 40px; margin-top: 20px; color:#3e4c5e;">


<thead valign="top" style="color:#ffffff; font-weight: normal;" >
<tr bgcolor="#566671" >
{stats}
    <th colspan="2" align="center">{date_month} {dte_yr}</th>
{/stats}
</tr>
<tr bgcolor="#a2b1ba">
{stats}
    <th width="100">Impressions</th>
    <th width="100">Clicks</th>
{/stats}
</tr>
</thead>

<tr align="center" bgcolor="#f1f4f7">
   {stats} 
<td align="center">{impression}</td>
    <td align="center">{click}</td>
    {/stats}
</tr>
</table>
{/ads}

<p>Thank you for advertising with {site_name} - we really appreciate your business!</p>';
		
		return $settings;
		
		
	}
	// END
	
	function settings_form()
	{
		
		$settings = $this->_get_settings();
		
		$adman_currency = (isset($settings['adman_currency'])) ? $settings['adman_currency'] : '';
		$adman_path = (isset($settings['adman_path'])) ? $settings['adman_path'] : ''; 
		$adman_url = (isset($settings['adman_url'])) ? $settings['adman_url'] : '';
		
		$adman_usefm = (isset($settings['adman_usefm'])) ? $settings['adman_usefm'] : '';
		$adman_email_subject = (isset($settings['adman_email_subject'])) ? $settings['adman_email_subject'] : '';
		$adman_email_body = (isset($settings['adman_email_body'])) ? $settings['adman_email_body'] : '';
		
		$form_settings['adman_currency'] = form_input('adman_currency', $adman_currency);
		$form_settings['adman_path'] = form_input('adman_path',$adman_path);
		$form_settings['adman_url'] = form_input('adman_url',$adman_url);
		$form_settings['adman_usefm'] = form_input('adman_usefm',$adman_usefm);
		$form_settings['adman_email_subject'] = form_input('adman_email_subject',$adman_email_subject);
		$form_settings['adman_email_body'] = form_textarea('adman_email_body',$adman_email_body);
		
		$ret = $this->EE->load->view('settings_form',$form_settings,TRUE);
		
		return $ret;
	}
	
	function save_settings()
	{
	unset($_POST['submit']);
	
	$this->EE->db->where('class', 'Adman_ext');
		$this->EE->db->update('extensions', array('settings' => serialize($_POST)));
		
		
		$this->EE->session->set_flashdata('message_success', 'Settings Saved');
	}
	
	function _get_settings()
	{
	$this->EE->db->select('settings')
						->from('extensions')
						->where('class','Adman_ext');
	
	$data = $this->EE->db->get()->row();
	
	$data = $data->settings;
	
	$settings = unserialize($data);
	
	$settings['adman_currency'] = ($this->EE->config->item('adman_currency')) ? $this->EE->config->item('adman_currency') : $settings['adman_currency'];
	
	$settings['adman_path'] = ($this->EE->config->item('adman_path')) ? $this->EE->config->item('adman_path') : $settings['adman_path']; 
	$settings['adman_url'] = ($this->EE->config->item('adman_url')) ? $this->EE->config->item('adman_url') : $settings['adman_url'];
	
	return $settings;
						
	}
	
}
// END CLASS
?>
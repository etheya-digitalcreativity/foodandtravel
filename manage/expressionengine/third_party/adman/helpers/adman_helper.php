<?php

	function _read_dir($directory){
			$arr = array();
			if(file_exists($directory)){
				$dir = opendir($directory);
				while(false != ($file = readdir($dir))){
					if(($file != ".") and ($file != "..")){
						$arr[] = $file;
					}
				}
			}
			return $arr;
		}

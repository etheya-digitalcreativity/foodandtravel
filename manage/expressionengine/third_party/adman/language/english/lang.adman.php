<?php

$lang = array(
"adman_module_name" 				=> 'Adman',
"adman_module_description" 			=> 'A Complete Ad Management Utility Enabling Banner Ad display and Tracking on your site',
"adman_currency"					=> 'Currency Denomination used in CPC representation<br /><em>(for example: $/&pound;/&euro;)</em>',
"adman_path"						=> 'Path to Image Banner-Ads',
"adman_url"							=> 'URL to Image Banner-Ads<br /><em>It is recommended that you create a fileupload location for your filemanager</em>',
"adman_usefm"						=> 'User File Manager for Banner Uploads (Enter y or n)<br /><em>(alternatively, you will need to upload banners manually)</em>',
"adman_email_subject"				=> "Client Email Report Subject<br /><em>This is for the email that gets sent to clients with the report data</em>",
"adman_email_body"					=> "Client Email Body<br /><em>This is the HTML body content of the email - it can contain parsable ExpressionEngine tags as well as the built-in tag pair</em>",

''=>'');

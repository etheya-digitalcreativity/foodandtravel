<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Adman : A Banner Ad Management Utility 
 *
 * @package		Adman
 * @subpackage	ThirdParty
 * @category	Modules
 * @author		Made By Hippo
 * @link		http://www.madebyhippo.com
 */
 
class Adman_mcp
{
	var $base;			// the base url for this module			
	var $form_base;		// base url for forms
	var $module_name = "adman";
	
	private $_base_url;
	
    var $EE;

	function Adman_mcp( $switch = TRUE )
	{
		// Make a local reference to the ExpressionEngine super object
		
		$this->EE =& get_instance();
		
		$this->theme_url = ($this->EE->config->item('url_third_themes')) ? $this->EE->config->item('url_third_themes').'/adman' : $this->EE->config->item('theme_folder_url').'third_party/adman';
		
	}

	function index() 
	{
		$this->EE->cp->add_to_head('<link rel="stylesheet" media="screen" href="'.$this->theme_url.'/css/forms.css" />');
		
		$this->EE->cp->add_to_foot('<script type="text/javascript" src="'.$this->theme_url.'/js/global.js"></script>');
		
		$this->EE->load->helper('adman');
		$this->EE->load->model('adman_core');
		
		$this->base_url = BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=adman';
		$this->file_manager = BASE.AMP.'D=cp'.AMP.'C=content_files';
		$this->base	 	 = BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->module_name;
		$this->form_base = 'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->module_name;
		$this->ext_settings = BASE.AMP.'C=addons_extensions'.AMP.'M=extension_settings'.AMP.'file='.$this->module_name;
		
		$vars = array(
			"total_ads" => $this->EE->adman_core->_totalads(),
			"active_ads" => $this->EE->adman_core->_totalactive(),
			"expired_ads" => $this->EE->adman_core->_totalexpired(),
			"scheduled_ads" => $this->EE->adman_core->_totalscheduled(),
			"site_id" => $this->EE->config->item('site_id'),
			"theme_url" => $this->theme_url
		);
		
		$settings = $this->EE->adman_core->_get_settings();
		
		$settings['base_url'] = $this->base_url;
		$settings['file_manager'] = $this->file_manager;
		
		if (!isset($settings['adman_path']))
		{
			return "<strong>Ahah! Not so fast feeble human.</strong><br /><br />You need to set all your settings first before any of this will make sense! The link is over on the top right hand side. Come back here once you've set everything.";
		}
		else{
			$files = $this->_read_dir($settings['adman_path']);
			
			$this->EE->cp->set_variable('cp_page_title', $this->EE->lang->line('adman_module_name'));
			
			$active_ads = $this->EE->adman_core->_list_active_ads();
			
			$output = $this->EE->load->view('cp_index',array(
				'settings' => $settings, 
				'vars' => $vars, 
				'groups' => $this->EE->adman_core->_list_groups(),
				'ads' => $active_ads,
				'exp_ads' => $this->EE->adman_core->_list_expired_ads(),
				'files' => $files,
				'top_ads' => $active_ads,
				'clients' => $this->EE->adman_core->_list_clients()
				),TRUE);
				
			return $output;
		}
	}
	
	function ad_view(){
		
		$this->EE->cp->add_to_head('<link rel="stylesheet" media="screen" href="'.$this->theme_url.'/css/forms.css" />');
		
		$this->EE->cp->add_to_head('<script type="text/javascript" src="'.$this->theme_url.'/js/jquery.tools.min.js"></script>');
		$this->EE->cp->add_to_head('<script type="text/javascript" src="'.$this->theme_url.'/js/global.js"></script>');
		
		$this->EE->load->helper('adman');
		$this->EE->load->model('adman_core');
		
		$this->base_url = BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=adman';
		$this->theme_url = $this->EE->config->item('theme_folder_url');
		
		$this->file_manager = BASE.AMP.'D=cp'.AMP.'C=content_files';
		
		//$this->EE->cp->add_to_head('<script type="text/javascript" src="'.$this->_theme('/script/br.js').'"></script>');
		
		$vars = array(
			"impressions" => $this->EE->adman_core->_totalimpressions($this->EE->input->get('ad_id')),
			"clicks" => $this->EE->adman_core->_totalclicks($this->EE->input->get('ad_id')),
			"total_cpc" => $this->EE->adman_core->_totalcpc($this->EE->input->get('ad_id')),
			"total_ctr" => number_format($this->EE->adman_core->_totalctr($this->EE->input->get('ad_id')),2),
			"site_id" => $this->EE->config->item('site_id'),
			"theme_url" => $this->theme_url
		);
		
		$settings = $this->EE->adman_core->_get_settings();
		
		$settings['base_url'] = $this->base_url;
		$settings['file_manager'] = $this->file_manager;
		
		$files = $this->_read_dir($settings['adman_path']);
		
		$settings['base_url'] = $this->base_url;
		
		$this->EE->cp->set_variable('cp_page_title', $this->EE->lang->line('adman_module_name').' - Ad Details');
		
		$output = $this->EE->load->view('ad_view',array(
			"group_info" => $this->EE->adman_core->_list_ad_group($this->EE->input->get('ad_id')),
			"ad_info" => $this->EE->adman_core->_get_ad($this->EE->input->get('ad_id')),
			'settings' => $settings, 
			'vars' => $vars, 
			'clients' => $this->EE->adman_core->_list_clients(),
			'groups' => $this->EE->adman_core->_list_groups(),
			'ads' => $this->EE->adman_core->_list_ads(),
			'files' => $files
		),TRUE);
		
		return $output;
	}
	
	function group_view(){
		
		$this->EE->cp->add_to_head('<link rel="stylesheet" media="screen" href="'.$this->theme_url.'/css/forms.css" />');
		
		$this->EE->cp->add_to_head('<script type="text/javascript" src="'.$this->theme_url.'/js/jquery.tools.min.js"></script>');
		$this->EE->cp->add_to_head('<script type="text/javascript" src="'.$this->theme_url.'/js/global.js"></script>');
		
		$this->EE->load->helper('adman');
		$this->EE->load->model('adman_core');
		
		$this->base_url = BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=adman';
		$this->theme_url = $this->EE->config->item('theme_folder_url');
		//$this->EE->cp->add_to_head('<script type="text/javascript" src="'.$this->_theme('/script/br.js').'"></script>');
		
		$settings = $this->EE->adman_core->_get_settings();
		$settings['base_url'] = $this->base_url;
		
		$vars = array(
			"site_id" => $this->EE->config->item('site_id'),
			"theme_url" => $this->theme_url
		);
		
		$this->EE->cp->set_variable('cp_page_title', $this->EE->lang->line('adman_module_name').' - Group Details');
		
		$output = $this->EE->load->view('group_view',array(
			"group_info" => $this->EE->adman_core->_get_group($this->EE->input->get('group_id')),
			'ads' => $this->EE->adman_core->_list_ads($this->EE->input->get('group_id')),
			'vars' => $vars,
			'settings' => $settings
		),TRUE);
		
		return $output;
	}
	
	function client_view() {
	
		if( ! class_exists('EE_Template')) 
        {
            $this->EE->load->library('template');
            $this->EE->TMPL = new EE_Template();
        }
        
		$this->EE->cp->add_to_head('<link rel="stylesheet" media="screen" href="'.$this->theme_url.'/css/forms.css" />');
		
		$this->EE->cp->add_to_head('<script type="text/javascript" src="'.$this->theme_url.'/js/jquery.tools.min.js"></script>');
		$this->EE->cp->add_to_head('<script type="text/javascript" src="'.$this->theme_url.'/js/global.js"></script>');
		
		$this->EE->load->helper('adman');
		$this->EE->load->model('adman_core');
		
		$this->base_url = BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=adman';
		$this->theme_url = $this->EE->config->item('theme_folder_url');
		//$this->EE->cp->add_to_head('<script type="text/javascript" src="'.$this->_theme('/script/br.js').'"></script>');
		
		$settings = $this->EE->adman_core->_get_settings();
		$settings['base_url'] = $this->base_url;
		
		$vars = array(
			"site_id" => $this->EE->config->item('site_id'),
			"theme_url" => $this->theme_url,
			"client_id" => $_GET["client_id"]
		);
		
		$this->EE->cp->set_variable('cp_page_title', $this->EE->lang->line('adman_module_name').' - Client Details');
		
		$ads = $this->EE->adman_core->_list_ads('',$this->EE->input->get('client_id'));
		$client = $this->EE->adman_core->_list_clients($this->EE->input->get('client_id'));
		
		$i=0;
		
		foreach($ads as $item)
		{
			$ads[$i]['stats']=$this->EE->adman_core->_get_stats($item['ad_id']);	
			
			$i++;
		}
		
		$this->EE->TMPL = new EE_Template;
		$this->EE->TMPL->parse_php = TRUE;
		
		$report[] = array(
			'client_name' => $client[0]['client_name'],
			'client_email' => $client[0]['client_email'],
			'ads' => $ads
		);
		
		$this->EE->TMPL->parse($settings["adman_email_subject"], FALSE, $this->EE->config->item('site_id'));
		
		$email_subject = $this->EE->TMPL->parse_globals($this->EE->TMPL->final_template);
		$email_subject = $this->EE->TMPL->parse_variables($email_subject,$report);
		
		$settings['adman_email_subject'] = $email_subject;
		
		$this->switch_cnt = 0;
		
		$this->EE->TMPL->parse($settings["adman_email_body"], FALSE, $this->EE->config->item('site_id'));
		$email_body = $this->EE->TMPL->parse_globals($this->EE->TMPL->final_template);
		$email_body = $this->EE->TMPL->parse_variables($email_body,$report);
		$email_body = preg_replace_callback('/'.LD.'ad_switch\s*=\s*([\'\"])([^\1]+)\1'.RD.'/sU', array(&$this, 'parse__switch'), $email_body);
		
		$settings['adman_email_body'] = $email_body;
		
		$output = $this->EE->load->view('client_view',array(
			"client_info" => $this->EE->adman_core->_list_clients($this->EE->input->get('client_id')),
			"ads" => $ads,
			'vars' => $vars,
			'settings' => $settings
		),TRUE);
		
		return $output;
	
	
	}
	
	function send_report()
	{
		$this->EE->load->library("email");
		
		$this->EE->email->EE_initialize();
		
		$this->EE->email->mailtype = 'html';
		
		$this->EE->email->from($this->EE->config->item('webmaster_email'));
		$this->EE->email->to($_POST['email_client']);
		$this->EE->email->subject($_POST['email_subject']);
		$this->EE->email->message($_POST['email_body']);
		
		$this->EE->email->Send();
	}
	
	
	function ad_delete() {
		$this->EE->db->where('ad_id',$this->EE->input->get('ad_id'));
		$this->EE->db->delete('adman_ads');
		
		$this->EE->db->where('ad_id',$this->EE->input->get('ad_id'));
		$this->EE->db->delete('adman_adgroups');
		
		$this->EE->db->where('ad_id',$this->EE->input->get('ad_id'));
		$this->EE->db->delete('adman_stats');
		
		$this->EE->session->set_flashdata('message_success',"Ad Deleted");
		
		$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=adman');
	}
	
	function ad_reset() {
		$this->EE->db->where('ad_id',$this->EE->input->get('ad_id'));
		$this->EE->db->delete('adman_stats');
		
		$this->EE->session->set_flashdata('message_success',"Ad Reset");
		
		$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=adman');
	}
	
	function group_delete() {
		$this->EE->db->where('group_id',$this->EE->input->get('group_id'));
		$this->EE->db->delete('adman_groups');
		
		$this->EE->db->where('group_id',$this->EE->input->get('group_id'));
		$this->EE->db->delete('adman_adgroups');
		
		$this->EE->session->set_flashdata('message_success',"Group Deleted");
		
		$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=adman');
	}
	
	function client_delete() {
		$this->EE->db->where('client_id',$this->EE->input->get('client_id'));
		$this->EE->db->delete('adman_clients');
		
		$this->EE->session->set_flashdata('message_success',"Client Deleted");
		
		$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=adman');
	}
	
	function log_click() {
		$ad_id = $this->EE->input->get('ad_id');
		
		$this->EE->db->query("update exp_adman_stats set click=click+1 where ad_id = ".$ad_id." and dte_mn = month(current_date()) and dte_yr = year(current_date())");
		
		//$this->EE->db->query("update exp_adman_ads set ad_clicks = ad_clicks+1 where ad_id=".$ad_id);
		
		$this->EE->db->select("ad_url");
		$this->EE->db->from("adman_ads");
		$this->EE->db->where("ad_id",$ad_id);
		
		$q = $this->EE->db->get();
		$row = $q->row();
		
		$this->EE->functions->redirect($row->ad_url);
	}
	
	function add_group(){
		// Check group_url exists first
		$this->EE->db->select('count(*) AS url_check');
		$this->EE->db->from('adman_groups');
		$this->EE->db->where('group_url',$_POST['group_url']);
		
		$q = $this->EE->db->get();
		$row = $q->row();
		
		if ($row->url_check!=0)
		{
			$this->EE->session->set_flashdata('message_success', "Sorry, You cannot use this Group URL - it may already exist");
		}
		else
		{
			$this->EE->db->insert('adman_groups',$_POST);
			$this->EE->session->set_flashdata('message_success',"Ad Group Added");
		}
		
		$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=adman');
	}
	
	function add_ad(){
		
		$adgroups = $_POST['ad_group'];
		
		unset($_POST['ad_group']);
		
		if ($_POST['ad_start']=="") {$_POST['ad_start']=date("Y-m-d");}
		
		//$autoexpire = strtotime ( '+1 year' , strtotime ( date("Y-m-d") ) ) ;
		
		$autoexpire = date('Y-m-d', time() + (365*24*60*60));
		
		if ($_POST['ad_end']=="") {$_POST['ad_end']=$autoexpire;}
		
		$this->EE->db->insert('adman_ads',$_POST);
		
		$ad_id = $this->EE->db->insert_id();
			
		foreach($adgroups as $group){
			$this->EE->db->insert('adman_adgroups',array(
				'group_id' => $group,
				'ad_id' => $ad_id
			));
		}
		
		$this->EE->session->set_flashdata('message_success',"Ad Added");
		
		$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=adman');	
	}
	
	function add_client(){
		$client = $_POST;
		
		$this->EE->db->insert("adman_clients",$client);
		
		$this->EE->session->set_flashdata('message_success',"Client Added");
		
		$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=adman');
	}
	
	function save_edit_group(){
	
		$this->EE->db->where('group_id', $_POST["group_id"]);
		$this->EE->db->update('adman_groups', $_POST);
		
		$this->EE->session->set_flashdata('message_success',"Group Saved");
		
		$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=adman');	
	}
	
	function save_edit_client(){
	
		$this->EE->db->where('client_id', $_POST["client_id"]);
		$this->EE->db->update('adman_clients', $_POST);
		
		$this->EE->session->set_flashdata('message_success',"Client Saved");
		
		$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=adman');	
	}
	
	function save_edit_ad(){
	
		if (isset($_POST['ad_group'])) {
			$adgroups = $_POST['ad_group'];
			unset($_POST['ad_group']);
			
			$this->EE->db->query('DELETE FROM exp_adman_adgroups WHERE ad_id ='.$_POST['ad_id']);
			
			foreach($adgroups as $group){
				$this->EE->db->insert('adman_adgroups',array(
					'group_id' => $group,
					'ad_id' => $_POST['ad_id']
				));
			}			
		}
		
		$this->EE->db->where('ad_id', $_POST["ad_id"]);
		$this->EE->db->update('adman_ads', $_POST);
		
		$this->EE->session->set_flashdata('message_success',"Ad Saved");
		
		$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=adman');	
	}
	
	function save_group_sort()
	{
		$ads = $_POST['sort_order'];
		
		foreach($ads as $item => $val){
			
			$this->EE->db->where('adgroup_id', $item);
			$this->EE->db->update('adman_adgroups', array(
					'sort_order' => $val
			));
		}
		
		$this->EE->session->set_flashdata('message_success',"Ad Sort Order Saved");
		
		$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=adman');
	}
	
	function upload_image()
	{
		$extension_whitelist = array("jpg", "gif", "png", "swf");
		
		$this->EE->load->model('adman_core');
		
		$settings = $this->EE->adman_core->_get_settings();
		
		$info = pathinfo($_FILES['ad_image']['name']);
		$ext = $info['extension'];
		
		if (in_array($ext,$extension_whitelist))
		{
		 $target = $settings['adman_path']; 
		 $target = $target . basename( $_FILES['ad_image']['name']) ; 
		 $ok=1; 
		 if(move_uploaded_file($_FILES['ad_image']['tmp_name'], $target)) 
		 {
		 // Success
			 $this->EE->session->set_flashdata('message_success',"Image Uploaded"); 
		 } 
		 else {
		 //Error saving file
		 $this->EE->session->set_flashdata('message_failure',"There was an error saving the file");
		 }	
		}
		else
		{
		//File not permitted
		 $this->EE->session->set_flashdata('message_failure',"This filetype is not allowed");
		}
		
		$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=adman');
	}
	
  	
  	function _read_dir($directory){
			$arr = array();
			
			$valid_ext=array("gif","jpg","jpeg","png","swf");
			
			if(file_exists($directory)){
				$dir = opendir($directory);
				while(false != ($file = readdir($dir))){
					if(($file != ".") and ($file != "..")){
						$ext = explode(".",$file);
						
						if (isset($ext[1])){
							if (in_array($ext[1],$valid_ext))
							{
							$arr[] = $file;
							}
						}
					}
				}
			}
			
			sort($arr);
			
			return $arr;
		}
	
	function Adman_cp_base(){
		return TRUE;
	}
	
	public function parse__switch($match)
		{
			$options = explode('|', $match[2]);
			$option = $this->switch_cnt % count($options);
			$this->switch_cnt++;
			return $options[$option];
		}
	
}

/* End of file mcp.adman.php */ 
/* Location: ./system/expressionengine/third_party/adman/mcp.adman.php */
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Adman : A Banner Ad Management Utility 
 *
 * @package		Adman
 * @subpackage	ThirdParty
 * @category	Modules
 * @author		Made By Hippo
 * @link		http://www.madebyhippo.com
 */
 
class Adman {

	function __construct()
	{
		$this->EE =& get_instance();
	}

	function Adman()
	{		
		$this->EE =& get_instance();
	}
		
	function show()
  	{
  	
  	$this->EE->load->model('adman_core');
  	
  	$settings = $this->EE->adman_core->_get_settings();
  	
  	$limit = $this->EE->TMPL->fetch_param('limit');
  	$group = $this->EE->TMPL->fetch_param('group');
  	$order = $this->EE->TMPL->fetch_param('order');
  	
  	$group = explode("|",$group);
  	
  	$this->EE->db->select("a.*,b.*,c.*");
  	$this->EE->db->from("adman_groups a");
  	$this->EE->db->join("adman_adgroups b","a.group_id = b.group_id");
  	$this->EE->db->join("adman_ads c","c.ad_id = b.ad_id");
  	foreach($group as $item)
  	{
  	$this->EE->db->or_where("a.group_url",$item);
  	}
  	$this->EE->db->where("now() between c.ad_start and c.ad_end");
  	
  	if ($order=="RANDOM")
  	{
  	$this->EE->db->order_by("RAND()");
  	}
  	
  	if ($order=="SORT_ORDER")
  	{
  	$this->EE->db->order_by("b.sort_order","asc");
  	}
  	
  	
  	if ($limit!="")
  	{
  	$this->EE->db->limit($limit);
  	}
  	
  	$q = $this->EE->db->get();
  	
  	$rows = $q->result_array();
  	
  	if (empty($rows)) { 
		return $this->EE->TMPL->no_results(); 
	}
  	
  	$i = 0;
  	
  	foreach($rows as $item){
  		
  		$max_click = $item['ad_max_click'];
  		$max_imp = $item['ad_max_impression'];
  		
  		if ($max_click != 0)
  		{
  			$this->EE->db->select("sum(click) as clickcount")
  						->from("adman_stats")
  						->where("ad_id",$item['ad_id']);
  						
  			$q = $this->EE->db->get();
  			$row = $q->row();
  			
  			$clickcount = $row->clickcount;
  		
  		   if ($max_click == $clickcount)
  		   {
  		   unset($rows[$i]);
  		   }
  		}
  		elseif ($max_imp != 0)
  		{
  		   $this->EE->db->select("sum(impression) as imp_count")
  						->from("adman_stats")
  						->where("ad_id",$item['ad_id']);
  						
  			$q = $this->EE->db->get();
  			$row = $q->row();
  			
  			$imp_count = $row->imp_count;
  		   
  		   if ($max_imp == $imp_count)
  		   {
  		   unset($rows[$i]);
  		   }
  		}
  		
  	$i++;		
  	}
  	
  	$data = array();
  	
  	foreach($rows as $row)
  	{
  		$row['ad_url'] = $this->EE->functions->fetch_site_index(0,0).QUERY_MARKER.'ACT='.$this->get_aid('Adman_mcp', 'log_click').AMP.'ad_id='.$row['ad_id'];
  		
  		$ext = explode(".",$row['ad_image']);
						
		if (isset($ext[1])){
			if ($ext[1]=="swf")
			{
				$row['is_flash'] = 1;
			}
			else
			{
				$row['is_flash'] = 0;
			}
		}
  		
  		if ((!is_null($row['ad_image'])) && ($row['ad_image'] !=""))
  		{
  			$row['ad_image'] = $settings['adman_url'].$row['ad_image'];
  		}
  		
  		$data[] = $row;
  		
  		$this->_log_imp($row['ad_id']);
  	}
  	
  	$output = $this->EE->TMPL->parse_variables($this->EE->TMPL->tagdata,$data);
	
	$this->switchcount=0;
	$output = preg_replace_callback('/'.LD.'switch\s*=\s*([\'\"])([^\1]+)\1'.RD.'/sU', array(&$this, '_switch_explode'), $output);
	
	return $output;
  	
  	}
  	
  	public function _switch_explode($match)
	{
			$options = explode('|', $match[2]);
			$option = $this->switchcount % count($options);
			$this->switchcount++;
			return $options[$option];
	}
  	
  	function _log_imp($ad_id)
  	{
  		$chk = $this->EE->db->query("select * from exp_adman_stats where ad_id = ".$ad_id." and dte_mn = month(current_date()) and dte_yr = year(current_date())")->result_array();
  		
  		if (empty($chk))
  		{
	  		//Insert a new record
	  		$data = array(
	  			'dte_mn' => date('n'),
	  			'dte_yr' => date('Y'),
	  			'ad_id'	 => $ad_id,
	  			'impression' => 1,
	  			'click' => 0
	  		);
	  		
	  		$this->EE->db->insert('adman_stats',$data);
  		}
  		else
  		{
	  		//Update the existing record
	  		$this->EE->db->query("update exp_adman_stats set impression=impression+1 where ad_id = ".$ad_id." and dte_mn = month(current_date()) and dte_yr = year(current_date())");
  		}
  		
  	}
  	
  	function _get_settings(){
		$this->EE =& get_instance();
		
		$this->EE->db->select("settings");
		$this->EE->db->from("extensions");
		$this->EE->db->where("class","Adman_ext");
		
		$q = $this->EE->db->get();
		$row = $q->row();
		
		return strip_slashes(unserialize($row->settings));
	}
  	
  	function get_aid($class,$method){
		$this->EE->db->select('action_id')
				->from('actions')
				->where('class',$class)
				->where('method',$method);
		$query = $this->EE->db->get();
		if($query->num_rows() > 0){
		   $row = $query->row(); 
		   return $row->action_id;
		}
		return 0;
	}
	
	//function EE() {if(!isset($this->EE)){$this->EE =& get_instance();}return $this->EE;}
}

/* End of file mod.adman.php */ 
/* Location: ./system/expressionengine/third_party/adman/mod.adman.php */
<?php

class Adman_core extends CI_Model {

	function __construct(){
		parent::__construct();
		
	}
	
	function _list_groups(){
		$this->db->select("*");
		$this->db->from("adman_groups");
		
		$q = $this->db->get();
		
		$group_array = $q->result_array();	
		$i = 0;
		
		foreach($group_array as $group){
			
			$group_array[$i]["adcount"] = $this->_get_groupads($group['group_id']);
			$i++;
		}
		
		return $group_array;	
	}
	
	function _list_ad_group($ad_id=''){
	   $this->db->select("group_id");
	   $this->db->from("adman_adgroups");
	   $this->db->where('ad_id',$ad_id);
	  
	   $q = $this->db->get();
	  
	   $group_array = $q->result_array();
	   
	   $ret='';
	   
	   foreach($group_array as $val)
	   {
	   $ret .= $val['group_id'].'|';
	   }
	   
	   return explode("|",$ret);	
	}
	
	function _get_client_ads($clientid)
	{
		
		$ads = $this->db->query("SELECT ad_id,ad_title FROM exp_adman_ads WHERE client_id = ".$clientid)->result_array();
		
		$alltime = $this->db->query("SELECT act_ad_id, COUNT(act_impression) AS impressions, COUNT(act_click) AS clicks FROM exp_adman_activity 
WHERE act_ad_id IN (SELECT ad_id FROM exp_adman_ads WHERE client_id = ".$clientid.") GROUP BY act_ad_id ORDER BY act_ad_id")->result_array();

		$thismonth = $this->db->query("SELECT act_ad_id, COUNT(act_impression) AS impressions, COUNT(act_click) AS clicks FROM exp_adman_activity
WHERE act_ad_id IN (SELECT ad_id FROM exp_adman_ads WHERE client_id = ".$clientid.") AND YEAR(act_date) = YEAR(NOW()) AND MONTH(act_date) = MONTH(NOW()) GROUP BY act_ad_id ORDER BY act_ad_id")->result_array();

		$lastmonth = $this->db->query("SELECT act_ad_id, COUNT(act_impression) AS impressions, COUNT(act_click) AS clicks FROM exp_adman_activity WHERE YEAR(act_date) = YEAR(DATE_SUB(NOW(), INTERVAL -1 MONTH)) AND MONTH(act_date) = MONTH(DATE_SUB(NOW(), INTERVAL -1 MONTH)) GROUP BY act_ad_id ORDER BY act_ad_id")->result_array();
		
		$result = array(
			'ads'		=> $ads,
			'alltime' => $alltime,
			'thismonth' => $thismonth,
			'lastmonth' => $lastmonth
		);	
		
		return $result;
	}
	
	function _list_clients($client_id='')
	{
		$this->db->select("*");
		$this->db->from("adman_clients");
		
		if ($client_id!="")
		{
		$this->db->where("client_id",$client_id);
		}
		
		$q = $this->db->get();
		
		return $q->result_array();
		
	}
	
	function _get_cp_ads()
	{
		$query = $this->db->query("SELECT *, SUM(impression) AS impressions, SUM(click) AS clicks FROM exp_adman_stats a join exp_adman_ads b on a.ad_id = b.ad_id GROUP BY ad_id")->result_array();
		
		return $query;
		
	}
	
	function _list_active_ads()
	{
		
		$this->db->select("a.*,SUM(b.impression) as ad_impression, SUM(b.click) as ad_clicks")
					->from("adman_ads a")
					->join("adman_stats b","a.ad_id = b.ad_id","left")
					->where("a.ad_end > current_timestamp()")
					->group_by('ad_title')
					->order_by("ad_impression","DESC");
					
		return $this->db->get()->result_array();
	}
	
	function _get_stats($ad_id)
	{
		$this->db->select("*")
					->from("adman_stats")
					->where("ad_id",$ad_id)
					->order_by("dte_yr,dte_mn");
					
		$stats = $this->db->get()->result_array();
		
		$c=0;
		foreach($stats as $item)
		{
			$stats[$c]['date_month']=date('F',mktime(0,0,0,$item['dte_mn'],1));
			$c++;
		}
		
		return $stats;
	}
	
	function _list_expired_ads()
	{
		$this->db->select("*")
					->from("adman_ads a")
					->where("a.ad_end <= current_timestamp()");
					
		return $this->db->get()->result_array();
	}
	
	function _list_ads($group = '',$client='',$from='',$to=''){
		
		if (($group=='') && ($client=='')){
			$this->db->select("*");
			$this->db->from("adman_ads as A");
		}
		elseif (($group!='') && ($client=='')) {
			$this->db->select("*");
			$this->db->from("adman_adgroups as A");
			$this->db->join('adman_ads B','B.ad_id = A.ad_id');
			$this->db->where("A.group_id",$group);
			$this->db->order_by('A.sort_order');
		}
		elseif (($group=='') && ($client!='')) {
			$this->db->select("*");
			$this->db->from("adman_adgroups as A");
			$this->db->join('adman_ads B','B.ad_id = A.ad_id');
			$this->db->where("B.client_id",$client);
			$this->db->group_by('A.ad_id');
		}
				
		$q = $this->db->get();
		
		$ad_array = $q->result_array();	
		
		$i=0;
		
		foreach($ad_array as $ad)
		{
		$ad_array[$i]['ad_impression']=$this->_totalimpressions($ad['ad_id'],$ad['ad_start'],$ad['ad_end']);	
		$ad_array[$i]['ad_clicks']=$this->_totalclicks($ad['ad_id'],$ad['ad_start'],$ad['ad_end']);
		$ad_array[$i]['ad_ctr']=$this->_totalctr($ad['ad_id']);	
		
		//$today = time();
		//$difference = $ad['ad_end'] - $today;
		//
		//	
		//$ad_array[$i]['ad_daysleft'] = floor($difference/60/60/24);

		
		$i++;	
		}
		
		return $ad_array;	
	}
	
	function _get_groupads($group_id) {
		$this->db->select("count(*) as adcount");
		$this->db->from("adman_adgroups");
		$this->db->where("group_id",$group_id);
		
		$q = $this->db->get();
		$row = $q->row();
		
		return $row->adcount;
	}
	
	function _get_settings(){
		$this->db->select("settings");
		$this->db->from("extensions");
		$this->db->where("class","Adman_ext");
		
		$q = $this->db->get();
		$row = $q->row();
		
		$settings = strip_slashes(unserialize($row->settings));
		
		$settings['adman_currency'] = ($this->config->item('adman_currency')) ? $this->config->item('adman_currency') : $settings['adman_currency'];
	
	$settings['adman_path'] = ($this->config->item('adman_path')) ? $this->config->item('adman_path') : $settings['adman_path']; 
	$settings['adman_url'] = ($this->config->item('adman_url')) ? $this->config->item('adman_url') : $settings['adman_url'];
	
	
		return $settings;
	}
	
	function _totalads() {
		$this->db->select('count(*) as total_ads');
		$this->db->from('adman_ads');
		
		$q = $this->db->get();
		
		$row = $q->row(); 
		
		return $row->total_ads;		
	}
	
	function _totalactive() {
		$this->db->select('count(*) as active_ads');
		$this->db->from('adman_ads');
		$this->db->where('now() between ad_start and ad_end');
		
		$q = $this->db->get();
		
		$row = $q->row(); 
		
		return $row->active_ads;		
	}
	
	function _totalexpired() {
		$now = date("Y-m-d H:i:s");
		$this->db->select('count(*) as expired_ads');
		$this->db->from('adman_ads');
		$this->db->where('ad_end < "'.$now.'"');
		
		$q = $this->db->get();
		
		$row = $q->row(); 
		
		return $row->expired_ads;		
	}
	
	function _totalscheduled() {
		$this->db->select('count(*) as scheduled_ads');
		$this->db->from('adman_ads');
		$this->db->where('ad_start > now()');
		
		$q = $this->db->get();
		
		$row = $q->row(); 
		
		return $row->scheduled_ads;		
	}
	
	function _totalimpressions($ad_id='', $start='', $end='') {
		$this->db->select('SUM(impression) as impressions');
		$this->db->from('adman_stats');
		
		if ($ad_id!="")
		{
		$this->db->where('ad_id',$ad_id);
		}
		
		$q = $this->db->get();
		
		$row = $q->row(); 
		
		if ($row->impressions==""){
			return "0";
		}else{
			return $row->impressions;
		}		
	}
	
	function _get_ad($ad_id='') {
		$this->db->select("*");
		$this->db->from('adman_ads a');
		$this->db->where('a.ad_id',$ad_id);
		$this->db->join('adman_adgroups g','g.ad_id = a.ad_id');
		
		$q=$this->db->get();
		
		return $q->result_array();
	}
	
	function _get_group($group_id='') {
		$this->db->select("*");
		$this->db->from('adman_groups');
		$this->db->where('group_id',$group_id);
		
		$q=$this->db->get();
		
		return $q->result_array();
	}
	
	function _totalcpc($ad_id='') {
		
		$this->db->select('ad_start,ad_end,ad_id,ad_cpc as cpc');
		$this->db->from('adman_ads');
		
		if ($ad_id!="")
		{
		$this->db->where('ad_id',$ad_id);
		}
		
		$q = $this->db->get();
		
		$total_cpc=0;
		
		foreach ($q->result_array() as $row){
			$clicks = $this->_totalclicks($row['ad_id'],$row['ad_start'],$row['ad_end']);
			$total_cpc += ($row['cpc']*$clicks);
		}		
		
		return $total_cpc;		
	}
	
	function _totalclicks($ad_id='',$start='',$end='') {
		$this->db->select('SUM(click) as clicks');
		$this->db->from('adman_stats');
		
		if ($ad_id!="")
		{
		$this->db->where('ad_id',$ad_id);
		}
		
		$q = $this->db->get();
		
		$row = $q->row(); 
		
		if ($row->clicks==""){
			return "0";
		}else{
			return $row->clicks;
		}		
	}
	
	function _totalctr($ad_id=''){
		
		$totalclicks = $this->_totalclicks($ad_id);
		$totalimpressions = $this->_totalimpressions($ad_id);
		
		if($totalclicks=="0" OR $totalimpressions=="0"){
			return "0";
		}
		
		$ctr = ($totalclicks/$totalimpressions)*100;
		
		return $ctr;
	}

}
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Adman : A Banner Ad Management Utility 
 *
 * @package		Adman
 * @subpackage	ThirdParty
 * @category	Modules
 * @author		Made By Hippo
 * @link		http://www.madebyhippo.com
 */
 
class Adman_upd {
		
	var $version        = '1.0.7'; 
	var $module_name = "Adman";
	
    function Adman_upd( $switch = TRUE ) 
    { 
		// Make a local reference to the ExpressionEngine super object
		$this->EE =& get_instance();
    } 

    function install() 
	{				
						
		$data = array(
			'module_name' 	 => $this->module_name,
			'module_version' => $this->version,
			'has_cp_backend' => 'y',
			'has_publish_fields' => 'n'
		);

		$this->EE->db->insert('modules', $data);		
		
		$data = array(
			'class'		=> $this->module_name,
			'method'	=> 'log_click'
		);
		
		$this->EE->db->insert('actions', $data);
																									
	$sql[] = "DROP TABLE IF EXISTS exp_adman_ads;";
	$sql[] = "CREATE TABLE exp_adman_ads (
				  ad_id int(11) NOT NULL AUTO_INCREMENT,
				  ad_title varchar(256) NOT NULL,
				  ad_image varchar(255) NULL DEFAULT '',
				  ad_remote_image varchar(512) NULL DEFAULT '',
				  ad_url varchar(512) NOT NULL,
				  ad_alt varchar(128) NULL,
				  ad_text varchar(1024) NULL,
				  ad_start datetime NULL,
				  ad_end datetime NULL,
				  ad_width int(11) NULL DEFAULT 0,
				  ad_height int(11) NULL DEFAULT 0,
				  ad_max_click int(11) NULL DEFAULT 0,
				  ad_max_impression int(11) NULL DEFAULT 0,
				  client_id int(11) NULL DEFAULT '0',
				  ad_impression int(11) NOT NULL DEFAULT '0',
				  ad_clicks int(11) NOT NULL DEFAULT '0',
				  ad_cpc decimal(5,2) NOT NULL DEFAULT '000.00',
				  site_id int(11) NOT NULL DEFAULT '1',
				  PRIMARY KEY (ad_id)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
	
	$sql[] = "DROP TABLE IF EXISTS exp_adman_groups;";
	$sql[] = "CREATE TABLE exp_adman_groups (
				  group_id int(11) NOT NULL AUTO_INCREMENT,
				  group_title varchar(124) NOT NULL,
				  group_url varchar(512) NOT NULL,
				  site_id int(11) NOT NULL DEFAULT '1',
				  PRIMARY KEY (group_id)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
	
	$sql[] = "DROP TABLE IF EXISTS exp_adman_adgroups;";
	$sql[] = "CREATE TABLE exp_adman_adgroups (
				  adgroup_id int(11) NOT NULL AUTO_INCREMENT,
				  group_id int(11) NOT NULL,
				  ad_id int(11) NOT NULL,
				  sort_order int(11) NOT NULL DEFAULT 0,
				  PRIMARY KEY (adgroup_id)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8;";	
	
	$sql[] = "CREATE TABLE exp_adman_stats (
				  stat_id int(11) NOT NULL AUTO_INCREMENT,
				  dte_mn int(3) NULL,
				  dte_yr int(3) NULL,
				  ad_id int(4) NULL,
				  impression int(4) NULL,
				  click int(4) NULL,
				  PRIMARY KEY (stat_id)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8;";	
	
	$sql[] = "DROP TABLE IF EXISTS exp_adman_clients;";
	$sql[] = "CREATE TABLE exp_adman_clients (
				  client_id int(11) NOT NULL AUTO_INCREMENT,
				  client_name varchar(1024) NOT NULL,
				  client_email varchar(1024) NOT NULL,
				  site_id int(11) NOT NULL,
				  PRIMARY KEY (client_id)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
				
	
	foreach ($sql as $query)
		{
			$this->EE->db->query($query);
		}
		
		return TRUE;
				
	}

	
	/**
	 * Uninstall the module
	 */
	function uninstall() 
	{ 				
		
		$this->EE->db->select('module_id');
		$query = $this->EE->db->get_where('modules', array('module_name' => $this->module_name));
		
		$this->EE->db->where('module_id', $query->row('module_id'));
		$this->EE->db->delete('module_member_groups');
		
		$this->EE->db->where('module_name', $this->module_name);
		$this->EE->db->delete('modules');
		
		$this->EE->db->where('class', $this->module_name);
		$this->EE->db->delete('actions');
		
		$this->EE->db->where('class', $this->module_name.'_mcp');
		$this->EE->db->delete('actions');
										
		return TRUE;
	}
	
	function update($current = '')
	{
		$this->EE->load->dbforge();
		$this->EE->load->library('logger');
		
		$upd = array();
		
		//$ver = str_replace(".","",$this->version)*1;
		$ver = preg_replace('/^(\d+)\.(\d+)\.(\d+).*$/', '\1\2\3', $this->version) * 1;
		
		//$curr = (str_replace(".","",$current)*1);
		$curr = preg_replace('/^(\d+)\.(\d+)\.(\d+).*$/', '\1\2\3', $current) * 1;
		
		if($curr < $ver){
			
			$start = $curr + 1;
				
				for($i = $start; $i <= $ver; $i++){
					if ($i>=$ver)
					{
						$fl = PATH_THIRD.'adman'.DIRECTORY_SEPARATOR.'updates'.DIRECTORY_SEPARATOR.$i.'.php';
						
						if(file_exists($fl)){
							$this->EE->logger->log_action("Running update file: ".$fl);
							
							include_once($fl);
						}
					}
				}
		
				foreach ($upd as $query){
					$this->EE->db->query($query);
				}
			}
		return TRUE;
	}

    function EE() {if(!isset($this->EE)){$this->EE =& get_instance();}return $this->EE;}    
}

/* End of file upd.adman.php */ 
/* Location: ./system/expressionengine/third_party/adman/upd.adman.php */

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$upd[] = "CREATE TABLE exp_adman_activity (
				  act_id int(11) NOT NULL AUTO_INCREMENT,
				  act_click int(1) NULL,
				  act_impression int(1) NULL,
				  act_date datetime NULL,
				  act_ad_id int(3) NOT NULL,
				  site_id int(11) NOT NULL DEFAULT '1',
				  PRIMARY KEY (act_id)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8;";

$q = $this->EE->db->query("select ad_id,site_id,ad_impression, ad_clicks from exp_adman_ads");
$results = $q->result_array();

foreach($results as $entries)
{

	for($i=0;$i<$entries['ad_impression'];$i++)
	{
	$upd[] = "INSERT INTO exp_adman_activity 
				(act_click,act_impression,act_date,act_ad_id,site_id)
			  VALUES (0,1,CURRENT_TIMESTAMP(),".$entries['ad_id'].",".$entries['site_id'].");";
	}	
	
	for($i=0;$i<$entries['ad_clicks'];$i++)
	{
	
	$upd[] = "INSERT INTO exp_adman_activity 
				(act_click,act_impression,act_date,act_ad_id,site_id)
			  VALUES (1,0,CURRENT_TIMESTAMP(),".$entries['ad_id'].",".$entries['site_id'].");";
	
	}
}

$upd[] = "CREATE TABLE exp_adman_clients (
				  client_id int(11) NOT NULL AUTO_INCREMENT,
				  client_name varchar(1024) NOT NULL,
				  client_email varchar(1024) NOT NULL,
				  site_id int(11) NOT NULL,
				  PRIMARY KEY (client_id)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8;";


$upd[] = "ALTER TABLE exp_adman_ads ADD client_id int(11);";
$upd[] = "ALTER TABLE exp_adman_ads ADD ad_width int(11) NULL DEFAULT 0;";
$upd[] = "ALTER TABLE exp_adman_ads ADD ad_height int(11) NULL DEFAULT 0;";
$upd[] = "ALTER TABLE exp_adman_ads ADD ad_max_impression int(11) NULL DEFAULT 0;";
$upd[] = "ALTER TABLE exp_adman_ads ADD ad_max_click int(11) NULL DEFAULT 0;";
$upd[] = "ALTER TABLE exp_adman_ads ADD ad_remote_image VARCHAR(512) NULL DEFAULT '';";
$upd[] = "ALTER TABLE exp_adman_ads MODIFY ad_image VARCHAR(512) NULL DEFAULT '';";
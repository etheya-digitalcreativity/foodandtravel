<table width="100%">
	<tr>
		<td width="80%" valign="top">
			<?=form_open('&D=cp&C=addons_modules&M=show_module_cp&module=adman&method=save_edit_ad','class=frm_newad');?>
			<input type="hidden" name="site_id" value="<?= $vars['site_id']; ?>" />
            <input type="hidden" name="ad_id" value="<?= $ad_info[0]['ad_id']; ?>" />
			<table class="mainTable" width="100%">
				<thead>
				<tr>
					<th colspan="2">EDIT ADVERT</th>
				</tr>
				</thead>
				<tr>
					<th width="15%">Name :</th>
					<td><input type="text" name="ad_title" class="ad_title" value="<?= $ad_info[0]['ad_title']; ?>" /></td>
				</tr>
				<tr>
					<th>URL :</th>
					<td><input type="text" name="ad_url" class="ad_url" value="<?= $ad_info[0]['ad_url']; ?>" /></td>
				</tr>
				<tr>
					<th>Client :</th>
					<td>
					<?php if(empty($clients)){?>
						<em>You don't have any clients registered. Why not <strong><a href="">add one</a></strong>?</em>
					<?php } else { ?>
						<select name="client_id" size="1">
                        	<option value="">----</option>
                        	<? foreach($clients as $client){ ?>
                        	<option value="<?= $client['client_id'];?>" <?php if($ad_info[0]['client_id']==$client['client_id']){echo('selected="selected"');}?>><?= $client['client_name']; ?></option>
                        	<? } ?>
                        </select>
                    <?php } ?>
					</td>
				</tr>
				<tr>
					<th>CPC :</th>
					<td><input type="text" name="ad_cpc" value="<?= $ad_info[0]['ad_cpc']; ?>" /></td>
				</tr>
				<tr>
					<th>Ad Group(s) :</th>
					<td>
						<?
						if (empty($groups))
						{
						?>
						<em>You don't have any groups registered.</em>
						<?
						}
						else
						{
						?>
						<table width="100%" class="tblgroups">
                           <? $count=0;?>
                           <? foreach($groups as $gitems){ ?>
                           <? 
                           if ((($count%4)==0)&& $count!=0)
                           {
                           echo("</tr><tr>");
                           }
                           $count++;
                           ?>
                               <td><input type="checkbox" class="ad_group" name="ad_group[]" value="<?= $gitems['group_id'] ?>" <?php if(in_array($gitems['group_id'],$group_info)){echo('checked="checked"');}?> />&nbsp;: <?= $gitems['group_title']; ?></td>
                           
                           <? } ?>
                        </table>
                        <?
                        }
                        ?>
					</td>
				</tr>
				<tr>
					<th>Banner Image :</th>
					<td>
						<?php 
							if (empty($files))
							{
							?>
							<em>You don't have any images. Why not <strong><a href="">add one</a></strong>?</em>
							<?
							}
							else
							{
						?>
						<select name="ad_image" size="1">
                                    	<option value="">----</option>
                                    	<? foreach($files as $file){ ?>
                                    	<option <? if($file==$ad_info[0]['ad_image']){echo('selected="yes"');}?>><?= $file; ?></option>
                                    	<? } ?>
                                    </select>
                        <?
                        }
                        ?>
					</td>
				</tr>
				<tr>
					<th>Remote Image URL :</th>
					<td><input type="text" name="ad_remote_image" value="<?= $ad_info[0]['ad_remote_image']; ?>" /></td>
				</tr>
				<tr>
					<th>Dimensions :</th>
					<td><input type="text" name="ad_width" style="width:25px;"  value="<?= $ad_info[0]['ad_width']; ?>" /> (w) <input type="text" name="ad_height" style="width:25px;"  value="<?= $ad_info[0]['ad_height']; ?>" /> (h)</td>
				</tr>
				
				<tr>
					<th>Image ALT :</th>
					<td><input type="text" name="ad_alt" value="<?= $ad_info[0]['ad_alt']; ?>" /></td>
				</tr>
				<tr>
					<th>Ad Text :</th>
					<td><textarea name="ad_text"><?= $ad_info[0]['ad_text']; ?></textarea></td>
				</tr>
				<tr>
					<th>Ad Duration :</th>
					<td><input type="date" name="ad_start" value="<?= date('Y-m-d',strtotime($ad_info[0]['ad_start'])); ?>" /> - <input type="date" name="ad_end" value="<?= date('Y-m-d',strtotime($ad_info[0]['ad_end'])); ?>" /></td>
				</tr>
				<tr>
					<th>Max Clicks :</th>
					<td><input type="text" name="ad_max_click" value="<?= $ad_info[0]['ad_max_click']; ?>" style="width:25px;" />
				</tr>
				<tr>
					<th>Max Impressions :</th>
					<td><input type="text" name="ad_max_impression" value="<?= $ad_info[0]['ad_max_impression']; ?>" style="width:25px;" />
				</tr>
				
				<tr>
					<th>&nbsp;</th>
					<td><input type="submit" class="submit" value="Save" /></td>
				</tr>
			</table>
			</form>	
		</td>
		<td width="20%" valign="top">
			<table class="mainTable" width="100%">
				<thead>
				<tr>
					<th colspan="3">Navigation</th>
				</tr>
				</thead>
			</table>
			<table width="100%" style="margin-bottom:10px;">
				<tr>
					<td align="center"><a class="submit" href="<?= BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=adman' ?>">Home</a></td>
					<td align="center"><a class="submit" href="#"  onclick="if(confirm('Are you Sure?\nThis will remove the ad, the statistics and it\'s associated ad group entries')){location.href='<?= $settings['base_url'].AMP.'method=ad_delete'.AMP.'ad_id='.$ad_info[0]['ad_id']; ?>';}">Delete</a></td>
					<td align="center"><a class="submit" href="#"  onclick="if(confirm('Are you Sure?\nThis will remove all the ad statistics')){location.href='<?= $settings['base_url'].AMP.'method=ad_reset'.AMP.'ad_id='.$ad_info[0]['ad_id']; ?>';}">Reset</a></td>
				</tr>
			</table>
			<table class="mainTable" width="100%">
				<thead>
				<tr>
					<th colspan="2" align="center">Ad Statistics</th>
				</tr>
				</thead>
				<tr>
					<th width="50%">Impressions :</th>
					<td><?= $vars['impressions']; ?></td>
				</tr>
				<tr>
					<th>Clicks :</th>
					<td><?= $vars['clicks']; ?></td>
				</tr>
				<tr>
					<th>CPC :</th>
					<td><?= $settings['adman_currency']; ?><?= number_format($vars['total_cpc'],2); ?></td>
				</tr>
				<tr>
					<th>CTR :</th>
					<td><?= number_format($vars['total_ctr'],2); ?>%</td>
				</tr>				
			</table>
		</td>
	</tr>
</table>
<table width="100%">
	<tr>
		<td width="80%" valign="top">
			<div id="emailreport" style="display:none;">
			<?=form_open('&D=cp&C=addons_modules&M=show_module_cp&module=adman&method=send_report','class=email_report');?>
			<input type="hidden" name="email_client" value="<?= $client_info[0]['client_email'];?>" />
			<table class="mainTable">
				<thead>
				<tr>
					<td>Send Email Report</td>
				</tr>
				</thead>
				<tr>
					<td><input type="text" name="email_subject" value="<?= $settings['adman_email_subject']; ?>" /></td>
				</tr>
				<tr>
					<td><textarea name="email_body" rows="20"><?= $settings['adman_email_body']; ?></textarea></td>
				</tr>
				<tr>
					<td><input type="submit" class="submit" id="send_button" value="Send Email" /></td>
				</tr>
			</table>
			</form>
			</div>
			<?=form_open('&D=cp&C=addons_modules&M=show_module_cp&module=adman&method=save_edit_client','class=frm_newclient');?>
            <input type="hidden" name="client_id" value="<?= $vars['client_id']; ?>" />
            <table class="mainTable" width="100%">
				<thead>
				<tr>
					<th colspan="2">EDIT CLIENT</th>
				</tr>
				</thead>
				<tr>
					<th>Client Name :</th>
					<td><input type="text" name="client_name" id="client_name" value="<?= $client_info[0]['client_name']; ?>" /></td>
				</tr>
				<tr>
					<th>Client Email :</th>
					<td><input type="text" name="client_email" id="client_email" value="<?= $client_info[0]['client_email']; ?>" /></td>
				</tr>
				<tr>
					<th>&nbsp;</th>
					<td><input type="submit" class="submit" value="Save" /></td>
				</tr>
			</table>                               
            </form>
            <? $c = 0;?>
            <? foreach($ads as $adinfo){?>
             <table class="mainTable">
                                <thead>
                                   <tr>
                                   		<th rowspan="2"><? if ($c==0){ ?>Ads associated with this client<? } ?></th>
                                   		<? foreach($adinfo['stats'] as $stats){ ?>
                                   			<th colspan="2" width="28%"><?= $stats['date_month'];?> <?= $stats['dte_yr'];?></th>
                                   		<? } ?>
                                   </tr>
                                   <tr>
                                        <? foreach($adinfo['stats'] as $stats){ ?>
                                   		 <th width="12%">Impressions</th>
                                   		 <th width="12%">Clicks</th>
                                   		<? } ?>
                                       
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    <tr>
                                    	<td><a href="<?= $settings['base_url'].AMP.'method=ad_view'.AMP.'ad_id='.$adinfo['ad_id']; ?>"><?= $adinfo['ad_title']; ?></a></td>
                                    	<? foreach($adinfo['stats'] as $stats){ ?>
                                   		 <th width="12%"><?= $stats['impression']; ?></th>
                                   		 <th width="12%"><?= $stats['click']; ?></th>
                                   		<? } ?>
                                    	
                                    </tr>
                                                             
                                </tbody>
                            </table>
          <? $c++; ?>
           <? } ?> 
		</td>
		<td width="20%" valign="top">
			<table class="mainTable" width="100%">
				<thead>
				<tr>
					<th colspan="3">Navigation</th>
				</tr>
				</thead>
			</table>
			<table width="100%">
				<tr>
					<td align="center"><a class="submit" href="<?= BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=adman' ?>">Home</a></td>
					<td align="center"><a href="#emailreport" class="submit addnew">Email Report</a></td>
					<td align="center"><a class="submit" href="#" onclick="if(confirm('Are you Sure?\nThis will remove the client and it\'s associated ad entries (it will not remove the ads themselves)')){location.href='<?= $settings['base_url'].AMP.'method=client_delete'.AMP.'client_id='.$client_info[0]['client_id']; ?>';}">Delete</a></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
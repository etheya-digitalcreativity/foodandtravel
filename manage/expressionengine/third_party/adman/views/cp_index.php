<table width="100%">
	<tr>
		<td width="75%" valign="top">
			<div id="newad" style="display:none;">
			<?=form_open('&D=cp&C=addons_modules&M=show_module_cp&module=adman&method=add_ad','class=frm_newad');?>
			<table class="mainTable" width="100%">
				<thead>
				<tr>
					<th colspan="2">NEW ADVERTISER</th>
				</tr>
				</thead>
				<tr>
					<th width="15%">Name :</th>
					<td><input type="text" name="ad_title" class="ad_title" /></td>
				</tr>
				<tr>
					<th>URL :</th>
					<td><input type="text" name="ad_url" class="ad_url" /></td>
				</tr>
				<tr>
					<th>Client :</th>
					<td>
					<?php if(empty($clients)){?>
						<em>You don't have any clients registered.</em>
					<?php } else { ?>
						<select name="client_id" size="1">
                        	<option value="">----</option>
                        	<? foreach($clients as $client){ ?>
                        	<option value="<?= $client['client_id'];?>"><?= $client['client_name']; ?></option>
                        	<? } ?>
                        </select>
                    <?php } ?>
					</td>
				</tr>
				<tr>
					<th>CPC :</th>
					<td><input type="text" name="ad_cpc" /></td>
				</tr>
				<tr>
					<th>Ad Group(s) :</th>
					<td>
						<?
						if (empty($groups))
						{
						?>
						<em>You don't have any groups registered.<br /><strong>You will need to add at least one group before you can add an ad</strong></em>
						<?
						}
						else
						{
						?>
						<table width="100%" class="tblgroups">
                           <? $count=0;?>
                           <? foreach($groups as $gitems){ ?>
                           <? 
                           if ((($count%4)==0)&& $count!=0)
                           {
                           echo("</tr><tr>");
                           }
                           $count++;
                           ?>
                               <td <? if ($count < 5){ ?>style="border-top:solid 1px #D0D7DF;" <? } ?>><input type="checkbox" name="ad_group[]" value="<?= $gitems['group_id'] ?>" class="ad_group" style="width:25px;margin:0;" />: <?= $gitems['group_title']; ?></td>
                           
                           <? } ?>
                        </table>
                        <?
                        }
                        ?>
					</td>
				</tr>
				<tr>
					<th>Banner Image :</th>
					<td>
						<?php 
							if (empty($files))
							{
							?>
							You don't have any images. <a href="#newimage" class="addnew">Add New Image</a>
							<?
							}
							else
							{
						?>
						<select name="ad_image" size="1">
                                    	<option value="">----</option>
                                    	<? foreach($files as $file){ ?>
                                    	<option><?= $file; ?></option>
                                    	<? } ?>
                                    </select>
                        <?
                        }
                        ?>
					</td>
				</tr>
				<tr>
					<th>Remote Image URL :</th>
					<td><input type="text" name="ad_remote_image" /></td>
				</tr>
				<tr>
					<th>Dimensions :</th>
					<td><input type="text" name="ad_width" style="width:25px;" /> (w) <input type="text" name="ad_height" style="width:25px;" /> (h)</td>
				</tr>
				<tr>
					<th>Image ALT :</th>
					<td><input type="text" name="ad_alt" /></td>
				</tr>
				<tr>
					<th>Ad Text :</th>
					<td><textarea name="ad_text"></textarea></td>
				</tr>
				<tr>
					<th>Ad Duration :</th>
					<td><input type="date" name="ad_start" class="dte" /> - <input type="date" name="ad_end" class="dte" /></td>
				</tr>
				<tr>
					<th>Max Clicks :</th>
					<td><input type="text" name="ad_max_click" value="" style="width:25px;" />
				</tr>
				<tr>
					<th>Max Impressions :</th>
					<td><input type="text" name="ad_max_impression" value="" style="width:25px;" />
				</tr>
				<?php
				if (!empty($groups))
				{
				?>
				<tr>
					<th>&nbsp;</th>
					<td><input type="submit" class="submit" value="Save" />&nbsp;or <a href="#newad" class="addnew">Close</a></td>
				</tr>
				<?php
				} else { ?>
				<tr>
					<th>&nbsp;</th>
					<td>You need to <a href="#newgroup" class="addnew">add a group</a> before you can create an advert</td>
				</tr>
				<?php
				}
				?>
			</table>
			</form>
		</div>
		<div id="newgroup" style="display:none;">	
			<?=form_open('&D=cp&C=addons_modules&M=show_module_cp&module=adman&method=add_group','class=frm_newgroup');?>
            <input type="hidden" name="site_id" value="<?= $vars['site_id']; ?>" />
			<table class="mainTable" width="100%">
				<thead>
				<tr>
					<th colspan="2">NEW GROUP</th>
				</tr>
				</thead>
				<tr>
					<th width="15%">Group Name :</th>
					<td><input type="text" name="group_title" id="group_title" /></td>
				</tr>
				<tr>
					<th>Group URL :</th>
					<td><input type="text" name="group_url" id="group_url" /></td>
				</tr>
				<tr>
					<th>&nbsp;</th>
					<td><input type="submit" class="submit" value="Save" />&nbsp;or <a href="#newgroup" class="addnew">Close</a></td>
				</tr>
			</table>
			</form>
		</div>
		<div id="newclient" style="display:none;">	
			 <?=form_open('&D=cp&C=addons_modules&M=show_module_cp&module=adman&method=add_client','class=frm_newclient');?>
            <input type="hidden" name="site_id" value="<?= $vars['site_id']; ?>" />
			<table class="mainTable" width="100%">
				<thead>
				<tr>
					<th colspan="2">NEW CLIENT</th>
				</tr>
				</thead>
				<tr>
					<th width="15%">Client Name :</th>
					<td><input type="text" name="client_name" id="client_name" /></td>
				</tr>
				<tr>
					<th>Client Email :</th>
					<td><input type="text" name="client_email" id="client_email" /></td>
				</tr>
				<tr>
					<th>&nbsp;</th>
					<td><input type="submit" class="submit" value="Save" />&nbsp;or <a href="#newclient" class="addnew">Close</a></td>
				</tr>
			</table>
			</form>
		</div>
			<table class="mainTable" width="100%">
				<thead>
				<tr>
					<th colspan="5">ACTIVE/SCHEDULES ADVERTISERS  (<?= count($ads); ?>)</th>
				</tr>
				</thead>
				<tbody>
				<tr>
					<th>Client</th>
                    <th width="10%">Impressions</th>
                    <th width="10%">Clicks</th>
                    <th width="10%">Status</th>
                </tr>
                <? 
                if (empty($ads))
                {
                ?>
                <tr>
                	<td colspan="5">You do not have any ads registered. Why not <a href="#newad" class="addnew">add one</a>?</td>
                </tr>
                <?
                }
                ?>
                <? 
                $cnt_imp = 0;
                $cnt_clk = 0;
                
                foreach($ads as $item) { ?>
                                    <tr>
                                        <td><a href="<?= $settings['base_url'].AMP.'method=ad_view'.AMP.'ad_id='.$item['ad_id']; ?>"><?= $item['ad_title']; ?></a></td>
                                        <td><?= $item['ad_impression']; ?> <?php if (($item['ad_max_impression']!="") && ($item['ad_max_impression']!=0)) { ?>/ <?= $item['ad_max_impression']; ?><?php } ?></td>
                                        <td><?= $item['ad_clicks']; ?> <?php if (($item['ad_max_click']!="") && ($item['ad_max_click']!=0)) { ?>/ <?= $item['ad_max_click']; ?><?php } ?></td>
                                        <?
                                        
                                        $today = date("Y-m-d H:i:s");
                                        $start = date("Y-m-d H:i:s",strtotime($item['ad_start']));
                                        $end = date("Y-m-d H:i:s",strtotime($item['ad_end']));
                                        
                                        //$diff = date("d",strtotime($item['ad_end'])-strtotime($item['ad_start']));
                                        
                                        if ($end < $today) {
                                        	$status = "<span style='color:#900;'>Expired</span>";
                                        }
                                        elseif ($start > $today) {
                                        	$status = "<span style='color:#009;'>Scheduled</span>";
                                        }
                                        else
                                        {
                                        	$status = "<span style='color:#090;'>Active</span>";
                                        }
                                        
                                        
                                        if (($item['ad_impression']==$item['ad_max_impression'])&&($item['ad_max_impression']!=0))
                                        {
                                        	$status = "<span style='color:#900;'>Max Impressions Reached</span>";
                                        }
                                        
                                        if (($item['ad_clicks']==$item['ad_max_click'])&&($item['ad_max_click']!=0))
                                        {
                                        	$status = "<span style='color:#900;'>Max Clicks Reached</span>";
                                        }
                                        
                                        ?>
                                        <td><?= $status; ?></td>
                                    </tr>
                                    <?
                                    $cnt_imp += $item['ad_impression'];
                                    $cnt_clk += $item['ad_clicks'];
                                    ?>
                                    <? } ?>
                 </tbody>
                 <tfoot>
                 	<tr>
                 		<th align="right">TOTALS</th>
                 		<th><?= $cnt_imp; ?></th>
                 		<th><?= $cnt_clk; ?></th>
                 		<th>&nbsp;</th>
                 	</tr>
                 </tfoot>
			</table>
			
			<table class="mainTable" width="100%">
				<thead>
				<tr>
					<th colspan="5">EXPIRED ADVERTISERS (<?= count($exp_ads); ?>)</th>
				</tr>
				</thead>
				<tbody>
				<tr>
					<th>Client</th>
					<th width="10%">Status</th>
                </tr>
                <? 
                if (empty($exp_ads))
                {
                ?>
                <tr>
                	<td colspan="5">You do not have any Expired Ads</td>
                </tr>
                <?
                }
                ?>
                <? 
                
                foreach($exp_ads as $item) { ?>
                                    <tr>
                                        <td><a href="<?= $settings['base_url'].AMP.'method=ad_view'.AMP.'ad_id='.$item['ad_id']; ?>"><?= $item['ad_title']; ?></a></td>
                                        <?
                                        
                                        $today = date("Y-m-d H:i:s");
                                        $start = date("Y-m-d H:i:s",strtotime($item['ad_start']));
                                        $end = date("Y-m-d H:i:s",strtotime($item['ad_end']));
                                        
                                        //$diff = date("d",strtotime($item['ad_end'])-strtotime($item['ad_start']));
                                        
                                        if ($end < $today) {
                                        	$status = "<span style='color:#900;'>Expired</span>";
                                        }
                                        elseif ($start > $today) {
                                        	$status = "<span style='color:#009;'>Scheduled</span>";
                                        }
                                        else
                                        {
                                        	$status = "<span style='color:#090;'>Active</span>";
                                        }
                                        
                                        
                                        if (($item['ad_impression']==$item['ad_max_impression'])&&($item['ad_max_impression']!=0))
                                        {
                                        	$status = "<span style='color:#900;'>Max Impressions Reached</span>";
                                        }
                                        
                                        if (($item['ad_clicks']==$item['ad_max_click'])&&($item['ad_max_click']!=0))
                                        {
                                        	$status = "<span style='color:#900;'>Max Clicks Reached</span>";
                                        }
                                        
                                        ?>
                                        <td><?= $status; ?></td>
                                    </tr>
                                    
                                    <? } ?>
                 </tbody>
			</table>
			
			<table class="mainTable" width="100%">
				<thead>
				<tr>
					<th colspan="3">GROUPS</th>
				</tr>
				</thead>
				<tr>
					<th>Title</th>
					<th width="30%">Group URL</th>
					<th width="10%">Ad Count</th>
				</tr>
				<? 
                if (empty($groups))
                {
                ?>
                <tr>
                	<td colspan="5">You do not have any groups registered. Why not <a href="#newgroup" class="addnew">add one</a>?</td>
                </tr>
                <?
                }
                ?>
				<? foreach($groups as $gitems){ ?>
                                    <tr>
                                        <td><a href="<?= $settings['base_url'].AMP.'method=group_view'.AMP.'group_id='.$gitems['group_id']; ?>"><?= $gitems['group_title'] ?></a></td>
                                        <td><?= $gitems['group_url'] ?></td>
                                        <td><?= $gitems['adcount'] ?></td>
                                    </tr>
                                    <? } ?>
			</table>
			
			<table width="100%" class="mainTable">	
			<thead>
			<tr>
				<th colspan="3">CLIENTS</th>	
			</tr>
			</thead>
			<tr>
				<th>Client Name</th>
				<th width="40%">Client Email</th>
			</tr>
			<? 
              if (empty($clients))
              {
              ?>
              <tr>
              	<td colspan="5">You do not have any clients registered. Why not <a href="#newclient" class="addnew">add one</a>?</td>
              </tr>
            <? } ?>
			<? foreach($clients as $client){ ?>
			<tr>
				<td><a href="<?= $settings['base_url'].AMP.'method=client_view'.AMP.'client_id='.$client['client_id']; ?>"><?= $client['client_name'] ?></a></td>
                <td><?= $client['client_email'] ?></td>
            </tr>
            <? } ?>
			</table>	
		</td>
		<td width="25%" valign="top">
			<table class="mainTable" width="100%">
				<thead>
				<tr>
					<th align="center">NAVIGATION</th>
				</tr>
				</thead>
				<tr>
					<th align="center">Add New</th>
				</tr>
			</table>
			<table width="100%" style="margin-bottom:10px;">
				<tr>
					<td align="center"><a href="#newimage" class="submit addnew">Image</a></td>
					<td align="center"><a href="#newad" class="submit addnew">Advert</a></td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td align="center"><a href="#newgroup" class="submit addnew">Group</a></td>
					<td align="center"><a href="#newclient" class="submit addnew">Client</a></td>
				</tr>
			</table>
			<div id="newimage" style="display:none;">
			<?=form_open_multipart('&D=cp&C=addons_modules&M=show_module_cp&module=adman&method=upload_image','class=form');?>
			<table class="mainTable" width="100%">
				<tr>
					<th>UPLOAD IMAGE</th>
				</tr>
				<tr>
					<td><input type="file" name="ad_image"><br /><br /><input type="submit" value="Upload" /> or <a href="#newimage" class="addnew">close</a>
				</tr>
			</table>
			</form>
			</div>
			<table class="mainTable" width="100%">
				<thead>
				<tr>
					<th colspan="2" align="center">AD OVERVIEW</th>
				</tr>
				</thead>
				<tr>
					<td class="stat"><span><?= $vars['total_ads']; ?></span>Total</td>
					<td class="stat"><span><?= $vars['active_ads']; ?></span>Active</td>
				</tr>
				<tr>
					<td class="stat"><span><?= $vars['expired_ads']; ?></span>Expired</td>
					<td class="stat"><span><?= $vars['scheduled_ads']; ?></span>Scheduled</td>
				</tr>
			</table>
			
			<table class="mainTable" width="100%">
				<thead>
				<tr>
					<th colspan="3" align="center">TOP 5 ACTIVE ADS</th>
				</tr>
				</thead>
				<tr>
					<th>Ad</th>
					<th width="20%">Impressions</th>
					<th width="20%">Clicks</th>
				</tr>
				<?
				if (empty($top_ads))
				{
				?>
				<tr>
					<td colspan="3" align="center">Sorry, we don't have enough data for this yet.</td>
				</tr>
				<?
				}
				?>
				<?
                 $i=0;
                 foreach($top_ads as $item) {
                 ?>
				<tr>
					<td><a href="<?= $settings['base_url'].AMP.'method=ad_view'.AMP.'ad_id='.$item['ad_id']; ?>"><?= $item['ad_title'];?></a></td>
					<td><?= $item['ad_impression'];?></td>
					<td><?= $item['ad_clicks'];?></td>
				</tr>
				<?
				$i++;
				
				if ($i==5)
				{break;}				 
                } 
                ?>				
			</table>
		</td>
	</tr>
</table>
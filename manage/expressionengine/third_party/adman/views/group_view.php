<table width="100%">
	<tr>
		<td width="80%" valign="top">
			<?=form_open('&D=cp&C=addons_modules&M=show_module_cp&module=adman&method=save_edit_group','class=frm_newgroup');?>
            <input type="hidden" name="site_id" value="<?= $vars['site_id']; ?>" />
            <input type="hidden" name="group_id" value="<?= $group_info[0]['group_id']; ?>" />
            <table class="mainTable" width="100%">
				<thead>
				<tr>
					<th colspan="2">NEW GROUP</th>
				</tr>
				</thead>
				<tr>
					<th width="20%">Group Name :</th>
					<td><input type="text" name="group_title" id="group_title" value="<?= $group_info[0]['group_title']; ?>" /></td>
				</tr>
				<tr>
					<th>Group URL :</th>
					<td><input type="text" name="group_url" id="group_url" value="<?= $group_info[0]['group_url']; ?>" /></td>
				</tr>
				<tr>
					<th>&nbsp;</th>
					<td><input type="submit" class="submit" value="Save" /></td>
				</tr>
			</table>                               
            </form>
            <?=form_open('&D=cp&C=addons_modules&M=show_module_cp&module=adman&method=save_group_sort','class=form');?>
			<table class="mainTable" width="100%">
				<thead>
				<tr>
					<th colspan="2">ADS IN THIS GROUP</th>
				</tr>
				</thead>
				<? foreach($ads as $ad){ ?>
                <tr>
                    <th width="20%"><?= $ad['ad_title'] ?></th>
                    <td><input type="text" style="width:20px;" name="sort_order[<?= $ad['adgroup_id']; ?>]" value="<?= $ad['sort_order'] ?>" /></td>
                </tr>
                <? } ?>  
				<tr>
					<th></th>
					<td><input type="submit" class="submit" value="Save Sort Order" /></td>
				</tr>				
			</table>
			</form>
		</td>
		<td width="20%" valign="top">
			<table class="mainTable" width="100%">
				<thead>
				<tr>
					<th colspan="3">Navigation</th>
				</tr>
				</thead>
			</table>
			<table width="100%" style="margin-bottom:10px;">
				<tr>
					<td width="50%" align="center"><a class="submit" href="<?= BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=adman' ?>">Home</a></td>
					<td width="50%" align="center"><a class="submit" href="#"   onclick="if(confirm('Are you Sure?\nThis will remove the group and it\'s associated ad entries (it will not remove the ads themselves)')){location.href='<?= $settings['base_url'].AMP.'method=group_delete'.AMP.'group_id='.$group_info[0]['group_id']; ?>';}">Delete</a></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
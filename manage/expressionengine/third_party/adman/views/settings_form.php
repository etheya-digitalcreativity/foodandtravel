<?=form_open('C=addons_extensions'.AMP.'M=save_extension_settings'.AMP.'file=Adman');?>
<table class="mainTable">
	<thead>
	<tr>
		<td colspan="2">Ad Manager Settings</td>
	</tr>
	</thead>
	<tr>
		<td width="25%"><?= lang('adman_currency'); ?></td>
		<td><?= $adman_currency; ?></td>
	</tr>
	<tr>
		<td><?= lang('adman_path');?></td>
		<td><?= $adman_path; ?></td>
	</tr>
	<tr>
		<td><?= lang('adman_url');?></td>
		<td><?= $adman_url; ?></td>
	</tr>
	<tr>
		<td><?= lang('adman_email_subject');?></td>
		<td><?= $adman_email_subject; ?></td>
	</tr>
	<tr>
		<td valign="top"><?= lang('adman_email_body');?></td>
		<td><?= $adman_email_body; ?></td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" name="submit" value="Save" /></td>
	</tr>
</table>
</form>
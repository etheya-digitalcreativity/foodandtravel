<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Channel Polls ACT File
 *
 * @package			DevDemon_ChannelPolls
 * @author			DevDemon <http://www.devdemon.com> - Lead Developer @ Parscale Media
 * @copyright 		Copyright (c) 2007-2010 Parscale Media <http://www.parscale.com>
 * @license 		http://www.devdemon.com/license/
 * @link			http://www.devdemon.com/channel_polls/
 */
class ChannelPolls_ACT
{

	/**
	 * Constructor
	 *
	 * @access public
	 *
	 * Calls the parent constructor
	 */
	public function __construct()
	{
		$this->EE =& get_instance();
		$this->site_id = $this->EE->config->item('site_id');
		$this->EE->load->library('polls_helper');
		$this->EE->load->model('polls_model');
		$this->EE->lang->loadfile('channel_polls');
	}

	// ********************************************************************************* //

	public function new_vote()
	{
		$this->ajax = FALSE; // Ajax Call?
		$form_data = array();
		$IP_ADDRESS = $this->EE->polls_helper->getUserIp();
		$IP = sprintf("%u", ip2long($IP_ADDRESS));

		/** ----------------------------------------
		/**  Form Data
		/** ----------------------------------------*/
		$FPID = $this->EE->input->post('FPID');

		if ($this->EE->polls_helper->is_natural_number($FPID) != FALSE)
		{
			// Get the data
			$query = $this->EE->db->select('*')->from('exp_channel_polls_formparams')->where('fp_id', $FPID)->limit(1)->get();

			if ($query->num_rows() == 0)
			{
				return $this->return_error('missing_data', $this->EE->lang->line('poll:error:missing_data') . '(MISSING_FORM_DATA)' );
			}

			$form_data = $query->row_array();

			// Parse form data
			$form_data = array_merge($form_data, unserialize($form_data['form_data']));

			// Same IP?
			if ($form_data['check_ip'] == 'yes' && $form_data['ip_address'] != $IP)
			{
				return $this->return_error('missing_data', $this->EE->lang->line('poll:error:missing_data') . '(IP_NOT_MATCHED)' );
			}

		}
		else
		{
			return $this->return_error('missing_data', $this->EE->lang->line('poll:error:missing_data') . '(FPID_NOT_NUMERIC)' );
		}

		// Need entry_id
		if (isset($form_data['entry_id']) == FALSE OR $form_data['entry_id'] < 1)
		{
			return $this->return_error('missing_data', $this->EE->lang->line('poll:error:missing_data') . '(MISSING_ENTRY_ID)' );
		}

		// Need poll_id
		if (isset($form_data['poll_id']) == FALSE OR $form_data['poll_id'] < 1)
		{
			return $this->return_error('missing_data', $this->EE->lang->line('poll:error:missing_data') . '(MISSING_POLL_ID)' );
		}

		/** ----------------------------------------
		/**  Is the user banned?
		/** ----------------------------------------*/

		if ($this->EE->session->userdata['is_banned'] == TRUE)
		{
			return $this->return_error('not_authorized', $this->EE->lang->line('poll:error:not_authorized') . '(BANNED)');
		}

		/** ----------------------------------------
		/**  Is the IP address and User Agent required?
		/** ----------------------------------------*/

		if ($form_data['check_ip'] == 'yes' && $this->EE->config->item('require_ip_for_posting') == 'y')
		{
			if ($IP_ADDRESS == '0.0.0.0' OR $this->EE->session->userdata['user_agent'] == "")
			{
				return $this->return_error('not_authorized', $this->EE->lang->line('poll:error:not_authorized') . '(INVALID_IP)');
			}
		}

		/** ----------------------------------------
		/**  Is the nation of the user banend?
		/** ----------------------------------------*/
		if ($this->EE->config->item('ip2nation') == 'y' && $this->EE->session->nation_ban_check(FALSE) === FALSE)
		{
			return $this->return_error('not_authorized', $this->EE->lang->line('poll:error:not_authorized') .'(NATION_BANNED)');
		}

		/** ----------------------------------------
		/**  Blacklist/Whitelist Check
		/** ----------------------------------------*/

		if ($this->EE->blacklist->blacklisted == 'y' && $this->EE->blacklist->whitelisted == 'n')
		{
			return $this->return_error('not_authorized', $this->EE->lang->line('poll:error:not_authorized') .'(BLACKLISTED)');
		}

		/** ----------------------------------------
		/**  Captcha
		/** ----------------------------------------*/
		if (isset($form_data['require_captcha']) == TRUE && $form_data['require_captcha'] == TRUE)
		{
			if ( $this->EE->input->post('captcha') == FALSE)
			{
				return $this->return_error('captcha_required', $this->EE->lang->line('poll:error:captcha_required') );
			}
			else
			{
				$this->EE->db->where('word', $this->EE->input->post('captcha'));
				$this->EE->db->where('ip_address', $IP_ADDRESS);
				$this->EE->db->where('date > UNIX_TIMESTAMP()-7200', NULL, FALSE);

				$result = $this->EE->db->count_all_results('captcha');

				if ($result == 0)
				{
					return $this->return_error('captcha_incorrect', $this->EE->lang->line('poll:error:captcha_incorrect') );
				}

				// Delete all old!
				$this->EE->db->query("DELETE FROM exp_captcha WHERE (word='".$this->EE->db->escape_str($_POST['captcha'])."' AND ip_address = '".$IP_ADDRESS."') OR date < UNIX_TIMESTAMP()-7200");
			}
		}

		/** ----------------------------------------
		/**  Allow Multiple?
		/** ----------------------------------------*/
		$already_voted = $this->EE->polls_model->if_already_voted($form_data['poll_id'], $form_data);

		if (isset($form_data['allow_multiple']) == TRUE && $form_data['allow_multiple'] == FALSE && $already_voted == TRUE)
		{
			return $this->return_error('duplicate_vote', $this->EE->lang->line('poll:error:duplicate_vote') );
		}

		/** ----------------------------------------
		/**  Check Answers
		/** ----------------------------------------*/
		$answers = array();
		$answer = $this->EE->input->post('answer');

		// Multiple Answers?
		if (is_array($answer) == TRUE)
		{
			foreach($answer as $val)
			{
				if ($val != FALSE) $answers[] = $val;
			}
		}
		else
		{
			if ($answer != FALSE) $answers[] = $answer;
		}

		unset($answer);

		// We need it!
		if (empty($answers) == TRUE)
		{
			return $this->return_error('missing_data', $this->EE->lang->line('poll:error:missing_answer_input') );
		}

		//----------------------------------------
		// Allow Multiple Answers?
		//----------------------------------------
		if ($form_data['allow_multiple_answers'] == FALSE)
		{
			if (is_array($answers) == TRUE && count($answers) > 1)
			{
				return $this->return_error('not_authorized', $this->EE->lang->line('poll:error:not_authorized') . '(MULTIPLE_ANSWERS_ERROR)');
			}
		}

		//----------------------------------------
		// What Nation
		//----------------------------------------
		$nation = 'xx';

		if ($this->EE->config->item('ip2nation') == 'y')
		{
			if ( version_compare(APP_VER, '2.5.2', '>=') )
			{
				$addr = (string) $IP_ADDRESS;

				// all IPv4 go to IPv6 mapped
				if (strpos($addr, ':') === FALSE && strpos($addr, '.') !== FALSE)
				{
					$addr = '::'.$addr;
				}
				$addr = inet_pton($addr);

				$query = $this->EE->db
				->select('country')
				->where("ip_range_low <= '".$addr."'", '', FALSE)
				->where("ip_range_high >= '".$addr."'", '', FALSE)
				->order_by('ip_range_low', 'desc')
				->limit(1, 0)
				->get('exp_ip2nation');

				if ($query->num_rows() > 0) $nation = $query->row('country');
			}
			else
			{
				$query = $this->EE->db->query("SELECT country FROM exp_ip2nation WHERE ip < INET_ATON('".$this->EE->db->escape_str($IP_ADDRESS)."') ORDER BY ip DESC LIMIT 0,1");
				if ($query->num_rows() > 0) $nation = $query->row('country');
			}
		}

		foreach ($answers as $answer)
		{
			//----------------------------------------
			// Insert Vote
			//----------------------------------------
			$data = array(	'site_id'		=>	$this->site_id,
							'poll_id'		=>	$form_data['poll_id'],
							'answer_id'		=>	$answer,
							'member_id'		=>	$this->EE->session->userdata['member_id'],
							'ip_address'	=>	$IP,
							'nation'		=>	$nation,
							'date'			=>	$this->EE->localize->now,
						);

			$this->EE->db->insert('exp_channel_polls_votes', $data);

			//----------------------------------------
			// Update Answer Stats
			//----------------------------------------
			$this->EE->db->set('total_votes', '(total_votes + 1)', FALSE);
			$this->EE->db->where('answer_id', $answer);
			$this->EE->db->update('exp_channel_polls_answers');
		}



		//----------------------------------------
		// Update Polls Stats
		//----------------------------------------
		$this->EE->db->set('total_votes', '(total_votes + '.count($answers).')', FALSE);
		$this->EE->db->set('last_vote_date', $this->EE->localize->now);
		$this->EE->db->where('poll_id', $form_data['poll_id']);
		$this->EE->db->update('exp_channel_polls');



		// Delete the FORM_DATA
		$this->EE->db->query("DELETE FROM exp_channel_polls_formparams WHERE fp_id = '".$FPID."' ");
		$this->EE->db->query("DELETE FROM exp_channel_polls_formparams WHERE `date` < UNIX_TIMESTAMP()-7200");

		// Set Flashdata! (not sure if we going to use it)
		$this->EE->session->set_flashdata('cp_answer', $answers);

		//----------------------------------------
		// Add cookie
		//----------------------------------------
		if (isset($form_data['disable_cookies']) === FALSE || $form_data['disable_cookies'] != 'yes')
		{
			$items = array();
			if ($this->EE->input->cookie('cpv') != FALSE)
			{
				$ck = $this->EE->input->cookie('cpv');
				$ck = explode('|', $ck);

				foreach ($ck as $key => $row)
				{
					$row = explode('-', $row);
					if (($this->EE->localize->now - $row[1]) > 259200)	continue;

					$items[] = implode('-', $row);
				}
			}

			$items[] = implode('-', array($form_data['poll_id'], $this->EE->localize->now));

			$this->EE->functions->set_cookie('cpv', implode('|', $items), 8553600);
		}

		//----------------------------------------
		// Return Users
		//----------------------------------------
		$RET = $form_data['return'];

		// Redirect people
		$RET = $this->EE->functions->remove_double_slashes($this->EE->functions->create_url(trim_slashes($RET)));
		$this->EE->functions->redirect($RET);

	}

	// ********************************************************************************* //

	protected function return_error($type, $msg)
	{
		// Ajax Response?
		if ($this->ajax == TRUE)
		{
			$out = '{"success":"no", "type": "'.$type.'", "body": "'.$msg.'"}';
		}
		else
		{
			return $this->EE->output->show_user_error('submission', $msg);
		}

		return $out;
	}

	// ********************************************************************************* //

} // END CLASS

/* End of file act.channel_polls.php  */
/* Location: ./system/expressionengine/third_party/channel_polls/act.channel_polls.php */

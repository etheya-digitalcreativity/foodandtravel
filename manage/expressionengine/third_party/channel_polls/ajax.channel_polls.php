<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Channel Polls AJAX File
 *
 * @package			DevDemon_ChannelPolls
 * @author			DevDemon <http://www.devdemon.com> - Lead Developer @ Parscale Media
 * @copyright 		Copyright (c) 2007-2010 Parscale Media <http://www.parscale.com>
 * @license 		http://www.devdemon.com/license/
 * @link			http://www.devdemon.com/channel_polls/
 */
class Channel_Polls_AJAX
{

	/**
	 * Constructor
	 *
	 * @access public
	 *
	 * Calls the parent constructor
	 */
	public function __construct()
	{
		$this->EE =& get_instance();
		$this->EE->load->library('polls_helper');
		$this->EE->lang->loadfile('channel_polls');

		if ($this->EE->input->get_post('site_id')) $this->site_id = $this->EE->input->get_post('site_id');
		else if ($this->EE->input->cookie('cp_last_site_id')) $this->site_id = $this->EE->input->cookie('cp_last_site_id');
		else $this->site_id = $this->EE->config->item('site_id');
	}

	// ********************************************************************************* //

	public function delete_answer()
	{
		if ($this->EE->input->post('answer_id') < 1) exit();

		$answer_id = $this->EE->input->post('answer_id');

		$this->EE->db->from('exp_channel_polls_answers');
		$this->EE->db->where('answer_id', $answer_id);
		$this->EE->db->delete();

		$this->EE->db->from('exp_channel_polls_votes');
		$this->EE->db->where('answer_id', $answer_id);
		$this->EE->db->delete();
	}

	// ********************************************************************************* //


} // END CLASS

/* End of file ajax.channel_polls.php  */
/* Location: ./system/expressionengine/third_party/channel_polls/ajax.channel_polls.php */
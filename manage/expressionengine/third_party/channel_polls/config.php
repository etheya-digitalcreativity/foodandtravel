<?php

/**
 * Config file for Channel Polls
 *
 * @package			DevDemon_ChannelPolls
 * @author			DevDemon <http://www.devdemon.com> - Lead Developer @ Parscale Media
 * @copyright 		Copyright (c) 2007-2010 Parscale Media <http://www.parscale.com>
 * @license 		http://www.devdemon.com/license/
 * @link			http://www.devdemon.com
 * @see				http://ee-garage.com/nsm-addon-updater/developers
 */

if ( ! defined('CHANNEL_POLLS_NAME'))
{
	define('CHANNEL_POLLS_NAME',         'Channel Polls');
	define('CHANNEL_POLLS_CLASS_NAME',   'channel_polls');
	define('CHANNEL_POLLS_VERSION',      '2.7.4');
}

$config['name'] 	= CHANNEL_POLLS_NAME;
$config["version"] 	= CHANNEL_POLLS_VERSION;
$config['nsm_addon_updater']['versions_xml'] = 'http://www.devdemon.com/'.CHANNEL_POLLS_CLASS_NAME.'/versions_feed/';

/* End of file config.php */
/* Location: ./system/expressionengine/third_party/channel_polls/config.php */

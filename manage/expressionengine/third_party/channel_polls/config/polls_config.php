<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (isset($this->EE) == FALSE) $this->EE =& get_instance(); // For EE 2.2.0+

$config['cp_field_defaults'] = array();
$config['cp_field_defaults']['allow_multiple']	= 0;
$config['cp_field_defaults']['allow_multiple_answers']	= 0;
$config['cp_field_defaults']['member_groups']	= '1|5';
$config['cp_field_defaults']['result_order']	= 'answer_order';
$config['cp_field_defaults']['show_results']	= 'after_vote';
$config['cp_field_defaults']['chart_type']		= 'google_pie';
$config['cp_field_defaults']['chart_width']		= 350;
$config['cp_field_defaults']['chart_height']	= 300;
$config['cp_field_defaults']['chart_bg']		= 'FFFFFF';
$config['cp_field_defaults']['answer_colors']	= 'F88437|AE3B93|7DD8F8|5F3A81|16B521|F0CA6A|2F3BBD|DE9D7F|AFED6D|CB1901|EADAEB';

$config['cp_poll_answer_order'] = array();
$config['cp_poll_answer_order']['votes_asc']		= $this->EE->lang->line('poll:result:order:votes_asc');
$config['cp_poll_answer_order']['votes_desc']		= $this->EE->lang->line('poll:result:order:votes_desc');
$config['cp_poll_answer_order']['answer_order']		= $this->EE->lang->line('poll:result:order:answer_order');
$config['cp_poll_answer_order']['random']			= $this->EE->lang->line('poll:result:order:random');

$config['cp_show_poll_result'] = array();
$config['cp_show_poll_result']['after_vote']		= $this->EE->lang->line('poll:result:show:after_vote');
$config['cp_show_poll_result']['poll_closed']		= $this->EE->lang->line('poll:result:show:poll_closed');
$config['cp_show_poll_result']['poll_closed_or_vote']	= $this->EE->lang->line('poll:result:show:poll_closed_or_vote');
$config['cp_show_poll_result']['never']				= $this->EE->lang->line('poll:result:show:never');
$config['cp_show_poll_result']['always']			= $this->EE->lang->line('poll:result:show:always');

$config['cp_charts'] = array();
$config['cp_charts']['google_pie']		= $this->EE->lang->line('poll:chart:google_pie');
$config['cp_charts']['google_pie_3d']	= $this->EE->lang->line('poll:chart:google_pie_3d');
$config['cp_charts']['google_bar_v']	= $this->EE->lang->line('poll:chart:google_bar_v');
$config['cp_charts']['google_bar_h']	= $this->EE->lang->line('poll:chart:google_bar_h');
$config['cp_charts']['none']			= $this->EE->lang->line('poll:chart:none');
<?php if (!defined('BASEPATH')) die('No direct script access allowed');

/**
 * Channel Polls Module FieldType
 *
 * @package			DevDemon_ChannelPolls
 * @version			2.5.1
 * @author			DevDemon <http://www.devdemon.com> - Lead Developer @ Parscale Media
 * @copyright 		Copyright (c) 2007-2011 Parscale Media <http://www.parscale.com>
 * @license 		http://www.devdemon.com/license/
 * @link			http://www.devdemon.com/channel_polls/
 * @see				http://expressionengine.com/user_guide/development/fieldtypes.html
 */
class Channel_polls_ft extends EE_Fieldtype
{

	/**
	 * Field info - Required
	 *
	 * @access public
	 * @var array
	 */
	public $info = array(
		'name' 		=> 'Channel Polls',
		'version'	=> '2.6'
	);

	/**
	 * The field settings array
	 *
	 * @access public
	 * @var array
	 */
	public $settings = array();

	/**
	 * Constructor
	 *
	 * @access public
	 *
	 * Calls the parent constructor
	 */
	public function __construct()
	{
		if (version_compare(APP_VER, '2.1.4', '>')) { parent::__construct(); } else { parent::EE_Fieldtype(); }

		$this->site_id = $this->EE->config->item('site_id');

		$this->EE->load->add_package_path(PATH_THIRD . 'channel_polls/');
		$this->EE->lang->loadfile('channel_polls');
		$this->EE->load->library('polls_helper');
		$this->EE->polls_helper->define_theme_url();
	}

	// ********************************************************************************* //

	/**
	 * Display the field in the publish form
	 *
	 * @access public
	 * @param $data String Contains the current field data. Blank for new entries.
	 * @return String The custom field HTML
	 *
	 * $this->settings =
	 *  Array
	 *  (
	 *      [field_id] =>
	 *      [field_label] =>
	 *      [field_required] => n
	 *      [field_data] =>
	 *      [field_list_items] =>
	 *      [field_fmt] =>
	 *      [field_instructions] =>
	 *      [field_show_fmt] => n
	 *      [field_pre_populate] => n
	 *      [field_text_direction] => ltr
	 *      [field_type] =>
	 *      [field_name] =>
	 *      [field_channel_id] =>
	 *  )
	 */
	public function display_field($data)
	{
		//----------------------------------------
		// Global Vars
		//----------------------------------------
		$vData = array();
		$vData['field_name'] = $this->field_name;
		$vData['field_id'] = $this->field_id;
		$vData['entry_id'] = ($this->EE->input->get_post('entry_id') != FALSE) ? $this->EE->input->get_post('entry_id') : FALSE;
		$vData['answers'] = array();
		$vData['votes'] = array();
		$vData['poll'] = FALSE;

		//----------------------------------------
		// Add Global JS & CSS & JS Scripts
		//----------------------------------------
		$this->EE->cp->add_js_script(array('ui' => array('tabs', 'sortable', 'datepicker', 'slider')));
		$this->EE->polls_helper->mcp_meta_parser('gjs', '', 'ChannelPolls');
		$this->EE->polls_helper->mcp_meta_parser('css', CHANNELPOLLS_THEME_URL . 'channel_polls_pbf.css', 'cp-pbf');
		$this->EE->polls_helper->mcp_meta_parser('css', CHANNELPOLLS_THEME_URL . 'colorpicker/colorpicker.css', 'cp-colorpicker');
		$this->EE->polls_helper->mcp_meta_parser('css', CHANNELPOLLS_THEME_URL . 'jquery-ui-timepicker-addon.css', 'jquery.ui-timepicker');
		//$this->EE->channel_files_helper->mcp_meta_parser('js', CHANNELRATINGS_THEME_URL . 'jquery.base64.js', 'jquery.base64', 'jquery');
		$this->EE->polls_helper->mcp_meta_parser('js', CHANNELPOLLS_THEME_URL . 'colorpicker/colorpicker.js', 'cp-colorpicker');
		$this->EE->polls_helper->mcp_meta_parser('js', CHANNELPOLLS_THEME_URL . 'channel_polls_pbf.js', 'cp-pbf');

		//----------------------------------------
		// Is this SAEF?
		//----------------------------------------
		if (REQ == 'PAGE')
		{
			$this->EE->polls_helper->mcp_meta_parser('css', CHANNELPOLLS_THEME_URL . 'channel_polls_seaf.css', 'cp-seaf');
		}

		//----------------------------------------
		// Polls Config
		//----------------------------------------
		$this->EE->config->load('polls_config');

		$default = $this->EE->config->item('cp_field_defaults');
		$override = $this->EE->config->item('channel_polls');
		if (is_array($override) === FALSE) $override = array();

		// Defaults
		$vData['config'] = $this->EE->polls_helper->array_extend($default, $override);

        // Existing?
		if (isset($this->settings['channel_polls']) == TRUE) $vData['config'] = array_merge($vData['config'], $this->settings['channel_polls']);

		//----------------------------------------
		// Grab Poll From DB
		//----------------------------------------
		$this->EE->db->select('*');
		$this->EE->db->from('exp_channel_polls');
		$this->EE->db->where('entry_id', $vData['entry_id']);
		$this->EE->db->where('field_id', $vData['field_id']);
		$poll = $this->EE->db->get();

		//----------------------------------------
		// Answers
		//----------------------------------------
		if ($poll->num_rows() > 0)
		{
			$this->EE->db->select('*');
			$this->EE->db->from('exp_channel_polls_answers');
			$this->EE->db->where('poll_id', $poll->row('poll_id'));
			$this->EE->db->order_by('answer_order');
			$answers = $this->EE->db->get();

			foreach ($answers->result() as $row)
			{
				$vData['answers'][] = $row;
			}
		}

		// Grab Member Groups
		$mgroups = $this->EE->db->query("SELECT group_id, group_title FROM exp_member_groups WHERE site_id = {$this->site_id} ");
		$vData['member_groups'] = $mgroups->result();

		// Saved Config :)
		$vData['config'] = array_merge($vData['config'], $poll->row_array());

		//----------------------------------------
		// Poll Votes!
		//----------------------------------------
		if ($poll->num_rows() > 0)
		{
			$vData['poll'] = $poll->row();
			$vData['votes_url'] = '';

			// Create Simple Arrays
			$aText = array();
			$aVotes = array();
			$aPercent = array();
			$aColors = array();

			foreach ($answers->result() as $row)
			{
				$aText[] = $row->answer;
				$aVotes[] = $row->total_votes;
				$aPercent[] = number_format( @($row->total_votes / ($poll->row('total_votes') / 100)), 2);
				$aColors[] = $row->color;
			}

			if (class_exists('gChart') == FALSE) include 'libraries/gchart.php';
			$piChart = new gPieChart(400, 200);
			$piChart->addDataSet($aVotes);
			$piChart->setLegend($aText);
			$piChart->set3D(true);
			$piChart->setColors($aColors);
			$piChart->addBackgroundFill('bg', 'ECF1F4');
			$vData['votes_url'] = $piChart->getUrl();
		}

		//----------------------------------------
		// Form Submission Error?
		//----------------------------------------
		if (isset($_POST[$this->field_name]))
		{
			$data = $_POST[$this->field_name]; //exit(print_r($data));

			$vData['answers'] = array();

			foreach ($data['answers'] as $ans)
			{
				$vData['answers'][] = (object) $ans;
			}

			// Kill it
			unset($data['answers']);

			// Member Groups
			$data['member_groups'] = implode('|', $data['member_groups']);

			// End Date needs to be INT
			$data['poll_end_date'] = (method_exists($this->EE->localize, 'string_to_timestamp')) ? $this->EE->localize->string_to_timestamp($data['poll_end_date']) : $this->EE->localize->convert_human_date_to_gmt($data['poll_end_date']);

			// Merge :)
			$vData['config'] = array_merge($vData['config'], $data);
		}

		return $this->EE->load->view('pbf_field', $vData, TRUE);
	}

	// ********************************************************************************* //

	/**
	 * Validates the field input
	 *
	 * @param $data Contains the submitted field data.
	 * @return mixed Must return TRUE or an error message
	 */
	public function validate($data)
	{
		$required_error = FALSE;

		if (isset($data['answers']) == FALSE OR is_array($data['answers']) == FALSE)
		{
			$required_error = TRUE;
		}
		else
		{
			// Check for empty answers
			foreach ($data['answers'] as $order => $answer)
			{
				// Remove unwanted spaces!
				$answer['answer'] = trim($answer['answer']);

				// Nothing Left? Kill
				if ($answer['answer'] == FALSE)
				{
					unset ($data['answers'][$order]);
					continue;
				}

				// Put the cleaned one back.
				$data['answers'][$order]['text'] = $answer['answer'];
			}

			// Now check again
			if (empty($data['answers']) == TRUE OR count($data['answers']) == 1) $required_error = TRUE;
		}

		// Is this a required field?
		if ($this->settings['field_required'] == 'y' && $required_error == TRUE)
		{
			return $this->EE->lang->line('poll:required_error');
		}

		return TRUE;
	}

	// ********************************************************************************* //

	/**
	 * Preps the data for saving
	 *
	 * @param $data Contains the submitted field data.
	 * @return string Data to be saved
	 */
	public function save($data)
	{
		$delete = FALSE;
		if (isset($data['delete_poll']) === TRUE && $data['delete_poll'] == 'yes') $delete = TRUE;

		//----------------------------------------
		// Poll Data
		//----------------------------------------
		$this->EE->session->cache['ChannelPolls']['FieldData'][$this->field_id] = $data;

		if (isset($data['answers']) != FALSE && $delete == FALSE)
		{
			foreach($data['answers'] as $key => $arr)
			{
				$ans = trim($arr['answer']);

				if ($ans == FALSE) unset($data['answers'][$key]);
			}

			if (empty($data['answers']) == TRUE)
			{
				return '';
			}
			else
			{
				return 'ChannelPolls';
			}
		}
		else
		{
			return '';
		}
	}

	// ********************************************************************************* //

	/**
	 * Handles any custom logic after an entry is saved.
	 * Called after an entry is added or updated.
	 * Available data is identical to save, but the settings array includes an entry_id.
	 *
	 * @param $data Contains the submitted field data. (Returned by save())
	 * @access public
	 * @return void
	 */
	public function post_save($data)
	{
		$this->EE->load->library('polls_helper');
		$data = $this->EE->session->cache['ChannelPolls']['FieldData'][$this->field_id];

		if (isset($data['answers']) == FALSE) return;

		$entry_id = $this->settings['entry_id'];
		$channel_id = $this->EE->input->post('channel_id');
		$field_id = $this->field_id;

		// Poll Status
		$poll_status = 0;
		if (isset($data['poll_status']) !== FALSE) $poll_status = $data['poll_status'];

		//----------------------------------------
		// Grab Poll From DB
		//----------------------------------------
		$this->EE->db->select('*');
		$this->EE->db->from('exp_channel_polls');
		$this->EE->db->where('entry_id', $entry_id);
		$this->EE->db->where('field_id', $field_id);
		$poll = $this->EE->db->get();

		// Delete the Poll?
		if (isset($data['delete_poll']) === TRUE && $data['delete_poll'] == 'yes' && $poll->num_rows() > 0)
		{
			$this->EE->db->where('poll_id', $poll->row('poll_id'));
			$this->EE->db->delete('exp_channel_polls');

			$this->EE->db->where('poll_id', $poll->row('poll_id'));
			$this->EE->db->delete('exp_channel_polls_votes');

			$this->EE->db->where('poll_id', $poll->row('poll_id'));
			$this->EE->db->delete('exp_channel_polls_answers');
		}

		// Check for answers
		if (isset($data['answers']) == FALSE OR is_array($data['answers']) == FALSE)
		{
			$data['answers'] = array();
		}

		// Safety measure for member_groups
		if (isset($data['member_groups']) == FALSE) $data['member_groups'] = array();

		// Check for empty answers
		foreach ($data['answers'] as $order => $answer)
		{
			// Remove unwanted spaces!
			$answer['answer'] = str_replace('&', '&36;', $answer['answer']);

			// Nothing Left? Kill
			if ($answer['answer'] == FALSE)
			{
				unset ($data['answers'][$order]);
				continue;
			}

			// Put the cleaned one back.
			$data['answers'][$order]['text'] = $answer['answer'];
		}

		// Just in case we are empty
		if (empty($data['answers']) == TRUE) return FALSE;

		//----------------------------------------
		// Poll already exists
		//----------------------------------------
		if ($poll->num_rows() > 0)
		{
			$total_votes = 0;

			//----------------------------------------
			// Loop over all answers and check for old ones
			//----------------------------------------
			foreach ($data['answers'] as $order => $answer)
			{
				// No Answer text ? Kill!!
				if ($answer['answer'] == FALSE) continue;

				//----------------------------------------
				// Answer Already Exists?
				//----------------------------------------
				if (isset($answer['answer_id']) !== FALSE && $answer['answer_id'] > 0)
				{
					// Update Answer
					$this->EE->db->set('answer', $answer['answer']);
					$this->EE->db->set('color', $answer['color']);
					$this->EE->db->set('total_votes', $answer['total_votes']);
					$this->EE->db->set('answer_order', $order);
					$this->EE->db->where('answer_id', $answer['answer_id']);
					$this->EE->db->update('exp_channel_polls_answers');
					$total_votes += $answer['total_votes'];
				}
				else
				{
					//----------------------------------------
					// New Answer!
					//----------------------------------------
					$this->EE->db->set('answer', $answer['answer']);
					$this->EE->db->set('color', $answer['color']);
					$this->EE->db->set('total_votes', $answer['total_votes']);
					$this->EE->db->set('poll_id', $poll->row('poll_id'));
					$this->EE->db->set('answer_order', $order);
					$this->EE->db->insert('exp_channel_polls_answers');
					$total_votes += $answer['total_votes'];
				}

			}

			//----------------------------------------
			// Update Poll Config
			//----------------------------------------
			$this->EE->db->set('total_votes', $total_votes);
			$this->EE->db->set('allow_multiple', $data['allow_multiple']);
			$this->EE->db->set('allow_multiple_answers', $data['allow_multiple_answers']);
			$this->EE->db->set('member_groups', implode('|', $data['member_groups']));
			$this->EE->db->set('result_order', $data['result_order']);
			$this->EE->db->set('show_results', $data['show_results']);
			$this->EE->db->set('chart_type', $data['chart_type']);
			$this->EE->db->set('chart_width', $data['chart_width']);
			$this->EE->db->set('chart_height', $data['chart_height']);
			$this->EE->db->set('chart_bg', $data['chart_bg']);
			$this->EE->db->set('poll_status',	$poll_status);
			$this->EE->db->set('poll_end_date',	((method_exists($this->EE->localize, 'string_to_timestamp')) ? $this->EE->localize->string_to_timestamp($data['poll_end_date']) : $this->EE->localize->convert_human_date_to_gmt($data['poll_end_date']))  );
			$this->EE->db->where('poll_id', $poll->row('poll_id'));
			$this->EE->db->update('exp_channel_polls');
		}
		else
		{
			//----------------------------------------
			// New Poll!
			//----------------------------------------
			$this->EE->db->set('site_id',		$this->site_id);
			$this->EE->db->set('entry_id',		$entry_id);
			$this->EE->db->set('channel_id',	$channel_id);
			$this->EE->db->set('field_id',		$field_id);
			$this->EE->db->set('member_id',		$this->EE->session->userdata['member_id']);
			$this->EE->db->set('poll_status',	$poll_status);

			$this->EE->db->set('allow_multiple', $data['allow_multiple']);
			$this->EE->db->set('member_groups', implode('|', $data['member_groups']));
			$this->EE->db->set('result_order', $data['result_order']);
			$this->EE->db->set('show_results', $data['show_results']);
			$this->EE->db->set('chart_type', $data['chart_type']);
			$this->EE->db->set('chart_width', $data['chart_width']);
			$this->EE->db->set('chart_height', $data['chart_height']);
			$this->EE->db->set('chart_bg', $data['chart_bg']);
			$this->EE->db->set('poll_status',	$poll_status);
			$this->EE->db->set('poll_end_date',	((method_exists($this->EE->localize, 'string_to_timestamp')) ? $this->EE->localize->string_to_timestamp($data['poll_end_date']) : $this->EE->localize->convert_human_date_to_gmt($data['poll_end_date']))  );
			$this->EE->db->insert('exp_channel_polls');

			$poll_id = $this->EE->db->insert_id();

			$total_votes = 0;

			//----------------------------------------
			// Add Answers
			//----------------------------------------
			foreach ($data['answers'] as $order => $answer)
			{
				// No Answer text ? Kill!!
				if ($answer['answer'] == FALSE) continue;

				//----------------------------------------
				// New Answer!
				//----------------------------------------
				$this->EE->db->set('answer', $answer['answer']);
				$this->EE->db->set('color', $answer['color']);
				$this->EE->db->set('poll_id', $poll_id);
				$this->EE->db->set('answer_order', $order);
				$this->EE->db->insert('exp_channel_polls_answers');
				$total_votes += $answer['total_votes'];
			}

			// Update total votes
			$this->EE->db->set('total_votes', $total_votes);
			$this->EE->db->where('poll_id', $poll_id);
			$this->EE->db->update('exp_channel_polls');
		}

		return;
	}

	// ********************************************************************************* //

	/**
	 * Handles any custom logic after an entry is deleted.
	 * Called after one or more entries are deleted.
	 *
	 * @param $ids array is an array containing the ids of the deleted entries.
	 * @access public
	 * @return void
	 */
	public function delete($ids)
	{
		foreach ($ids as $item_id)
		{
			//$this->EE->db->where('entry_id', $item_id);
			//$this->EE->db->delete('exp_channel_videos');
		}
	}

	// ********************************************************************************* //

	/**
	 * Display the settings page. The default ExpressionEngine rows can be created using built in methods.
	 * All of these take the current $data and the fieltype name as parameters:
	 *
	 * @param $data array
	 * @access public
	 * @return void
	 */
	public function display_settings($data)
	{
		$this->EE->config->load('polls_config');

		// Defaults
		$conf = $this->EE->config->item('cp_field_defaults');

        // Existing?
		if (isset($data['channel_polls']) == TRUE) $conf = array_merge($conf, $data['channel_polls']);

		// -----------------------------------------
		// CSS/JS
		// -----------------------------------------
		$this->EE->polls_helper->mcp_meta_parser('css', CHANNELPOLLS_THEME_URL . 'channel_polls_fts.css', 'cp-fts');
		$this->EE->polls_helper->mcp_meta_parser('css', CHANNELPOLLS_THEME_URL . 'colorpicker/colorpicker.css', 'cp-colorpicker');
		$this->EE->polls_helper->mcp_meta_parser('js', CHANNELPOLLS_THEME_URL . 'colorpicker/colorpicker.js', 'cp-colorpicker');
		$this->EE->polls_helper->mcp_meta_parser('js', CHANNELPOLLS_THEME_URL . 'channel_polls_fts.js', 'cp-fts');


		// -----------------------------------------
		// Multiple Votes
		// -----------------------------------------
		$row = '';
		$options = array('0' => lang('poll:no'), '1' => lang('poll:yes'));

		$row .= form_dropdown('channel_polls[allow_multiple]', $options, $conf['allow_multiple']);
		$this->EE->table->add_row('<strong>'.lang('poll:multiple_votes').'</strong>', $row);

		// -----------------------------------------
		// Multiple Answers
		// -----------------------------------------
		$row = '';
		$options = array('0' => lang('poll:no'), '1' => lang('poll:yes'));

		$row .= form_dropdown('channel_polls[allow_multiple_answers]', $options, $conf['allow_multiple_answers']);
		$this->EE->table->add_row('<strong>'.lang('poll:multiple_answers').'</strong>', $row);

		// -----------------------------------------
		// Vote Member Groups
		// -----------------------------------------
		$row = '';
		$mgroups = $this->EE->db->query("SELECT group_id, group_title FROM exp_member_groups WHERE site_id = {$this->site_id} ");
		$conf['member_groups'] = explode('|', $conf['member_groups']);

		foreach ($mgroups->result() as $group)
		{
			$checked = (in_array($group->group_id, $conf['member_groups']) == FALSE) ? FALSE : TRUE;
			$row .= form_checkbox('channel_polls[member_groups][]', $group->group_id, $checked);
			$row .= '&nbsp;&nbsp;&nbsp;' . $group->group_title . '<br />';
		}
		$this->EE->table->add_row('<strong>'.lang('poll:vote_member_group').'</strong>', $row);

		// -----------------------------------------
		// Result Answer Order
		// -----------------------------------------
		$row = '';
		$options = $this->EE->config->item('cp_poll_answer_order');

		$row .= form_dropdown('channel_polls[result_order]', $options, $conf['result_order']);
		$this->EE->table->add_row('<strong>'.lang('poll:result_answer_order').'</strong>', $row);

		// -----------------------------------------
		// Show Result
		// -----------------------------------------
		$row = '';
		$options = $this->EE->config->item('cp_show_poll_result');

		$row .= form_dropdown('channel_polls[show_results]', $options, $conf['show_results']);
		$this->EE->table->add_row('<strong>'.lang('poll:show_poll_result').'</strong>', $row);

		// -----------------------------------------
		// Chart Type
		// -----------------------------------------
		$row = '';
		$options = $this->EE->config->item('cp_charts');

		$row .= form_dropdown('channel_polls[chart_type]', $options, $conf['chart_type']);
		$this->EE->table->add_row('<strong>'.lang('poll:result_chart_type').'</strong>', $row);

		// -----------------------------------------
		// Chart Settings
		// -----------------------------------------
		$row = '';
		$row .= lang('poll:width') . '&nbsp;' . form_input('channel_polls[chart_width]',  $conf['chart_width'], 'style="width:50px"' ). '&nbsp;&nbsp;&nbsp;&nbsp;';
		$row .= lang('poll:height') . '&nbsp;' . form_input('channel_polls[chart_height]',  $conf['chart_height'], 'style="width:50px"' );
		$row .= lang('poll:bg_color') . '&nbsp;' . form_input('channel_polls[chart_bg]',  $conf['chart_bg'], 'style="width:50px"' );
		$this->EE->table->add_row('<strong>'.lang('poll:result_chart_settings').'</strong>', $row);

		// -----------------------------------------
		// Default Colors
		// -----------------------------------------
		$row = '';
		$conf['answer_colors'] = explode('|', $conf['answer_colors']);

		foreach ($conf['answer_colors'] as $color)
		{
			$row .= '<div class="AnswerColor">';
			$row .= '<span class="colorbutton" rel="'.$color.'" style="background:#'.$color.'"></span>';
			$row .= '<input type="hidden" name="channel_polls[answer_colors][]" value="'.$color.'" class="cpicker"/>';
			$row .= '</div>';
		}

		$this->EE->table->add_row('<strong>'.lang('poll:answer_colors').'</strong>', $row);


	}

	// ********************************************************************************* //

	/**
	 * Save the fieldtype settings.
	 *
	 * @param $data array Contains the submitted settings for this field.
	 * @access public
	 * @return array
	 */
	public function save_settings($data)
	{
		$settings = array('channel_polls' => array());

		if (isset($_POST['channel_polls']) == FALSE) return $settings;

		$P = $_POST['channel_polls'];
		$S = array();

		// Multiple Votes
		$S['allow_multiple'] = (isset($P['allow_multiple']) == TRUE) ? $P['allow_multiple'] : 0;

		// Multiple Votes
		$S['allow_multiple_answers'] = (isset($P['allow_multiple_answers']) == TRUE) ? $P['allow_multiple_answers'] : 0;

		// Vote Member Groups
		$S['member_groups'] = (isset($P['member_groups']) == TRUE) ? implode('|', $P['member_groups']) : '';

		// Result Answer Order
		$S['result_order'] = (isset($P['result_order']) == TRUE) ? $P['result_order'] : '';

		// Show Result
		$S['show_results'] = (isset($P['show_results']) == TRUE) ? $P['show_results'] : '';

		// Chart Type
		$S['chart_type'] = (isset($P['chart_type']) == TRUE) ? $P['chart_type'] : '';

		// Chart Width
		$S['chart_width'] = (isset($P['chart_width']) == TRUE) ? $P['chart_width'] : 350;

		// Chart Height
		$S['chart_height'] = (isset($P['chart_height']) == TRUE) ? $P['chart_height'] : 300;

		// Chart Height
		$S['chart_bg'] = (isset($P['chart_bg']) == TRUE) ? $P['chart_bg'] : 'FFFFFF';

		// Vote Member Groups
		$S['answer_colors'] = (isset($P['answer_colors']) == TRUE) ? implode('|', $P['answer_colors']) : '';

		$settings['channel_polls'] = $S;

		return $settings;
	}

	// ********************************************************************************* //

}

/* End of file ft.channel_polls.php */
/* Location: ./system/expressionengine/third_party/channel_polls/ft.channel_polls.php */

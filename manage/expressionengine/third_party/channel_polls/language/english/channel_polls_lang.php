<?php if (!defined('BASEPATH')) die('No direct script access allowed');

$lang = array(

// Required for MODULES page
'channel_polls'			=>	'Channel Polls',
'channel_polls_module_name'=>	'Channel Polls',
'channel_polls_module_description'	=>	'Add polls to Channel Entries',

//----------------------------------------
// General
//----------------------------------------

'poll:yes'	=> 	'Yes',
'poll:no'	=> 	'No',
'poll:home'	=>	'Polls Home',
'poll:docs'	=>	'Documentation',
'poll:pref'		=>	'Preference',
'poll:setting'	=>	'Setting',

// Fieldtype
'poll:answer'	=>	'Answer',
'poll:answers'	=>	'Answers',
'poll:order'	=>	'Order',
'poll:votes'	=>	'Votes',
'poll:add_answer'		=>	'Add Answer',
'poll:config'	=>	'Poll Settings',
'poll:poll_enabled'	=>	'Is this Poll enabled?',
'poll:poll_end_date'	=>	'Poll End Date',
'poll:poll_end_date_exp'	=>	'(Optional) Normally the entry expiration date will be used (if any).',
'poll:votes'	=>	'Votes',
'poll:color'	=>	'Color',
'poll:delete'	=>	'Delete',
'poll:width'	=>	'Width (px)',
'poll:height'	=>	'Height (px)',
'poll:bg_color'	=>	'Background Color',
'poll:result'	=>	'Result',
'poll:required_error'	=>	'This is a required field. Please fill in at least two (2) answers.',

'poll:no_poll_created'	=>	'No Poll Created...',

// Fieldtype Settings
'poll:multiple_votes'		=>	'Allow users to vote more than once',
'poll:multiple_answers'		=>	'Allow users to vote on multiple answers',
'poll:vote_member_group'	=>	'Which member groups are allowed to vote',
'poll:result_answer_order'	=>	'Poll Result Answer Order',
'poll:result:order:votes_asc'=>	'Least Votes First',
'poll:result:order:votes_desc'=>'Most Votes First',
'poll:result:order:answer_order'=>'Same order as the answers',
'poll:result:order:random'	=>'Random',
'poll:show_poll_result'		=>	'Show Poll Result',
'poll:result:show:after_vote'	=>	'After Vote',
'poll:result:show:poll_closed'	=>	'After Poll Closed',
'poll:result:show:poll_closed_or_vote' => 'After Poll Closed (non-voters) OR After Vote',
'poll:result:show:always'		=>	'Always',
'poll:result:show:never'		=>	'Never',

'poll:result_chart_type'	=>	'Poll Result Chart Type',
'poll:chart:none'			=>	'None (Custom)',
'poll:chart:google_pie'		=>	'Pie Chart (Google Charts)',
'poll:chart:google_pie_3d'	=>	'Pie Chart 3D (Google Charts)',
'poll:chart:google_bar_v'	=>	'Bar Chart "Vertical" (Google Charts)',
'poll:chart:google_bar_h'	=>	'Bar Chart "Horizontal" (Google Charts)',
'poll:result_chart_settings'=>	'Chart Settings',

'poll:delete_poll'	=>	'Delete Poll',
'poll:delete_poll_exp'=>	'Yes i am sure, delete this poll',

'poll:answer_colors'		=>	'Answer Colors',
//----------------------------------------
// Submission Errors
//----------------------------------------
'poll:error:missing_data'		=>	'Missing Data.',
'poll:error:not_authorized'		=>	'You are not authorized to perform this action',
'poll:error:captcha_required'	=>	'You must submit the word that appears in the image',
'poll:error:captcha_incorrect'	=>	'You did not submit the word exactly as it appears in the image',
'poll:error:duplicate_vote'		=>	'You have already voted in this poll.',
'poll:error:missing_answer_input'	=>	'No Answer form field found!',

'poll:error:field_notfound'		=>	'Rating field not found',
'poll:error:required_field'		=>	'Missing REQUIRED field: ',
'poll:error:invalid_rating_format'=>	'Only NUMBERS are allowed as a rating: ',
'poll:error:out_of_range'			=>	'RATING is out of the allowed range: ',
'rating:success:new_like'			=>	'Thank you! Your review has been recorded',

// END
''=>''
);

/* End of file channel_polls_lang.php */
/* Location: ./system/expressionengine/third_party/channel_polls/channel_polls_lang.php */

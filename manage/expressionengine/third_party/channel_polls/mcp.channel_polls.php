<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Channel Polls Module Control Panel Class
 *
 * @package			DevDemon_ChannelPolls
 * @author			DevDemon <http://www.devdemon.com> - Lead Developer @ Parscale Media
 * @copyright 		Copyright (c) 2007-2010 Parscale Media <http://www.parscale.com>
 * @license 		http://www.devdemon.com/license/
 * @link			http://www.devdemon.com/channel_polls/
 * @see				http://expressionengine.com/user_guide/development/module_tutorial.html#control_panel_file
 */
class Channel_polls_mcp
{
	/**
	 * Views Data
	 * @var array
	 * @access private
	 */
	private $vData = array();

	/**
	 * Constructor
	 *
	 * @access public
	 * @return void
	 */
	public function __construct()
	{
		// Creat EE Instance
		$this->EE =& get_instance();

		// Load Models & Libraries & Helpers
		$this->EE->load->library('polls_helper');


		// Some Globals
		$this->base = BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=channel_polls';
		$this->base_short = 'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=channel_polls';

		// Global Views Data
		$this->vData['base_url'] = $this->base;
		$this->vData['base_url_short'] = $this->base_short;
		$this->vData['method'] = $this->EE->input->get('method');

		$this->EE->polls_helper->define_theme_url();

		$this->mcp_globals();

		// Add Right Top Menu
		$this->EE->cp->set_right_nav(array(
			'poll:docs' 				=> $this->EE->cp->masked_url('http://www.devdemon.com/channel_polls/docs/'),
		));

		$this->site_id = $this->EE->config->item('site_id');

		// Debug
		//$this->EE->db->save_queries = TRUE;
		//$this->EE->output->enable_profiler(TRUE);
	}

	// ********************************************************************************* //

	function index()
	{
		// Set the page title
        if (function_exists('ee')) {
            ee()->view->cp_page_title = $this->EE->lang->line('poll:home');
        } else {
            $this->EE->cp->set_variable('cp_page_title', $this->EE->lang->line('poll:home'));
        }

		$this->vData['PageHeader'] = 'home';

		return $this->EE->load->view('mcp_index', $this->vData, TRUE);
	}

	// ********************************************************************************* //

	function mcp_globals()
	{
		$this->EE->cp->set_breadcrumb($this->base, $this->EE->lang->line('channel_polls'));


		// Add Global JS & CSS & JS Scripts
		$this->EE->polls_helper->mcp_meta_parser('gjs', '', 'ChannelPolls');
		$this->EE->polls_helper->mcp_meta_parser('css', CHANNELPOLLS_THEME_URL . 'channel_polls_mcp.css', 'cr-pbf');
		//$this->EE->polls_helper->mcp_meta_parser('js', CHANNELRATINGS_THEME_URL . 'jquery.editable.js', 'jquery.editable', 'jquery');
		$this->EE->polls_helper->mcp_meta_parser('js',  CHANNELPOLLS_THEME_URL . 'channel_polls_mcp.js', 'cr-pbf');

	}

	// ********************************************************************************* //

} // END CLASS

/* End of file mcp.channel_polls.php */
/* Location: ./system/expressionengine/third_party/channel_polls/mcp.channel_polls.php */

<?php if (!defined('BASEPATH')) die('No direct script access allowed');

/**
 * Channel Polls Module Tags
 *
 * @package			DevDemon_ChannelPolls
 * @author			DevDemon <http://www.devdemon.com> - Lead Developer @ Parscale Media
 * @copyright 		Copyright (c) 2007-2011 Parscale Media <http://www.parscale.com>
 * @license 		http://www.devdemon.com/license/
 * @link			http://www.devdemon.com/channel_polls/
 * @see				http://expressionengine.com/user_guide/development/module_tutorial.html#core_module_file
 */
class Channel_polls
{

	/**
	 * Constructor
	 *
	 * @access public
	 *
	 * Calls the parent constructor
	 */
	public function __construct()
	{
		$this->EE =& get_instance();
		$this->site_id = $this->EE->config->item('site_id');
		$this->EE->load->library('polls_helper');
		$this->EE->load->model('polls_model');
	}

	// ********************************************************************************* //

	public function new_vote()
	{
		// Variable prefix
		$prefix = $this->EE->TMPL->fetch_param('prefix', 'poll') . ':';

		$IP_ADDRESS = $this->EE->polls_helper->getUserIp();

		$form_data = array();
		$form_data['allow_multiple'] = FALSE;
		$form_data['return'] = ( isset($this->EE->TMPL->tagparams['return']) == FALSE) ? $this->EE->uri->uri_string().'/' : $this->EE->TMPL->tagparams['return'];
		$form_data['check_ip'] = ($this->EE->TMPL->fetch_param('check_ip') == 'no') ? 'no' : 'yes';
		$form_data['cookies_only'] = ($this->EE->TMPL->fetch_param('cookies_only') == 'yes') ? 'yes' : 'no';
		$form_data['disable_cookies'] = ($this->EE->TMPL->fetch_param('disable_cookies') == 'yes') ? 'yes' : 'no';
		$vars = array();

		// Entry ID
		$entry_id = $this->EE->polls_helper->get_entry_id_from_param();

		// We need an entry_id
		if ($entry_id == FALSE)
		{
			$this->EE->TMPL->log_item('CHANNEL POLLS: Entry ID could not be resolved');
			return $this->EE->polls_helper->custom_no_results_conditional($prefix.'no_access', $this->EE->TMPL->tagdata);
		}

		$form_data['entry_id'] = $entry_id;

		// Which Fields?
		$fields = $this->get_fields_from_params($this->EE->TMPL->tagparams);

		/** ----------------------------------------
		/** Get Poll Data
		/** ----------------------------------------*/
		$this->EE->db->select("*");
		$this->EE->db->from('exp_channel_polls');
		$this->EE->db->where('entry_id', $entry_id);
		if ($fields != FALSE) $this->EE->db->where_in('field_id', $fields);
		$poll = $this->EE->db->get();

		// We need a poll!
		if ($poll->num_rows() == 0)
		{
			$this->EE->TMPL->log_item('CHANNEL POLLS: Entry ID could not be resolved');
			return $this->EE->polls_helper->custom_no_results_conditional($prefix.'no_access', $this->EE->TMPL->tagdata);
		}

		// Parse
		$poll = $poll->row();
		$form_data['poll_id'] = $poll->poll_id;

		// Some variables
		$vars[$prefix.'end_date'] = $poll->poll_end_date;
		$vars[$prefix.'total_votes'] = $poll->total_votes;

		/** ----------------------------------------
		/** Member Group Restriction
		/** ----------------------------------------*/
		$poll->member_groups = explode('|', $poll->member_groups);

		if ( in_array($this->EE->session->userdata['group_id'], $poll->member_groups) == FALSE)
		{
			$this->EE->TMPL->log_item('CHANNEL POLLS: You are not allowed to vote! (member group restriction)');
			return $this->EE->polls_helper->custom_no_results_conditional($prefix.'no_access', $this->EE->TMPL->tagdata);
		}

		/** ----------------------------------------
		/** Poll Closed (Date?)
		/** ----------------------------------------*/
		if (($poll->poll_end_date > 0 && $this->EE->localize->now > $poll->poll_end_date) OR $poll->poll_status == 0)
		{
			$this->EE->TMPL->log_item('CHANNEL POLLS: Poll Closed');
			return $this->EE->polls_helper->custom_no_results_conditional($prefix.'closed', $this->EE->TMPL->tagdata);
		}

		/** ----------------------------------------
		/** Can people vote multiple times?
		/** ----------------------------------------*/
		if ($poll->allow_multiple == 1)
		{
			$form_data['allow_multiple'] = TRUE;
		}

		//----------------------------------------
		// Can people vote on multiple answers?
		//----------------------------------------
		$form_data['allow_multiple_answers'] = FALSE;
		if ($poll->allow_multiple_answers == 1)
		{
			$form_data['allow_multiple_answers'] = TRUE;
		}

		/** ----------------------------------------
		/** Already Voted?
		/** ----------------------------------------*/
		$already_voted = $this->EE->polls_model->if_already_voted($poll->poll_id);
		$cond[$prefix.'already_voted']	= $already_voted;
		$cond[$prefix.'not_voted']	= (($already_voted == FALSE) ? TRUE : FALSE);
		$cond[$prefix.'multiple_answers']	= $form_data['allow_multiple_answers'];

		// XID
		$XID = (isset($_POST['XID']) == TRUE) ? $_POST['XID'] : '';

		/** ----------------------------------------
		/** Store Form Data
		/** ----------------------------------------*/
		$this->EE->db->set('xid', $XID);
		$this->EE->db->set('ip_address', sprintf("%u", ip2long($IP_ADDRESS)));
		$this->EE->db->set('date', $this->EE->localize->now);
		$this->EE->db->set('form_data', serialize($form_data));
		$this->EE->db->insert('channel_polls_formparams');
		$FPID = $this->EE->db->insert_id();

		/** ----------------------------------------
		/** Hidden Fields
		/** ----------------------------------------*/
		$hidden_fields = array();
		$hidden_fields['ACT']	= $this->EE->polls_helper->get_router_url('act_id', 'add_vote');
		$hidden_fields['FPID']	= $FPID;
		$hidden_fields['XID']	= $XID;

		/** ----------------------------------------
		/** Form Data
		/** ----------------------------------------*/
		$data = array();
		$data['hidden_fields']	= $hidden_fields;
		$data['action']		= $this->EE->functions->fetch_current_uri();
		$data['id']			= ($this->EE->TMPL->fetch_param('form_id') != FALSE) ? $this->EE->TMPL->fetch_param('form_id'): 'new_poll';
		$data['class']		= ($this->EE->TMPL->fetch_param('form_class') != FALSE) ? $this->EE->TMPL->fetch_param('form_class'): '';
		$data['onsubmit']	= ($this->EE->TMPL->fetch_param('onsubmit') != FALSE) ? $this->EE->TMPL->fetch_param('onsubmit'): '';

		/** ----------------------------------------
		/** Parse Poll Ansers
		/** ----------------------------------------*/

		// Grab the {poll:answers} var pair
		if (isset($this->EE->TMPL->var_pair[$prefix.'answers']) == FALSE)
		{
			$this->EE->TMPL->log_item("CHANNEL POLLS: No {poll:answers} var pair found.");
			return $this->EE->polls_helper->custom_no_results_conditional($prefix.'no_access', $this->EE->TMPL->tagdata);
		}

		$pair_data = $this->EE->polls_helper->fetch_data_between_var_pairs($prefix.'answers', $this->EE->TMPL->tagdata);

		// Get all answers
		$answers = $this->EE->db->select("*")->from('exp_channel_polls_answers')->where('poll_id', $poll->poll_id)->order_by('answer_order')->get();

		/** ----------------------------------------
		/** Loop and Parse
		/** ----------------------------------------*/
		$answersOUT = '';
		$acount = 0;

		foreach ($answers->result() as $row)
		{
			$acount++;

			$temp = '';
			$tVars = array();
			$tVars[$prefix.'form_name'] = ($poll->allow_multiple_answers == 0) ? 'answer' : 'answer[]';
			$tVars[$prefix.'form_value'] = $row->answer_id;
			$tVars[$prefix.'answer'] = $row->answer;
			$tVars[$prefix.'answer_count'] = $acount;
			$tVars[$prefix.'color'] = $row->color;
			$tVars[$prefix.'votes'] = $row->total_votes;

			$temp = $this->EE->TMPL->parse_variables_row($pair_data, $tVars);
			$answersOUT .= $temp;
		}

		$this->EE->TMPL->tagdata = $this->EE->polls_helper->swap_var_pairs($prefix.'answers', $answersOUT, $this->EE->TMPL->tagdata);

		// Parse variables
		$this->EE->TMPL->tagdata = $this->EE->TMPL->parse_variables_row($this->EE->TMPL->tagdata, $vars);

		// Parse Conditionals
		$this->EE->TMPL->tagdata = $this->EE->functions->prep_conditionals($this->EE->TMPL->tagdata, $cond);

		$OUT = $this->EE->functions->form_declaration($data);
		$OUT .= $this->EE->TMPL->tagdata;
		$OUT .= '<'.'/form>';

		return $OUT;
	}

	// ******************************************************************************** //

	public function poll()
	{
		// Variable prefix
		$prefix = $this->EE->TMPL->fetch_param('prefix', 'poll') . ':';

		//----------------------------------------
		// We need Entry ID
		//----------------------------------------
		$entry_id = $this->EE->polls_helper->get_entry_id_from_param();

		// We need an entry_id
		if ($entry_id == FALSE)
		{
			$this->EE->TMPL->log_item('CHANNEL POLLS: Entry ID could not be resolved');
			return $this->EE->polls_helper->custom_no_results_conditional($prefix.'no_poll', $this->EE->TMPL->tagdata);
		}

		// Which Fields?
		$fields = $this->get_fields_from_params($this->EE->TMPL->tagparams);

		//----------------------------------------
		// Get Poll Data
		//----------------------------------------
		$this->EE->db->select("*");
		$this->EE->db->from('exp_channel_polls');
		$this->EE->db->where('entry_id', $entry_id);
		if ($fields != FALSE) $this->EE->db->where_in('field_id', $fields);
		$poll = $this->EE->db->get();

		// We need a poll!
		if ($poll->num_rows() == 0)
		{
			$this->EE->TMPL->log_item('CHANNEL POLLS: No poll found for this entry');
			return $this->EE->polls_helper->custom_no_results_conditional($prefix.'no_poll', $this->EE->TMPL->tagdata);
		}

		// Parse
		$poll = $poll->row();

		//----------------------------------------
		// Variables (Start)
		//----------------------------------------
		$vars = array();
		$vars[$prefix.'poll_id'] = $poll->poll_id;
		$vars[$prefix.'total_votes'] = $poll->total_votes;
		$vars[$prefix.'last_vote_date'] = $poll->last_vote_date;
		$vars[$prefix.'end_date'] = $poll->poll_end_date;

		//----------------------------------------
		// Has User Already Voted?
		//----------------------------------------
		$already_voted = $this->EE->polls_model->if_already_voted($poll->poll_id);
		$cond[$prefix.'already_voted']	= $already_voted;
		$cond[$prefix.'not_voted']	= (($already_voted == FALSE) ? TRUE : FALSE);
		$cond[$prefix.'can_vote_multiple'] = ($poll->allow_multiple == 1) ? TRUE: FALSE;

		if ($already_voted == TRUE)
		{
			$this->EE->TMPL->log_item('CHANNEL POLLS: Vote Status: ALREADY VOTED');
		}
		else
		{
			$this->EE->TMPL->log_item('CHANNEL POLLS: Vote Status: NOT VOTED');
		}

		$voted_answer_id = 0;
		if (isset($this->EE->session->cache['ChannelPolls']['AlreadyVoted']))
		{
			$voted_answer_id = $this->EE->session->cache['ChannelPolls']['AlreadyVoted']->answer_id;
		}

		//----------------------------------------
		// Poll Closed (Date?)
		//----------------------------------------
		$poll_closed = FALSE;
		if (($poll->poll_end_date > 0 && $this->EE->localize->now > $poll->poll_end_date) OR $poll->poll_status == 0)
		{
			$this->EE->TMPL->log_item('CHANNEL POLLS: Poll Status: CLOSED');
			$poll_closed = TRUE;
		}
		else
	    {
	        $this->EE->TMPL->log_item('CHANNEL POLLS: Poll Status: OPEN');
	    }

	    //----------------------------------------
		// Conditionals
		//----------------------------------------
		$cond[$prefix.'closed'] = $poll_closed;

		//----------------------------------------
		// Can we show the poll?
		//----------------------------------------
		$cond[$prefix.'show_results'] = TRUE;

		if (isset($poll->show_results) !== FALSE)
		{
			if ($poll->show_results == 'after_vote')
			{
				if ($cond[$prefix.'already_voted'] != TRUE) $cond[$prefix.'show_results'] = FALSE;
			}

			elseif ($poll->show_results == 'never')
			{
				$cond[$prefix.'show_results'] = FALSE;
			}

			elseif ($poll->show_results == 'poll_closed')
			{
				if ($poll_closed == TRUE) $cond[$prefix.'show_results'] = FALSE;
			}

			elseif ($poll->show_results == 'poll_closed_or_vote')
			{
				if ($cond[$prefix.'already_voted'] == TRUE ) $cond[$prefix.'show_results'] = TRUE;
				else
				{
					if ($poll_closed == TRUE) $cond[$prefix.'show_results'] = TRUE;
					else $cond[$prefix.'show_results'] = FALSE;
				}
			}
		}

		//----------------------------------------
		// Lets stop here if you can't show poll results
		//----------------------------------------
		if ($cond[$prefix.'show_results'] == FALSE)
		{
			$this->EE->TMPL->tagdata = $this->EE->TMPL->parse_variables_row($this->EE->TMPL->tagdata, $vars);
			$this->EE->TMPL->tagdata = $this->EE->functions->prep_conditionals($this->EE->TMPL->tagdata, $cond);
			return $this->EE->TMPL->tagdata;
		}

		//----------------------------------------
		// Poll Result Order
		//----------------------------------------
		$order	= 'answer_order';
		$sort	= 'ASC';

		if (isset($poll->result_order) !== FALSE)
		{
			switch ($poll->result_order)
			{
				case 'votes_asc':
					$order	= 'total_votes';
					$sort	= 'ASC';
					break;
				case 'votes_desc':
					$order	= 'total_votes';
					$sort	= 'DESC';
					break;
				case 'answer_order':
					$order	= 'answer_order';
					$sort	= 'ASC';
					break;
				case 'random':
					$order	= 'answer_id';
					$sort	= 'random';
					break;
			}
		}

		//----------------------------------------
		// Get All Answers
		//----------------------------------------
		$answers = array();
		$this->EE->db->select("*");
		$this->EE->db->from('exp_channel_polls_answers');
		$this->EE->db->where('poll_id', $poll->poll_id);
		$this->EE->db->order_by($order, $sort);
		$query = $this->EE->db->get();

		foreach ($query->result_array() as $row) $answers[$row['answer_id']] = $row;
		$query->free_result();

		// -----------------------------------------
		// Find our pair data!
		// -----------------------------------------
		$pair_data = FALSE;
		foreach ($this->EE->TMPL->var_pair as $key => $val)
		{
			if (strpos($key,':answers') !== FALSE) $pair_data = $key;
		}

		// Has parameters?
		$pair_params = array();
		$anwers_pair = $pair_data;

		if (isset($this->EE->TMPL->var_pair[$anwers_pair]) != FALSE)
		{

			if (is_array($this->EE->TMPL->var_pair[$anwers_pair]) == TRUE)
			{
				$pair_params = $this->EE->TMPL->var_pair[$anwers_pair];
				$pair_data = $this->EE->polls_helper->fetch_data_between_var_pairs_params($anwers_pair, $prefix.'answers', $this->EE->TMPL->tagdata);
			}
			else
			{
				$pair_data = $this->EE->polls_helper->fetch_data_between_var_pairs($prefix.'answers', $this->EE->TMPL->tagdata);
			}
		}
		else
		{
			$pair_data = '';
		}

		$precision		= ( $this->EE->TMPL->fetch_param('precision') !== FALSE ) ? $this->EE->TMPL->fetch_param('precision'): 2;
		$thousands		= ( $this->EE->TMPL->fetch_param('thousands') ) ? $this->EE->TMPL->fetch_param('thousands'): ',';
		$fractions		= ($this->EE->TMPL->fetch_param('fractions') ) ? $this->EE->TMPL->fetch_param('fractions'): '.';

		// Do we need to parse {polll:answers:countries} ?
		$parse_answer_countries = FALSE;

		if (strpos($pair_data, LD.$prefix.'answers:countries') !== FALSE)
		{
			$parse_answer_countries = TRUE;
			$pair_data_answer_countries = $this->EE->polls_helper->fetch_data_between_var_pairs($prefix.'answers:countries', $pair_data);
		}

		// Loop and Parse!
		$answersOUT = '';
		$acount = 0;

		//----------------------------------------
		// Loop Over all Answers
		//----------------------------------------
		foreach ($answers as $row)
		{
			$acount++;

			//----------------------------------------
			// Parse Answer Vars
			//----------------------------------------
			$tVars = array();
			$tVars[$prefix.'answer_id'] = $row['answer_id'];
			$tVars[$prefix.'answer'] = $row['answer'];
			$tVars[$prefix.'answer_count'] = $acount;
			$tVars[$prefix.'votes'] = $row['total_votes'];
			$tVars[$prefix.'percent'] = number_format( @($row['total_votes'] / ($poll->total_votes / 100)) , $precision, $fractions, $thousands);
			$tVars[$prefix.'color'] = $row['color'];
			$tVars[$prefix.'my_choice'] = ($voted_answer_id == $row['answer_id']) ? 'yes' : '';

			$temp = $this->EE->TMPL->parse_variables_row($pair_data, $tVars);

			//----------------------------------------
			// Parse Countries!
			//----------------------------------------
			if ($parse_answer_countries == TRUE)
			{
				$ccOUT = '';

				// Grab Countries Stats!
				$acq = $this->EE->db->select('COUNT(*) as total, nation', FALSE)->from('exp_channel_polls_votes')->where('answer_id', $row['answer_id'])->group_by('nation')->order_by('total', 'desc')->order_by('nation', 'asc')->get();
				//print_r($acq->result());
				foreach ($acq->result() as $cc)
				{
					$ccVars = array();
					$ccVars[$prefix.'country'] = $cc->nation;
					$ccVars[$prefix.'country_long'] = $this->EE->polls_helper->get_country($cc->nation);
					$ccVars[$prefix.'country_total'] = $cc->total;
					$ccVars[$prefix.'country_percent'] = number_format( @($cc->total / ($row['total_votes'] / 100)) , $precision, $fractions, $thousands);
					$cctemp = $this->EE->TMPL->parse_variables_row($pair_data_answer_countries, $ccVars);
					$ccOUT .= $cctemp;
				}

				$temp = $this->EE->polls_helper->swap_var_pairs($prefix.'answers:countries', $ccOUT, $temp);
			}

			$answersOUT .= $temp;
		}

		// Swap var pair
		if (empty($pair_params) == TRUE) $this->EE->TMPL->tagdata = $this->EE->polls_helper->swap_var_pairs($prefix.'answers', $answersOUT, $this->EE->TMPL->tagdata);
		else
		{
			// Apply backspace?
			$backspace = (isset($pair_params['backspace']) === TRUE) ? $pair_params['backspace'] : 0;
			$answersOUT = ($backspace > 0) ? substr($answersOUT, 0, - $backspace): $answersOUT;

			$this->EE->TMPL->tagdata = $this->EE->polls_helper->swap_var_pairs_params($anwers_pair, $prefix.'answers', $answersOUT, $this->EE->TMPL->tagdata);
		}


		// Create Chart!
		$vars[$prefix.'chart'] = $this->generate_charts($answers, $poll);

		// Parse Variables
		$this->EE->TMPL->tagdata = $this->EE->TMPL->parse_variables_row($this->EE->TMPL->tagdata, $vars);

		// Parse Conditionals
		$this->EE->TMPL->tagdata = $this->EE->functions->prep_conditionals($this->EE->TMPL->tagdata, $cond);

		//----------------------------------------
		// Parse Countries!
		//----------------------------------------
		if (strpos($this->EE->TMPL->tagdata, LD.$prefix.'countries') !== FALSE)
		{
			$pair_data_countries = $this->EE->polls_helper->fetch_data_between_var_pairs($prefix.'countries', $this->EE->TMPL->tagdata);
			$ccOUT = '';

			// Grab Countries Stats!
			$acq = $this->EE->db->select('COUNT(*) as total, nation', FALSE)->from('exp_channel_polls_votes')->where('poll_id', $poll->poll_id)->group_by('nation')->order_by('total', 'desc')->order_by('nation', 'asc')->get();
			//print_r($acq->result());
			foreach ($acq->result() as $cc)
			{
				$ccVars = array();
				$ccVars[$prefix.'country'] = $cc->nation;
				$ccVars[$prefix.'country_long'] = $this->EE->polls_helper->get_country($cc->nation);
				$ccVars[$prefix.'country_total'] = $cc->total;
				$ccVars[$prefix.'country_percent'] = number_format( @($cc->total / ($poll->total_votes / 100)) , $precision, $fractions, $thousands);
				$cctemp = $this->EE->TMPL->parse_variables_row($pair_data_countries, $ccVars);
				$ccOUT .= $cctemp;
			}

			$this->EE->TMPL->tagdata = $this->EE->polls_helper->swap_var_pairs($prefix.'countries', $ccOUT, $this->EE->TMPL->tagdata);
		}

		//----------------------------------------
		// Grab Votes
		//----------------------------------------
		if (strpos($this->EE->TMPL->tagdata, LD.$prefix.'votes_log'.RD) !== FALSE)
		{
			$pair_data = $this->EE->polls_helper->fetch_data_between_var_pairs($prefix.'votes_log', $this->EE->TMPL->tagdata);

			$query = $this->EE->db->select('pv.answer_id, pv.ip_address, pv.member_id, pv.date, pv.nation, mb.screen_name, mb.username')->from('channel_polls_votes pv')->join('exp_members mb', 'mb.member_id = pv.member_id', 'left')->where('pv.poll_id', $poll->poll_id)->get();

			// Loop and Parse!
			$votesOUT = '';

			foreach ($query->result() as $row)
			{
				$tVars = array();
				$tVars[$prefix.'log:answer_id'] = $row->answer_id;
				$tVars[$prefix.'log:answer'] = (isset($answers[$row->answer_id]['answer']) == TRUE) ? $answers[$row->answer_id]['answer'] : 'Unknown';
				$tVars[$prefix.'log:ip_address'] = $row->ip_address;
				$tVars[$prefix.'log:vote_date'] = $row->date;
				$tVars[$prefix.'log:country'] = $row->nation;
				$tVars[$prefix.'log:member_id'] = $row->member_id;
				$tVars[$prefix.'log:screen_name'] = ($row->screen_name != FALSE) ? $row->screen_name : 'Guest';
				$tVars[$prefix.'log:username'] = ($row->username != FALSE) ? $row->username : 'Guest';

				$temp = $this->EE->TMPL->parse_variables_row($pair_data, $tVars);
				$votesOUT .= $temp;
			}

			// Swap var pair
			$this->EE->TMPL->tagdata = $this->EE->polls_helper->swap_var_pairs($prefix.'votes_log', $votesOUT, $this->EE->TMPL->tagdata);
		}

		return $this->EE->TMPL->tagdata;
	}

	// ********************************************************************************* //

	private function generate_charts($answers, $poll)
	{
		$chart = '';

		$chart_type = 'none';

		// Which Chart Type!
		if (isset($poll->chart_type) !== FALSE && $poll->chart_type != FALSE)
		{
			$chart_type = $poll->chart_type;
		}

		// If None..We are done
		if ($chart_type == 'none') return $chart;

		// Which Sizes?
		if (isset($poll->chart_width) !== FALSE && isset($poll->chart_height) !== FALSE)
		{
			$width = $poll->chart_width;
			$height = $poll->chart_height;
		}

		// Background
		$background = 'ffffff';
		if (isset($poll->chart_bg) != FALSE && $poll->chart_bg != FALSE)
		{
			$background = $poll->chart_bg;
		}

		//----------------------------------------
		// Create Simple Arrays
		//----------------------------------------
		$aText = array();
		$aVotes = array();
		$aPercent = array();
		$aColors = array();

		foreach ($answers as $row)
		{
			$aText[] = $row['answer'];
			$aVotes[] = $row['total_votes'];
			$aPercent[] = number_format( @($row['total_votes'] / ($poll->total_votes / 100)), 2);
			$aColors[] = $row['color'];
		}

		//----------------------------------------
		// Google PIE Chart
		//----------------------------------------
		if ($chart_type == 'google_pie')
		{
			if (class_exists('gChart') == FALSE) include 'libraries/gchart.php';
			$piChart = new gPieChart($width, $height);
			$piChart->addDataSet($aVotes);
			$piChart->setLegend($aText);
			$piChart->addBackgroundFill('bg', $background);
			//$piChart->setLabels(array("first", "second", "third","fourth"));
			$piChart->setColors($aColors);
			$chart = '<img src="' . $piChart->getUrl() . '" alt="Poll"/>';
		}


		//----------------------------------------
		// Google 3D PIE CHart
		//----------------------------------------
		elseif ($chart_type == 'google_pie_3d')
		{
			if (class_exists('gChart') == FALSE) include 'libraries/gchart.php';
			$piChart = new gPieChart($width, $height);
			$piChart->addDataSet($aVotes);
			$piChart->setLegend($aText);
			$piChart->set3D(true);
			$piChart->addBackgroundFill('bg', $background);
			//$piChart->setLabels(array("first", "second", "third","fourth"));
			$piChart->setColors($aColors);
			$chart = '<img src="' . $piChart->getUrl() . '" alt="Poll"/>';
		}


		//----------------------------------------
		// Google Bar Chart Vertical
		//----------------------------------------
		elseif ($chart_type == 'google_bar_v')
		{
			if (class_exists('gChart') == FALSE) include 'libraries/gchart.php';
			$barChart = new gBarChart($width, $height,'g');
			foreach($aVotes as $vote) $barChart->addDataSet(array($vote));
			$barChart->setColors($aColors);
			$barChart->setLegend($aText);
			$barChart->setDataRange(0, $poll->total_votes);
			$barChart->addBackgroundFill('bg', $background);
			//$barChart->setAutoBarWidth();
			$chart = '<img src="' . $barChart->getUrl() . '" alt="Poll"/>';
		}

		//----------------------------------------
		// Google Bar Chart Horizontal
		//----------------------------------------
		elseif ($chart_type == 'google_bar_h')
		{
			if (class_exists('gChart') == FALSE) include 'libraries/gchart.php';
			$barChart = new gBarChart($width, $height,'g','h');
			foreach($aVotes as $vote) $barChart->addDataSet(array($vote));
			$barChart->setColors($aColors);
			$barChart->setLegend($aText);
			$barChart->setDataRange(0, $poll->total_votes);
			$barChart->addBackgroundFill('bg', $background);
			//$barChart->setAutoBarWidth();
			$chart = '<img src="' . $barChart->getUrl() . '" alt="Poll"/>';
		}


		return $chart;
	}

	// ********************************************************************************* //

	public function get_fields_from_params($params)
	{
		$fields = array();

		if (isset($params['field_id']) === TRUE)
		{
			// Multiple fields?
			if (strpos($params['field_id'], '|') !== FALSE)
			{
				return explode('|', $params['field_id']);
			}
			else
			{
				return $params['field_id'];
			}
		}

		if (isset($params['field']) === TRUE)
		{
			// Multiple fields?
			if (strpos($params['field'], '|') !== FALSE)
			{
				$pfields = explode('|', $params['field']);

				foreach($pfields as $field)
				{
					if (isset($this->EE->session->cache['channel']['custom_channel_fields'][$this->site_id][ $field ]) === FALSE)
					{
						// Grab the field id
						$query = $this->EE->db->query("SELECT field_id FROM exp_channel_fields WHERE field_name = '{$field}' AND site_id = {$this->site_id} ");
						if ($query->num_rows() == 0)
						{
							if (isset($this->EE->TMPL) === TRUE) $this->EE->TMPL->log_item('CHANNEL_IMAGES: Could not find field : ' . $field);
							return FALSE;
						}
						else
						{
							$this->EE->session->cache['channel']['custom_channel_fields'][$this->site_id][ $field ] = $query->row('field_id');
						}
					}

					$fields[] = $this->EE->session->cache['channel']['custom_channel_fields'][$this->site_id][ $field ];
				}
			}
			else
			{
				if (isset($this->EE->session->cache['channel']['custom_channel_fields'][$this->site_id][ $params['field'] ]) === FALSE)
				{
					// Grab the field id
					$query = $this->EE->db->query("SELECT field_id FROM exp_channel_fields WHERE field_name = '{$params['field']}' AND site_id = {$this->site_id} ");
					if ($query->num_rows() == 0)
					{
						if (isset($this->EE->TMPL) === TRUE) $this->EE->TMPL->log_item('CHANNEL_IMAGES: Could not find field : ' . $params['field']);
						return FALSE;
					}
					else
					{
						$this->EE->session->cache['channel']['custom_channel_fields'][$this->site_id][ $params['field'] ] = $query->row('field_id');
					}
				}

				return $this->EE->session->cache['channel']['custom_channel_fields'][$this->site_id][ $params['field'] ];
			}
		}

		if (empty($fields) === TRUE) return FALSE;

		return $fields;
	}

	// ********************************************************************************* //

	function channel_polls_router()
	{
		// -----------------------------------------
		// Ajax Request?
		// -----------------------------------------
		if ( $this->EE->input->get_post('ajax_method') != FALSE OR (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') )
		{
			// Load Library
			if (class_exists('Channel_Polls_AJAX') != TRUE) include 'ajax.channel_polls.php';

			$AJAX = new Channel_Polls_AJAX();

			// Shoot the requested method
			$method = $this->EE->input->get_post('ajax_method');
			echo $AJAX->$method();
			exit();
		}

		// -----------------------------------------
		// Normal Request
		// -----------------------------------------
		if ($this->EE->input->get_post('MET') != FALSE)
		{
			// Load Library
			if (class_exists('ChannelPolls_ACT') != TRUE) include 'act.channel_polls.php';

			$AJAX = new ChannelPolls_ACT();
		}

	}

	// ********************************************************************************* //

	public function add_vote()
	{
		// Load Library
		if (class_exists('ChannelPolls_ACT') != TRUE) include 'act.channel_polls.php';

		$ACT = new ChannelPolls_ACT();

		$ACT->new_vote();
	}

	// ********************************************************************************* //


} // END CLASS

/* End of file mod.channel_polls.php */
/* Location: ./system/expressionengine/third_party/channel_polls/mod.channel_polls.php */

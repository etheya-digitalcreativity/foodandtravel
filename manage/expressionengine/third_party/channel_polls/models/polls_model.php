<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Polls Model File
 *
 * @package			DevDemon_ChannelPolls
 * @author			DevDemon <http://www.devdemon.com> - Lead Developer @ Parscale Media
 * @copyright 		Copyright (c) 2007-2010 Parscale Media <http://www.parscale.com>
 * @license 		http://www.devdemon.com/license/
 * @link			http://www.devdemon.com/channel_polls/
 */
class Polls_model
{
	/**
	 * Constructor
	 *
	 * @access public
	 * @return void
	 */
	public function __construct()
	{
		// Creat EE Instance
		$this->EE =& get_instance();
		$this->site_id = $this->EE->config->item('site_id');
	}

	// ********************************************************************************* //

	/**
	 * Has user has already voted this poll?
	 *
	 * @param string $type (entry, comment)
	 * @param int $entry_id - The Entry
	 * @access public
	 * @return bool
	 */
	public function if_already_voted($poll_id, $form_data=array())
	{
		// Just to be sure, kill it
		unset($this->EE->session->cache['ChannelPolls']['AlreadyVoted']);

		$voted = FALSE;

		//----------------------------------------
		// Try Cookie
		//----------------------------------------
		if ($this->EE->input->cookie('cpv') != FALSE)
		{
			$ck = $this->EE->input->cookie('cpv');
			$ck = explode('|', $ck);

			foreach ($ck as $row)
			{
				if (strpos($row, $poll_id.'-') !== FALSE)
				{
					$row = explode('-', $row);
					if (($this->EE->localize->now - $row[1]) < 259200)
					{
						$voted = TRUE;

						$this->EE->db->select('vote_id, answer_id');
						$this->EE->db->from('exp_channel_polls_votes');
						$this->EE->db->where('poll_id', $row[0]);
						$this->EE->db->where('date', $row[1]);
						$query = $this->EE->db->get();

						if ($query->num_rows() > 0)
						{
							// Store the result for a short time
							$this->EE->session->cache['ChannelPolls']['AlreadyVoted'] = $query->row();
						}
					}
					break;
				}
			}
		}
		else
		{
			if (isset($this->EE->TMPL->tagparams['cookies_only']) === TRUE && $this->EE->TMPL->tagparams['cookies_only'] == 'yes')
			{
				if ($this->EE->session->userdata['member_id'] == 0) return FALSE;
			}

			if (isset($form_data['cookies_only']) === TRUE && $form_data['cookies_only'] == 'yes')
			{
				if ($this->EE->session->userdata['member_id'] == 0) return FALSE;
			}

			// IP
			$IP = sprintf("%u", ip2long($this->EE->input->ip_address()));

			$this->EE->db->select('vote_id, answer_id');
			$this->EE->db->from('exp_channel_polls_votes');
			$this->EE->db->where('poll_id', $poll_id);
			$this->EE->db->where('member_id', $this->EE->session->userdata['member_id']);
			if ($this->EE->session->userdata['member_id'] == 0) $this->EE->db->where('ip_address', $IP);
			$query = $this->EE->db->get();

			if ($query->num_rows() > 0)
			{
				$voted = TRUE;

				// Store the result for a short time
				$this->EE->session->cache['ChannelPolls']['AlreadyVoted'] = $query->row();
			}

			$query->free_result();
		}

		return $voted;
	}

	// ********************************************************************************* //

	// TEMP SOLUTION FOR EE 2.1.1 SIGH!!!
	public function _assign_libraries()
	{

	}


} // END CLASS

/* End of file polls_model.php  */
/* Location: ./system/expressionengine/third_party/channel_polls/models/polls_model.php */

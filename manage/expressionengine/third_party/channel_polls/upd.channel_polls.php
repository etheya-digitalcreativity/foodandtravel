<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// include config file
include PATH_THIRD.'channel_polls/config'.EXT;

/**
 * Install / Uninstall and updates the modules
 *
 * @package			DevDemon_ChannelPolls
 * @version			2.5.1
 * @author			DevDemon <http://www.devdemon.com> - Lead Developer @ Parscale Media
 * @copyright 		Copyright (c) 2007-2010 Parscale Media <http://www.parscale.com>
 * @license 		http://www.devdemon.com/license/
 * @link			http://www.devdemon.com/channel_polls/
 * @see				http://expressionengine.com/user_guide/development/module_tutorial.html#update_file
 */
class Channel_polls_upd
{
	/**
	 * Module version
	 *
	 * @var string
	 * @access public
	 */
	public $version		=	CHANNEL_POLLS_VERSION;

	/**
	 * Module Short Name
	 *
	 * @var string
	 * @access private
	 */
	private $module_name	=	CHANNEL_POLLS_CLASS_NAME;

	/**
	 * Has Control Panel Backend?
	 *
	 * @var string
	 * @access private
	 */
	private $has_cp_backend = 'y';

	/**
	 * Has Publish Fields?
	 *
	 * @var string
	 * @access private
	 */
	private $has_publish_fields = 'n';


	/**
	 * Constructor
	 *
	 * @access public
	 * @return void
	 */
	public function __construct()
	{
		$this->EE =& get_instance();
	}

	// ********************************************************************************* //

	/**
	 * Installs the module
	 *
	 * Installs the module, adding a record to the exp_modules table,
	 * creates and populates and necessary database tables,
	 * adds any necessary records to the exp_actions table,
	 * and if custom tabs are to be used, adds those fields to any saved publish layouts
	 *
	 * @access public
	 * @return boolean
	 **/
	public function install()
	{
		// Load dbforge
		$this->EE->load->dbforge();

		//----------------------------------------
		// EXP_MODULES
		//----------------------------------------
		$module = array(	'module_name' => ucfirst($this->module_name),
							'module_version' => $this->version,
							'has_cp_backend' => 'y',
							'has_publish_fields' => 'n' );

		$this->EE->db->insert('modules', $module);

		//----------------------------------------
		// EXP_CHANNEL_POLLS
		//----------------------------------------
		$ci = array(
			'poll_id'		=> array('type' => 'INT',		'unsigned' => TRUE,	'auto_increment' => TRUE),
			'site_id'		=> array('type' => 'TINYINT',	'unsigned' => TRUE,	'default' => 1),
			'entry_id'		=> array('type' => 'INT',		'unsigned' => TRUE, 'default' => 0),
			'channel_id'	=> array('type' => 'TINYINT',	'unsigned' => TRUE, 'default' => 0),
			'field_id'		=> array('type' => 'MEDIUMINT',	'unsigned' => TRUE, 'default' => 0),
			'member_id'		=> array('type' => 'INT',		'unsigned' => TRUE, 'default' => 0),
			'poll_status'	=> array('type' => 'TINYINT',	'unsigned' => TRUE, 'default' => 1),
			'poll_end_date'	=> array('type' => 'INT',		'unsigned' => TRUE, 'default' => 0),
			'total_votes'	=> array('type' => 'INT',		'unsigned' => TRUE, 'default' => 0),
			'last_vote_date'=> array('type' => 'INT',		'unsigned' => TRUE, 'default' => 0),

			'allow_multiple'=> array('type' => 'TINYINT',	'unsigned' => TRUE, 'default' => 0),
			'allow_multiple_answers'=> array('type' => 'TINYINT',	'unsigned' => TRUE, 'default' => 0),
			'member_groups'	=> array('type' => 'VARCHAR',	'constraint' => 250, 'default' => ''),
			'result_order'	=> array('type' => 'VARCHAR',	'constraint' => 50, 'default' => ''),
			'show_results'	=> array('type' => 'VARCHAR',	'constraint' => 50, 'default' => ''),
			'chart_type'	=> array('type' => 'VARCHAR',	'constraint' => 50, 'default' => ''),
			'chart_width'	=> array('type' => 'VARCHAR',	'constraint' => 20, 'default' => ''),
			'chart_height'	=> array('type' => 'VARCHAR',	'constraint' => 20, 'default' => ''),
			'chart_bg'		=> array('type' => 'VARCHAR',	'constraint' => 100, 'default' => ''),
		);

		$this->EE->dbforge->add_field($ci);
		$this->EE->dbforge->add_key('poll_id', TRUE);
		$this->EE->dbforge->add_key('entry_id');
		$this->EE->dbforge->create_table('channel_polls', TRUE);

		//----------------------------------------
		// EXP_CHANNEL_POLLS_ANSWERS
		//----------------------------------------
		$ci = array(
			'answer_id'		=> array('type' => 'INT',		'unsigned' => TRUE,	'auto_increment' => TRUE),
			'poll_id'		=> array('type' => 'INT',		'unsigned' => TRUE, 'default' => 0),
			'answer'		=> array('type' => 'VARCHAR',	'constraint' => 250, 'default' => ''),
			'color'			=> array('type' => 'VARCHAR',	'constraint' => 10, 'default' => ''),
			'total_votes'	=> array('type' => 'INT',		'unsigned' => TRUE, 'default' => 0),
			'answer_order'	=> array('type' => 'TINYINT',	'unsigned' => TRUE, 'default' => 1),
		);

		$this->EE->dbforge->add_field($ci);
		$this->EE->dbforge->add_key('answer_id', TRUE);
		$this->EE->dbforge->add_key('poll_id');
		$this->EE->dbforge->create_table('channel_polls_answers', TRUE);

		//----------------------------------------
		// EXP_CHANNEL_POLLS_VOTES
		//----------------------------------------
		$ci = array(
			'vote_id'		=> array('type' => 'INT',		'unsigned' => TRUE,	'auto_increment' => TRUE),
			'site_id'		=> array('type' => 'TINYINT',	'unsigned' => TRUE,	'default' => 1),
			'poll_id'		=> array('type' => 'INT',		'unsigned' => TRUE, 'default' => 0),
			'answer_id'		=> array('type' => 'INT',		'unsigned' => TRUE, 'default' => 0),
			'member_id'		=> array('type' => 'INT',		'unsigned' => TRUE, 'default' => 0),
			'ip_address'	=> array('type' => 'INT',		'unsigned' => TRUE, 'default' => 0),
			'nation'		=> array('type' => 'VARCHAR',	'constraint' => 10, 'default' => ''),
			'date'			=> array('type' => 'INT',		'unsigned' => TRUE, 'default' => 0),
		);

		$this->EE->dbforge->add_field($ci);
		$this->EE->dbforge->add_key('vote_id', TRUE);
		$this->EE->dbforge->add_key('poll_id');
		$this->EE->dbforge->add_key('answer_id');
		$this->EE->dbforge->add_key('member_id');
		$this->EE->dbforge->create_table('channel_polls_votes', TRUE);

		//----------------------------------------
		// EXP_CHANNEL_POLLS_FORMPARAMS
		//----------------------------------------
		$ci = array(
			'fp_id'		=> array('type' => 'INT',		'unsigned' => TRUE,	'auto_increment' => TRUE),
			'xid'		=> array('type' => 'VARCHAR',	'constraint' => 250, 'default' => ''),
			'ip_address'=> array('type' => 'INT',		'unsigned' => TRUE, 'default' => 0),
			'date'		=> array('type' => 'INT',		'unsigned' => TRUE, 'default' => 0),
			'form_data'	=> array('type' => 'TEXT'),
		);

		$this->EE->dbforge->add_field($ci);
		$this->EE->dbforge->add_key('fp_id', TRUE);
		$this->EE->dbforge->create_table('channel_polls_formparams', TRUE);

		//----------------------------------------
		// EXP_ACTIONS
		//----------------------------------------
		$module = array( 'class' => ucfirst($this->module_name), 'method' => $this->module_name . '_router' );
		$this->EE->db->insert('actions', $module);
		$module = array( 'class' => ucfirst($this->module_name), 'method' => 'add_vote' );
		$this->EE->db->insert('actions', $module);

		//----------------------------------------
		// EXP_MODULES
		// The settings column, Ellislab should have put this one in long ago.
		// No need for a seperate preferences table for each module.
		//----------------------------------------
		if ($this->EE->db->field_exists('settings', 'modules') == FALSE)
		{
			$this->EE->dbforge->add_column('modules', array('settings' => array('type' => 'TEXT') ) );
		}

		// Do we need to enable the extension
        //if ($this->uses_extension === TRUE) $this->extension_handler('enable');

		return TRUE;
	}

	// ********************************************************************************* //

	/**
	 * Uninstalls the module
	 *
	 * @access public
	 * @return Boolean FALSE if uninstall failed, TRUE if it was successful
	 **/
	function uninstall()
	{
		// Load dbforge
		$this->EE->load->dbforge();

		// Remove
		$this->EE->dbforge->drop_table('channel_polls');
		$this->EE->dbforge->drop_table('channel_polls_answers');
		$this->EE->dbforge->drop_table('channel_polls_votes');
		$this->EE->dbforge->drop_table('channel_polls_formparams');


		$this->EE->db->where('module_name', ucfirst($this->module_name));
		$this->EE->db->delete('modules');
		$this->EE->db->where('class', ucfirst($this->module_name));
		$this->EE->db->delete('actions');

		// $this->EE->cp->delete_layout_tabs($this->tabs(), 'points');

		return TRUE;
	}

	// ********************************************************************************* //

	/**
	 * Updates the module
	 *
	 * This function is checked on any visit to the module's control panel,
	 * and compares the current version number in the file to
	 * the recorded version in the database.
	 * This allows you to easily make database or
	 * other changes as new versions of the module come out.
	 *
	 * @access public
	 * @return Boolean FALSE if no update is necessary, TRUE if it is.
	 **/
	public function update($current = '')
	{
		// Are they the same?
		if ($current >= $this->version)
		{
			return FALSE;
		}

		$current = str_replace('.', '', $current);

		// Two Digits? (needs to be 3)
		if (strlen($current) == 2) $current .= '0';

		$update_dir = PATH_THIRD.strtolower($this->module_name).'/updates/';

		// Does our folder exist?
		if (@is_dir($update_dir) === TRUE)
		{
			// Loop over all files
			$files = @scandir($update_dir);

			if (is_array($files) == TRUE)
			{
				foreach ($files as $file)
				{
					if ($file == '.' OR $file == '..' OR strtolower($file) == '.ds_store') continue;

					// Get the version number
					$ver = substr($file, 0, -4);

					// We only want greater ones
					if ($current >= $ver) continue;

					require $update_dir . $file;
					$class = 'ChannelPollsUpdate_' . $ver;
					$UPD = new $class();
					$UPD->do_update();
				}
			}
		}

		// Upgrade The Module
		$this->EE->db->set('module_version', $this->version);
		$this->EE->db->where('module_name', ucfirst($this->module_name));
		$this->EE->db->update('exp_modules');

		return TRUE;
	}

} // END CLASS

/* End of file upd.channel_polls.php */
/* Location: ./system/expressionengine/third_party/channel_polls/upd.channel_polls.php */
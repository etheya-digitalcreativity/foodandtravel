<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ChannelPollsUpdate_250
{

	/**
	 * Constructor
	 *
	 * @access public
	 *
	 * Calls the parent constructor
	 */
	public function __construct()
	{
		$this->EE =& get_instance();

		// Load dbforge
		$this->EE->load->dbforge();
	}

	// ********************************************************************************* //

	public function do_update()
	{
		/** ----------------------------------------
		/** Make field_id a MEDIUM INT
		/** ----------------------------------------*/
		if ($this->EE->db->field_exists('field_id', 'channel_polls') == TRUE)
		{
			$fields = array('field_id' => array('name' => 'field_id', 'type' => 'MEDIUMINT',	'unsigned' => TRUE, 'default' => 0) );
			$this->EE->dbforge->modify_column('channel_polls', $fields);
		}

    	/** ----------------------------------------
		/** Add Extra Fields
		/** ----------------------------------------*/
    	if ($this->EE->db->field_exists('allow_multiple', 'channel_polls') == FALSE)
		{
			$fields = array('poll_end_date'	=> array('type' => 'INT',		'unsigned' => TRUE, 'default' => 0),
							'allow_multiple'=> array('type' => 'TINYINT',	'unsigned' => TRUE, 'default' => 0),
							'member_groups'	=> array('type' => 'VARCHAR',	'constraint' => 250, 'default' => ''),
							'result_order'	=> array('type' => 'VARCHAR',	'constraint' => 50, 'default' => ''),
							'show_results'	=> array('type' => 'VARCHAR',	'constraint' => 50, 'default' => ''),
							'chart_type'	=> array('type' => 'VARCHAR',	'constraint' => 50, 'default' => ''),
							'chart_width'	=> array('type' => 'VARCHAR',	'constraint' => 20, 'default' => ''),
							'chart_height'	=> array('type' => 'VARCHAR',	'constraint' => 20, 'default' => ''),
							'chart_bg'		=> array('type' => 'VARCHAR',	'constraint' => 100, 'default' => ''),

			);

			$this->EE->dbforge->add_column('channel_polls', $fields);
		}

		/** ----------------------------------------
		/** Change poll settings over!
		/** ----------------------------------------*/
		$query = $this->EE->db->select('poll_id, poll_config')->from('exp_channel_polls')->get();

		foreach($query->result() as $row)
		{
			$config = unserialize($row->poll_config);

			$data = array();
			$data['allow_multiple'] = (isset($config['multiple_votes']) != FALSE) ? (($config['multiple_votes'] == 'yes') ? 1 : 0) : '';
			$data['member_groups'] = (isset($config['vote_member_groups']) != FALSE) ? implode('|',$config['vote_member_groups']) : '';
			$data['result_order'] = (isset($config['result_answer_order']) != FALSE) ? $config['result_answer_order'] : '';
			$data['show_results'] = (isset($config['show_poll_result']) != FALSE) ? $config['show_poll_result'] : '';
			$data['chart_type'] = (isset($config['result_chart_type']) != FALSE) ? $config['result_chart_type'] : '';
			$data['chart_width'] = (isset($config['result_chart_settings']['width']) != FALSE) ? $config['result_chart_settings']['width'] : '';
			$data['chart_height'] = (isset($config['result_chart_settings']['height']) != FALSE) ? $config['result_chart_settings']['height'] : '';
			$this->EE->db->where('poll_id', $row->poll_id);
			$this->EE->db->update('exp_channel_polls', $data);
		}

		$query->free_result();

		/** ----------------------------------------
		/** Poll Fields now!
		/** ----------------------------------------*/
		$query = $this->EE->db->select('field_id, field_settings')->from('exp_channel_fields')->where('field_type', 'channel_polls')->get();

		foreach ($query->result() as $field)
		{
			$settings = unserialize(base64_decode($field->field_settings));

			$settings['channel_polls'] = array();
			$settings['channel_polls']['allow_multiple'] = 0;
			$settings['channel_polls']['member_groups'] = '';
			$settings['channel_polls']['result_order'] = '';
			$settings['channel_polls']['show_results'] = '';
			$settings['channel_polls']['chart_type'] = '';
			$settings['channel_polls']['chart_width'] = '';
			$settings['channel_polls']['chart_height'] = '';

			if (isset($settings['cp_multiple_votes']) == TRUE)
			{
				$settings['channel_polls']['allow_multiple'] = (($settings['cp_multiple_votes'] == 'yes') ? 1 : 0);
			}

			if (isset($settings['cp_vote_member_groups']) == TRUE && is_array($settings['cp_vote_member_groups']) == TRUE)
			{
				$settings['channel_polls']['member_groups'] = implode('|', $settings['cp_vote_member_groups']);
			}

			if (isset($settings['cp_result_answer_order']) == TRUE)
			{
				$settings['channel_polls']['result_order'] = $settings['cp_result_answer_order'];
			}

			if (isset($settings['cp_show_poll_result']) == TRUE)
			{
				$settings['channel_polls']['show_results'] = $settings['cp_show_poll_result'];
			}

			if (isset($settings['cp_result_chart_type']) == TRUE)
			{
				$settings['channel_polls']['chart_type'] = $settings['cp_result_chart_type'];
			}

			if (isset($settings['cp_result_chart_settings']['width']) == TRUE)
			{
				$settings['channel_polls']['chart_width'] = $settings['cp_result_chart_settings']['width'];
			}

			if (isset($settings['cp_result_chart_settings']['height']) == TRUE)
			{
				$settings['channel_polls']['chart_height'] = $settings['cp_result_chart_settings']['height'];
			}

			unset($settings['cp_multiple_votes']);
			unset($settings['cp_vote_member_groups']);
			unset($settings['cp_result_answer_order']);
			unset($settings['cp_show_poll_result']);
			unset($settings['cp_result_chart_type']);
			unset($settings['cp_result_chart_settings']);

			$settings = base64_encode(serialize($settings));

			$this->EE->db->set('field_settings', $settings);
			$this->EE->db->where('field_id', $field->field_id);
			$this->EE->db->update('exp_channel_fields');
		}

		return TRUE;
	}

	// ********************************************************************************* //

}

/* End of file 230.php */
/* Location: ./system/expressionengine/third_party/channel_polls/updates/230.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ChannelPollsUpdate_260
{

	/**
	 * Constructor
	 *
	 * @access public
	 *
	 * Calls the parent constructor
	 */
	public function __construct()
	{
		$this->EE =& get_instance();

		// Load dbforge
		$this->EE->load->dbforge();
	}

	// ********************************************************************************* //

	public function do_update()
	{
		//----------------------------------------
		// ADD: nation column
		//----------------------------------------
    	if ($this->EE->db->field_exists('nation', 'channel_polls_votes') == FALSE)
		{
			$fields = array('nation' => array('type' => 'VARCHAR',	'constraint' => 10, 'default' => ''));
			$this->EE->dbforge->add_column('channel_polls_votes', $fields, 'ip_address');
		}

		//----------------------------------------
		// Parse Countries
		//----------------------------------------
		if ($this->EE->config->item('ip2nation') == 'y')
		{
			// Grab all votes, hope it can take it
			$query = $this->EE->db->query('SELECT vote_id, ip_address FROM exp_channel_polls_votes');

			foreach ($query->result() as $row)
			{
				$temp = $this->EE->db->select('country')->from('exp_ip2nation')->where('ip <', $row->ip_address)->order_by('ip', 'desc')->limit(1, 0)->get();

				if ($temp->num_rows() == 1)
				{
					$nation = $temp->row('country');
				}
				else
				{
					$nation = 'xx';
				}

				$this->EE->db->set('nation', $nation);
				$this->EE->db->where('vote_id', $row->vote_id);
				$this->EE->db->update('exp_channel_polls_votes');
			}
		}

		return TRUE;
	}

	// ********************************************************************************* //

}

/* End of file 230.php */
/* Location: ./system/expressionengine/third_party/channel_polls/updates/230.php */
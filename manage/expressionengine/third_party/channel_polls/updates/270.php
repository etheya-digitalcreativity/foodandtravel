<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ChannelPollsUpdate_270
{

	/**
	 * Constructor
	 *
	 * @access public
	 *
	 * Calls the parent constructor
	 */
	public function __construct()
	{
		$this->EE =& get_instance();

		// Load dbforge
		$this->EE->load->dbforge();
	}

	// ********************************************************************************* //

	public function do_update()
	{
		//----------------------------------------
		// ADD: allow_multiple_answers
		//----------------------------------------
    	if ($this->EE->db->field_exists('allow_multiple_answers', 'channel_polls') == FALSE)
		{
			$fields = array('allow_multiple_answers' => array('type' => 'TINYINT',	'unsigned' => TRUE, 'default' => 0));
			$this->EE->dbforge->add_column('channel_polls', $fields, 'allow_multiple');
		}

		return TRUE;
	}

	// ********************************************************************************* //

}

/* End of file 230.php */
/* Location: ./system/expressionengine/third_party/channel_polls/updates/270.php */
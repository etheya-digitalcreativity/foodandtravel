<div class="CPField" id="ChannelPolls<?=$field_id?>" rel="<?=$field_id?>">
	<ul class="tabs">
		<li><a href="#<?=$field_id?>_answers"><?=lang('poll:answers')?></a></li>
		<li><a href="#<?=$field_id?>_config"><?=lang('poll:config')?></a></li>
		<li><a href="#<?=$field_id?>_votes"><?=lang('poll:votes')?></a></li>
	</ul>

	<div id="<?=$field_id?>_answers">
		<table class="mainTable PollAnswers">
			<thead>
				<tr>
					<th style="width:20px"><?=lang('poll:order')?></th>
					<th><?=lang('poll:answer')?></th>
					<th style="width:30px"><?=lang('poll:color')?></th>
					<th style="width:50px"><?=lang('poll:votes')?></th>
					<th style="width:20px"><?=lang('poll:delete')?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($answers as $order => $answer):?>
				<tr>
					<td><a href="#" class="MoveAnswer">&nbsp;</a></td>
					<td>
						<input type="text" class="answer" name="<?=$field_name?>[answers][<?=$order?>][answer]" value="<?=$answer->answer?>">
						<input type="hidden" class="answer_id" name="<?=$field_name?>[answers][<?=$order?>][answer_id]" value="<?=$answer->answer_id?>">
					</td>
					<td>
						<span class="colorbutton" rel="<?=$answer->color?>" style="background:#<?=$answer->color?>"></span>
						<input name="<?=$field_name?>[answers][<?=$order?>][color]" class="cpicker" type="hidden" value="<?=$answer->color?>">
					</td>
					<td>
						<input type="text" class="votes" name="<?=$field_name?>[answers][<?=$order?>][total_votes]" value="<?=$answer->total_votes?>">
					</td>
					<td><a href="#" class="DeleteAnswer" rel="<?=$answer->answer_id?>">&nbsp;</a></td>
				</tr>
				<?php endforeach;?>

				<?php if (empty($answers) == TRUE):?>
				<tr>
					<td><a href="#" class="MoveAnswer">&nbsp;</a></td>
					<td>
						<input type="text" class="answer" name="<?=$field_name?>[answers][][answer]">
						<input type="hidden" class="answer_id" name="<?=$field_name?>[answers][][answer_id]">
					</td>
					<td>
						<span class="colorbutton"></span>
						<input name="<?=$field_name?>[answers][][color]" class="cpicker" type="hidden">
					</td>
					<td>
						<input type="text" class="votes" name="<?=$field_name?>[answers][][total_votes]" value="0">
					</td>
					<td><a href="#" class="DeleteAnswer">&nbsp;</a></td>
				</tr>
				<tr>
					<td><a href="#" class="MoveAnswer">&nbsp;</a></td>
					<td>
						<input type="text" class="answer" name="<?=$field_name?>[answers][][answer]">
						<input type="hidden" class="answer_id" name="<?=$field_name?>[answers][][answer_id]">
					</td>
					<td>
						<span class="colorbutton"></span>
						<input name="<?=$field_name?>[answers][][color]" class="cpicker" type="hidden">
					</td>
					<td>
						<input type="text" class="votes" name="<?=$field_name?>[answers][][total_votes]" value="0">
					</td>
					<td><a href="#" class="DeleteAnswer">&nbsp;</a></td>
				</tr>
				<?php endif;?>
			</tbody>
			<tfoot>
				<tr> <td colspan="5"><a href="#" class="AddAnswer"><?=lang('poll:add_answer')?></a></td></tr>
			</tfoot>
		</table>
	</div>

	<div id="<?=$field_id?>_config">
		<table class="mainTable">
			<thead>
				<tr>
					<th width="40%"><?=lang('poll:pref')?></th>
					<th width="60%"><?=lang('poll:setting')?></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td> <strong><?=lang('poll:poll_enabled')?></strong> </td>
					<td>
						<?php $options = array('1' => lang('poll:yes'), '0' => lang('poll:no')); ?>
						<?php echo form_dropdown("{$field_name}[poll_status]", $options, ((isset($config['poll_status']) == FALSE) ? 1 : $config['poll_status']) );?>
					</td>
				</tr>
				<tr>
					<td> <strong><?=lang('poll:poll_end_date')?></strong> </td>
					<td>
						<?=form_input("{$field_name}[poll_end_date]",  ((isset($config['poll_end_date']) == FALSE) ? '' : $this->polls_helper->formatDate('%Y-%m-%d %H:%i', $config['poll_end_date'])), ' class="datetimepicker" style="width:150px" ' )?>
					</td>
				</tr>
				<tr>
					<td> <strong><?=lang('poll:multiple_votes')?></strong> </td>
					<td>
						<?php $options = array('0' => lang('poll:no'), '1' => lang('poll:yes')); ?>
						<?php echo form_dropdown("{$field_name}[allow_multiple]", $options, $config['allow_multiple']);?>
					</td>
				</tr>
				<tr>
					<td> <strong><?=lang('poll:multiple_answers')?></strong> </td>
					<td>
						<?php $options = array('0' => lang('poll:no'), '1' => lang('poll:yes')); ?>
						<?php echo form_dropdown("{$field_name}[allow_multiple_answers]", $options, $config['allow_multiple_answers']);?>
					</td>
				</tr>
				<tr>
					<td> <strong><?=lang('poll:vote_member_group')?></strong> </td>
					<td>
						<?php $config['member_groups'] = explode('|', $config['member_groups']);?>
						<?php foreach ($member_groups as $group):?>
							<?php $checked = (in_array($group->group_id, $config['member_groups']) == TRUE) ? TRUE : FALSE; ?>
							<?=form_checkbox("{$field_name}[member_groups][]", $group->group_id, $checked)?>
							&nbsp;&nbsp;&nbsp;<?=$group->group_title?> <br />
						<?php endforeach;?>
					</td>
				</tr>
				<tr>
					<td> <strong><?=lang('poll:result_answer_order')?></strong> </td>
					<td>
						<?php $options = $this->config->item('cp_poll_answer_order'); ?>
						<?php echo form_dropdown("{$field_name}[result_order]", $options, $config['result_order'] );?>
					</td>
				</tr>
				<tr>
					<td> <strong><?=lang('poll:show_poll_result')?></strong> </td>
					<td>
						<?php $options = $this->config->item('cp_show_poll_result'); ?>
						<?php echo form_dropdown("{$field_name}[show_results]", $options, $config['show_results']);?>
					</td>
				</tr>


				<tr>
					<td> <strong><?=lang('poll:result_chart_type')?></strong> </td>
					<td>
						<?php $options = $this->config->item('cp_charts'); ?>
						<?php echo form_dropdown("{$field_name}[chart_type]", $options, $config['chart_type'] );?>
					</td>
				</tr>

				<tr>
					<td> <strong><?=lang('poll:result_chart_settings')?></strong> </td>
					<td>
						<?=lang('poll:width')?> <?=form_input("{$field_name}[chart_width]",  $config['chart_width'], 'style="width:50px"' )?> &nbsp;&nbsp;&nbsp;&nbsp
						<?=lang('poll:height')?> <?=form_input("{$field_name}[chart_height]",  $config['chart_height'], 'style="width:50px"' )?> &nbsp;&nbsp;&nbsp;&nbsp
						<?=lang('poll:bg_color')?> <?=form_input("{$field_name}[chart_bg]",  $config['chart_bg'], 'style="width:50px"' )?> &nbsp;&nbsp;&nbsp;&nbsp
					</td>
				</tr>

				<tr>
					<td> <strong><?=lang('poll:delete_poll')?></strong> </td>
					<td>
						<input type="checkbox" name="<?=$field_name?>[delete_poll]" value="yes">&nbsp;&nbsp;<?=lang('poll:delete_poll_exp')?>
					</td>
				</tr>

			</tbody>
		</table>
	</div>

	<div id="<?=$field_id?>_votes">
		<table class="mainTable PollVotes">
			<thead>
				<tr>
					<th width="60%"><?=lang('poll:answer')?></th>
					<th width="40%"><?=lang('poll:votes')?></th>
				</tr>
			</thead>
			<tbody>
			<?php if ($poll != FALSE):?>
				<?php foreach($answers as $order => $answer):?>
				<?php $percent = number_format( @($answer->total_votes / ($poll->total_votes / 100)), 2) ?>
				<tr>
					<td> <?=$answer->answer?> </td>
					<td> <?=$answer->total_votes?> &nbsp; (<?=$percent?>%)</td>
				</tr>
				<?php endforeach;?>
			<?php else:?>
				<tr><td colspan="2"><?=lang('poll:no_poll_created')?></td></tr>
			<?php endif;?>
			</tbody>
			<tfoot>
				<?php if ($poll != FALSE):?>
					<tr><td colspan="2"><img src="<?=$votes_url?>"></td></tr>
				<?php endif;?>
			</tfoot>
		</table>

	</div>

	<input type="hidden" class="CP_Data" value='{"field_id":"<?=$field_id?>", "default_colors":"<?=$config['answer_colors']?>"}'/>

</div>

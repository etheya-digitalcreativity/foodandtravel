<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Geotagger Config Items
|--------------------------------------------------------------------------
|
| The following items are for use with Geotagger.
|
*/

if (! defined('NLGEO_NAME'))
{
	define('NLGEO_NAME', 'Geotagger');
	define('NLGEO_ID', strtolower(NLGEO_NAME));
	define('NLGEO_VERSION',  '3.04');
	define('NLGEO_DESC', 'Geotag channel entries using the Control Panel or SafeCracker.');
	define('NLGEO_DOCS', 'http://natural-logic.com/software/geotagger/');
}

/* End of file config.php */
/* Location: ./system/expressionengine/third_party/geotagger/config.php */
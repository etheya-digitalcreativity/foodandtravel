<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Fieldtype for Geotagger: Displays the view for capturing lat/lng.
 *
 * @package		Geotagger
 * @author		Natural Logic - Jason Ferrel
 * @copyright	Copyright (c) 2009-2010, Natural Logic LLC
 * @link		http://www.natural-logic.com/software/geotagger-for-expression-engine/
 * @since		Version 2
 */

require_once PATH_THIRD.'geotagger/config.php';

class Geotagger_ft extends EE_Fieldtype
{
	/**
	 * Field info - Required
	 * 
	 * @access public
	 * @var array
	 */
	public $info = array(
		'name'		=> NLGEO_NAME,
		'version'	=> NLGEO_VERSION
	);

	public $field_id;
	public $field_name;
	public $EE;

	/**
	 * The fieldtype global settings array
	 * 
	 * @access public
	 * @var array
	 */
	public $settings = array();

	/**
	 * Constructor
	 * 
	 * @access public
	 */
	public function __construct()
	{
		parent::EE_Fieldtype();
	}	
	
	/**
	 * Install
	 * 
	 * @access public
	 */
	public function install()
	{
		return array();
	}
	
	/**
	 * Display Settings
	 * 
	 * @access public
	 */
	public function display_settings($data)
	{
		$this->EE->lang->loadfile('geotagger');
		
		// get individual settings if they exist
		$tag_mode = (isset($data['tag_mode'])) ? $data['tag_mode'] : 'manual';
		$default_zoom = (isset($data['default_zoom'])) ? $data['default_zoom'] : 13;
		$zoom = (isset($data['zoom'])) ? $data['zoom'] : 13;
		$address = (isset($data['address'])) ? $data['address'] : 0;
		$city = (isset($data['city'])) ? $data['city'] : 0;
		$state = (isset($data['state'])) ? $data['state'] : 0;
		$zipcode = (isset($data['zipcode'])) ? $data['zipcode'] : 0;
		$latitude = (isset($data['latitude'])) ? $data['latitude'] : 0;
		$longitude = (isset($data['longitude'])) ? $data['longitude'] : 0;
		
		$mode_options = array('manual' => 'Manual', 'auto' => 'Auto-tag');
		
		$this->EE->table->add_row($this->EE->lang->line('label_mode'), form_dropdown(NLGEO_ID.'_tag_mode', $mode_options, $tag_mode));
		
		$zoom_options = array(
							'1' => '1',
							'2' => '2',
							'3' => '3',
							'4' => '4',
							'5' => '5',
							'6' => '6',
							'7' => '7',
							'8' => '8',
							'9' => '9',
							'10' => '10',
							'11' => '11',
							'12' => '12',
							'13' => '13',
							'14' => '14',
							'15' => '15',
							'16' => '16',
							'17' => '17',
							'18' => '18',
							'19' => '19',
							'20' => '20'
						);
						
		$this->EE->table->add_row($this->EE->lang->line('label_default_zoom'), form_dropdown(NLGEO_ID.'_default_zoom', $zoom_options, $default_zoom));

		// get the field group id
		$this->EE->db->select('group_id')->from('field_groups')->where('group_name', $data['group_name'])->where('site_id', $this->EE->config->item('site_id'));
		$field_group_id = $this->EE->db->get()->row()->group_id;

		// get available fields for this channel
		$this->EE->db->select('field_id, field_label, field_name')->from('channel_fields')->where('group_id', $field_group_id)->where('site_id', $this->EE->config->item('site_id'))->order_by('field_label', 'asc');
		$fields = $this->EE->db->get()->result();
		$field_map_options = array('0|0' => $this->EE->lang->line('label_fm_na'));
		
		foreach($fields as $field)
		{
			$field_map_options[$field->field_id.'|'.$field->field_name] = $field->field_label;
		}
		
		$this->EE->table->add_row(array('data' => '<strong>'.$this->EE->lang->line('label_fm_heading').'</strong>', 'colspan' => 2));
		
		$this->EE->table->add_row($this->EE->lang->line('label_fm_address'), form_dropdown(NLGEO_ID.'_address', $field_map_options, $address));
						
		$this->EE->table->add_row($this->EE->lang->line('label_fm_city'), form_dropdown(NLGEO_ID.'_city', $field_map_options, $city));
						
		$this->EE->table->add_row($this->EE->lang->line('label_fm_state'), form_dropdown(NLGEO_ID.'_state', $field_map_options, $state));
						
		$this->EE->table->add_row($this->EE->lang->line('label_fm_zip'), form_dropdown(NLGEO_ID.'_zipcode', $field_map_options, $zipcode));
						
		$this->EE->table->add_row($this->EE->lang->line('label_fm_latitude'), form_dropdown(NLGEO_ID.'_latitude', $field_map_options, $latitude));
						
		$this->EE->table->add_row($this->EE->lang->line('label_fm_longitude'), form_dropdown(NLGEO_ID.'_longitude', $field_map_options, $longitude));
						
		$this->EE->table->add_row($this->EE->lang->line('label_fm_zoom'), form_dropdown(NLGEO_ID.'_zoom', $field_map_options, $zoom));


	}
	
	/**
	 * Save Field Settings
	 * 
	 * @access public
	 */
	public function save_settings($data)
	{
		return array(
			'tag_mode' => $this->EE->input->post(NLGEO_ID.'_tag_mode'),
			'zoom' => $this->EE->input->post(NLGEO_ID.'_zoom'),
			'address' => $this->EE->input->post(NLGEO_ID.'_address'),
			'city' => $this->EE->input->post(NLGEO_ID.'_city'),
			'state' => $this->EE->input->post(NLGEO_ID.'_state'),
			'zipcode' => $this->EE->input->post(NLGEO_ID.'_zipcode'),
			'latitude' => $this->EE->input->post(NLGEO_ID.'_latitude'),
			'longitude' => $this->EE->input->post(NLGEO_ID.'_longitude'),
			'default_zoom' => $this->EE->input->post(NLGEO_ID.'_default_zoom'),
			'sensor' => $this->EE->input->post(NLGEO_ID.'_sensor')
		);
	}

	/**
	 * Display the field in the publish form
	 * 
	 * @access public
	 * @param $data String Contains the current field data. Blank for new entries.
	 * @return String The custom field HTML
	 */
	public function display_field($data)
	{	
		$this->EE->lang->loadfile('geotagger');
		$this->EE->load->library('geotagger_theme');
		
		$channel_id = $this->EE->input->get_post('channel_id');
		$entry_id = $this->EE->input->get_post('entry_id');
		$is_cp = ($this->EE->input->get('M') == 'entry_form') ? TRUE : FALSE;

		$tag_mode = $this->settings['tag_mode'];		
		$address_field = explode("|", $this->settings['address']);
		$city_field = explode("|", $this->settings['city']);
		$state_field = explode("|", $this->settings['state']);
		$zip_field = explode("|", $this->settings['zipcode']);
		$lat_field = explode("|", $this->settings['latitude']);
		$lng_field = explode("|", $this->settings['longitude']);
		$zoom_field = explode("|", $this->settings['zoom']);
		
		//load up javascript vars
		$this->EE->cp->add_to_head('
			<script type="text/javascript">
				var NLGEO = {
					"is_cp" : '.(($is_cp) ? "true" : "false").',
					"tag_mode" : "'.$tag_mode.'",
					"field_prefix" : "'.(($is_cp) ? 'field_id_' : '').'",
					"address_field" : "'.(($is_cp) ? $address_field[0] : $address_field[1]).'",
					"city_field" : "'.(($is_cp) ? $city_field[0] : $city_field[1]).'",
					"state_field" : "'.(($is_cp) ? $state_field[0] : $state_field[1]).'",
					"zip_field" : "'.(($is_cp) ? $zip_field[0] : $zip_field[1]).'",
					"lat_field" : "'.(($is_cp) ? $lat_field[0] : $lat_field[1]).'",
					"lng_field" : "'.(($is_cp) ? $lng_field[0] : $lng_field[1]).'",
					"zoom_field" : "'.(($is_cp) ? $zoom_field[0] : $zoom_field[1]).'",
					"default_zoom" : '.$this->settings['default_zoom'].',
					"message_bg" : "#ffffcc",
					"msg_geo_location"  : "'.$this->EE->lang->line('msg_geo_location').'",
					"msg_geo_success"  : "'.$this->EE->lang->line('msg_geo_success').'",
					"msg_geo_error" : "'.$this->EE->lang->line('msg_geo_error').'",
					"label_btn_geo" : "'.(($entry_id) ? $this->EE->lang->line('btn_geo_update') : $this->EE->lang->line('btn_geo')).'",
					"label_msg_geo" : "'.(($entry_id) ? $this->EE->lang->line('msg_existing_geo') : $this->EE->lang->line('msg_before_geo')).'",
					"existing_entry" : '.(($entry_id) ? 1 : 0).',
					"override" : false,
					"auto_status" : 0,
					"form_hook" : "'.(($is_cp) ? '#publishForm' : '.geotagger').'"		
				}
			</script>
			');		
		
		
		//load google maps api
		if(isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "on")
		{
			$this->EE->cp->add_to_head('<script type="text/javascript" src="http://maps.google.com/maps/api/js?v=3.3&sensor=false"></script>');	
		}else
		{
			$this->EE->cp->add_to_head('<script type="text/javascript" src="https://maps.google.com/maps/api/js?v=3.3&sensor=false"></script>');
		}

		
		// Add CSS
		$this->EE->geotagger_theme->add_css('css/geotagger.css');
		
		// Add JS
		$this->EE->geotagger_theme->add_js('js/geotagger.js');
		
		$field_data = array(
				'field_name' 	=> $this->field_name,
				'entry_id'		=> $entry_id,
				'tag_mode'		=> $tag_mode
			);
		
		if ($tag_mode == 'auto')
		{
			return $this->EE->load->view('fieldtype/auto', $field_data, TRUE);
		}else
		{
			return $this->EE->load->view('fieldtype/index', $field_data, TRUE);
		}	
	}
}
//END CLASS
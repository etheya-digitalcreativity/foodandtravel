<?php 
$lang = array(
	'geotagger'							=> 'Geotagger',
	'label_mode'		 				=> 'Geotagger Mode',
	'label_default_zoom' 				=> 'Default Zoom',
	'label_fm_heading' 					=> 'Specify Field Mappings for Geotagger',
	'label_fm_address' 					=> 'Address field',
	'label_fm_city' 					=> 'City field',
	'label_fm_state' 					=> 'State field',
	'label_fm_zip' 						=> 'Zip code field',
	'label_fm_na' 						=> 'No mapping needed',
	'label_fm_latitude' 				=> 'Latitude field',
	'label_fm_longitude' 				=> 'Longitude field',
	'label_fm_zoom' 					=> 'Zoom level field',
	'label_auto_new' 					=> 'Location data will be obtained when you save the entry.',
	'msg_auto_update'					=> 'Update latitude/longitude points by dragging the marker pin or by changing the address related fields and saving the entry.',
	'msg_before_geo'  					=> 'Before Geotagging, make sure you have entered the address information for this entry.',
	'msg_existing_geo'  				=> 'Geotagging this entry will overwrite the existing location data.',
	'msg_geo_error'  					=> 'Geocode was not successful for the following reason: ',
	'msg_geo_address'  					=> 'Geotagged address is',
	'msg_geo_location'  				=> 'Location',
	'msg_geo_success'  					=> 'updated successfully.',
	'msg_lat_updated'  					=> 'Latitude updated to ',
	'msg_lng_updated'  					=> 'Longitude updated to ',
	'btn_geo'  							=> 'Geotag Entry',
	'btn_geo_update'  					=> 'Geotag Entry',

	// END
	''=>''
);

/* End of file lang.geotagger.php */
/* Location: ./system/expressionengine/third_party/geotagger/language/english/lang.geotagger.php */
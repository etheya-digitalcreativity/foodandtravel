<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Geotagger Theme Class for EE2
*/
class Geotagger_theme {

	function Geotagger_theme()
	{
		$this->EE =& get_instance();
	}

	/**
	 * Get Theme URL
	 */
	function get_theme_url()
	{
		$theme_folder_url = $this->EE->config->item('theme_folder_url');
		if (substr($theme_folder_url, -1) != '/') $theme_folder_url .= '/';
		return $theme_folder_url.'third_party/geotagger/';
	}

	/**
	 * Add CSS
	 */
	function add_css($file)
	{
		$this->EE->cp->add_to_head('<link rel="stylesheet" type="text/css" href="'.$this->get_theme_url().$file.'" />');
	}

	/**
	 * Add JS
	 */
	function add_js($file)
	{
		$this->EE->cp->add_to_foot('<script type="text/javascript" src="'.$this->get_theme_url().$file.'"></script>');
	}
}
/* End of file Geotagger_theme.php */
/* Location: ./system/expressionengine/third_party/geotagger/libraries/Geotagger_theme.php */
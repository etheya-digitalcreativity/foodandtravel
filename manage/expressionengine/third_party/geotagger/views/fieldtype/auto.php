<div id="geo-wrap">
	<div id="geo-details">
		<input type="hidden" id="nlgeo-address" name="nlgeo-address" value="" />
		<div class="geo-msg-wrap">
			<p id="geo-messages"><?=($entry_id) ? lang('msg_auto_update') : lang('label_auto_new')?></p>
		</div>
		<?php if ($entry_id): ?>
		<div id="geo-map-canvas"></div>
		<?php endif; ?>
	</div>
</div>
<div id="geo-wrap">
	<div id="geo-details">
		<div class="geo-msg-wrap">
			<p id="geo-btn">
				<a id="geo-tag" class="btn" href="#" title="<?=($entry_id) ? lang('btn_geo_update') : lang('btn_geo')?>"><?=($entry_id) ? lang('btn_geo_update') : lang('btn_geo')?></a>
			</p>
			<p id="geo-messages"><?=($entry_id) ? lang('msg_existing_geo') : lang('msg_before_geo')?></p>
		</div>
		<div id="geo-map-canvas" <?=($entry_id) ? '' : 'style="display:none;"' ?>></div>
	</div>	
</div>
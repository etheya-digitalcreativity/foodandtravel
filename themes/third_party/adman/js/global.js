// HTML5 placeholder plugin version 0.3
// Enables cross-browser* html5 placeholder for inputs, by first testing
// for a native implementation before building one.
//
// USAGE: 
//$('input[placeholder]').placeholder();

(function($){
  
  $.fn.placeholder = function(options) {
    return this.each(function() {
      if ( !("placeholder"  in document.createElement(this.tagName.toLowerCase()))) {
        var $this = $(this);
        var placeholder = $this.attr('placeholder');
        $this.val(placeholder).data('color', $this.css('color')).css('color', '#aaa');
        $this
          .focus(function(){ if ($.trim($this.val())===placeholder){ $this.val('').css('color', $this.data('color')); } })
          .blur(function(){ if (!$.trim($this.val())){ $this.val(placeholder).data('color', $this.css('color')).css('color', '#aaa'); } });
      }
    });
  };
}(jQuery));

var menuYloc = null;
var sortableCookieExpiry = 365;
var sortableCookie = "sortable-order"
var sortableName = ".sortable";

function slugify(str){
    return str.replace(/\s+/g,'-').replace(/[^a-zA-Z0-9\-]/g,'').toLowerCase();
}


// perform JavaScript after the document is scriptable.
$(document).ready(function() {

	$(".frm_newad").submit(function(e){
		
		//Lets Validate!
		if ($(".ad_title").val()=="")
		{
			$(".ad_title").css('border','thin solid #E11842');
			$(".ad_title").focus();
			return false;
		}
		
		if ($(".ad_url").val()=="")
		{
			$(".ad_url").css('border','thin solid #E11842');
			$(".ad_url").focus();
			return false;
		}
		
		var checked = $(".ad_group:checked").length;
		if (checked == 0)
		{
			$(".tblgroups").css('border','thin solid #E11842');
			return false;
		}
		
		return true;
	
	})
	
	$(".frm_newgroup").submit(function(){
	
		if ($("#group_title").val()=="")
		{
			$("#group_title").css('border','thin solid #E11842');
			$("#group_title").focus();
			return false;
		}
		
		if ($("#group_url").val()=="")
		{
			$("#group_url").css('border','thin solid #E11842');
			$("#group_url").focus();
			return false;
		}
		
		return true;
	
	})
	
	$(".frm_newclient").submit(function(){
		if ($("#client_name").val()=="")
		{
			$("#client_name").css('border','thin solid #E11842');
			$("#client_name").focus();
			return false;	
		}
		
		if ($("#client_email").val()=="")
		{
			$("#client_email").css('border','thin solid #E11842');
			$("#client_email").focus();
			return false;	
		}
		
		return true;
	})

	$(".email_report").submit(function(e){
		e.preventDefault();
		
		$.ajax({
			type: 'POST',
 			url: $(this).attr('action'),
 			data: $(this).serialize(),
 			success: function(){
 				
 				$("#send_button").val('Email Sent!');
 				setTimeout("$('#emailreport').animate({height:'toggle'},{duration: 1200});",1500);
 				setTimeout("$('#send_button').val('Send Email');",1500);
 			}
		})
	})
	
	$(".addnew").click(function(e){
		e.preventDefault();
		
		$id = $(this).attr('href');
		
		//$($id).toggle('slide');
		
		$($id).animate({
				height:'toggle'
			},{
    		duration: 1200
    	});
		
	})


	$("input#group_title").keyup(function(){
		$("input#group_url").val(slugify($(this).val()));
	})
	
	$(".adselector").click(function(){
		$("div.ad_banner").hide();
		$("div.ad_text").hide();
		
		$("div."+$(this).attr('rel')).show();
		return false;
	})

    /**
     * Main Menu Shortcuts Toggle
     */
    $('.chevron').click(function(){
        if ($(this).hasClass('toggle-up')) {
            if (!$.browser.msie) {
                $('.shortcuts').slideUp();
            } else {
                $('.shortcuts').hide();
            }
        } else {
            if (!$.browser.msie) {
                $('.shortcuts').slideDown();
            } else {
                $('.shortcuts').show();
            }
        }
        $(this).toggleClass('toggle-up');
    });

    /**
     * attach calendar to date inputs
     */
    $(":date")
		.wrap('<span class="ui-date" />')
        .dateinput({trigger: true, format: 'yyyy-mm-dd 00:00:00', selectors: true})
        .focus(function(){$(this).parent().addClass('ui-focused'); return false;})
        .blur(function(){$(this).parent().removeClass('ui-focused'); return false;});

    /**
     * add close buttons to closeable message boxes
     */
    $(".message.closeable").prepend('<span class="message-close"></span>')
        .find('.message-close')
        .click(function(){
            $(this).parent().fadeOut(function(){$(this).remove();});
        });

    /**
     * setup popup balloons (add contact / add task)
     */
    $('.has-popupballoon').click(function(){
        // close all open popup balloons
        $('.popupballoon').fadeOut();
        $(this).next().fadeIn();
        return false;
    });

    $('.popupballoon .close').click(function(){
        $(this).parents('.popupballoon').fadeOut();
    });

    /**
     * floating menu
     */
    if ($('#wrapper > header').length>0) { menuYloc = parseInt($('#wrapper > header').css("top").substring(0,$('#wrapper > header').css("top").indexOf("px")), 10); }
    $(window).scroll(function () {
        var offset = 0;
        if ($('#wrapper > header').length>0) {
            offset = menuYloc+$(document).scrollTop();
            if (!$.browser.msie) { $('#wrapper > header').animate({opacity: ($(document).scrollTop()<=10? 1 : 0.8)}); }
        }
    });

    if (!$.browser.msie) {
        $('#wrapper > header').hover(
            function(){$(this).animate({opacity: 1});},
            function(){$(this).animate({opacity: ($(document).scrollTop()<=10? 1 : 0.8)});}
        );
    }

    /**
     * html element for the help popup
     */
    $('body').append('<div class="apple_overlay black" id="overlay"><iframe class="contentWrap" style="width: 100%; height: 500px"></iframe></div>');

    /**
     * this is the help popup
     */
    $("a.help[rel]").overlay({

        effect: 'apple',

        onBeforeLoad: function() {

            // grab wrapper element inside content
            var wrap = this.getOverlay().find(".contentWrap");

            // load the page specified in the trigger
            wrap.attr('src', this.getTrigger().attr("href"));
        }

    });

    /**
     * Form Validators
     */
    // Regular Expression to test whether the value is valid
    $.tools.validator.fn("[type=time]", "Please supply a valid time", function(input, value) { 
        return (/^\d\d:\d\d$/).test(value);
    });

    $.tools.validator.fn("[data-equals]", "Value not equal with the $1 field", function(input) {
        var name = input.attr("data-equals"),
        field = this.getInputs().filter("[name=" + name + "]"); 
        return input.val() === field.val() ? true : [name]; 
    });
     
    $.tools.validator.fn("[minlength]", function(input, value) {
        var min = input.attr("minlength");
        
        return value.length >= min ? true : {     
            en: "Please provide at least " +min+ " character" + (min > 1 ? "s" : "")
        };
    });
     
    $.tools.validator.localizeFn("[type=time]", {
        en: 'Please supply a valid time'
    });
     
    /**
     * setup the validators
     */
    $(".form").validator({ 
        position: 'bottom left', 
        offset: [5, 0],
        messageClass:'form-error',
        message: '<div><em/></div>' // em element is the arrow
    });

    if ($(sortableName).sortable) {
        $(sortableName).sortable({
            cursor: 'move',
            revert: 500,
            opacity: 0.7,
            appendTo: 'body',
            handle: 'header',
            items: '.widget-container[draggable=true]',
            placeholder: 'widget-placeholder grid_2',
            forcePlaceholderSize: true,
            start: function(event, ui) {
                ui.item.addClass('start-drag');
            },
            stop: function(event, ui) {
                ui.item.removeClass('start-drag');
            },
            update: function(event, ui) {
                if ($.cookie) {
                    $.cookie(sortableCookie, $(this).sortable("toArray"), { expires: sortableCookieExpiry, path: "/" });
                }
            }
        }).disableSelection();
    }

    /**
     * restore the order of sortable widgets
     */
    if ($.cookie) {
        restoreOrder(sortableName, sortableCookie);
    }

    /**
     * Widget Drag and Drop
     */
    $('.widget-container .widget')
    .hover(
        function(){
            if (!$(this).parent().hasClass('start-drag')) {
                $(this).find('header').animate({height : 30}).parents('.widget-container').siblings(':not(.start-drag)').find('header').animate({height: 4});
            }
            return false;
        },
        function() {
            if (!$(this).parent().hasClass('start-drag')) {
                $(this).find('header').animate({height : 4})
            }
            return false;
        }
    )
    .find('.widget-close').click(function(){
        $(this).parents('.widget-container').fadeOut(function(){$(this).remove();});
        // perhaps call an ajax function to remember the widget is closed
        // $.ajax({url: "somescript.php", data: {widgetId: $(this).parents('.widget-container').attr('id')});
        return false;
    })
   .parents('.widget-container').each(function(){
        $(this).css({height: $(this).find('.widget').height()+30}, "slow");
    });

    /**
     * Widget overlays
     */
    $(".widget.has-details > section").each(function(){
        var section = this;
        $(section).append('<a class="rollover" />')
            .find('.rollover')
            .attr('rel', $(section).prev().find('h2 > a').attr('rel'))
            .hover(
                function(){$(this).animate({opacity: 0.6});},
                function(){$(this).animate({opacity: 0});}
            ).parents('.widget').find('.rollover[rel]').overlay({
                effect: $.browser.msie? 'default' : 'drop',
                top: 'center',
                mask: {
                    color: '#000',
                    loadSpeed: 200,
                    opacity: 0.5
                }
            });
    });


    /**
     * Table Sorting, Row Selection and Pagination
     */
    if ($.paginate) {
        $("table.paginate").paginate({rows: 10, buttonClass: 'button-blue'});
    }
    if ($.tablesort) {
        $("table.tablesort").tablesort();
    }
    if ($.selectable) {
        $("table.selectable").selectable({
            onSelect: function(row) {
                // do something
            },
            onDeselect: function(row) {
                // do something
            }
        });
    }

    /**
     * Setup details viewing for table
     */
    $('a.view-details').each(function(){
        var a = $(this);
        $(this).next().overlay({
            effect: $.browser.msie? 'default' : 'drop',
            top: '0px',
            mask: {
                color: '#000',
                loadSpeed: 200,
                opacity: 0.5
            },
            onClose: function() {
                this.getOverlay().appendTo(a.parent());
            }
        });
    }).click(function(){
        $(this).next().appendTo('body').overlay().load();
        return false;
    });

});

/**
 * Restores the sortable order from a cookie
 */
function restoreOrder(sortable, cookieName) {
    var list = $(sortable);
    if (!list) return;

    // fetch the saved cookie
    var cookie = $.cookie(cookieName);
    if (!cookie) return;

    // create array from cookie
    var IDs = cookie.split(",");

    // fetch current order
    var items = list.sortable("toArray");

    // create associative array from current order
    var current = [];
    for ( var v=0; v < items.length; v++ ){
        current[items[v]] = items[v];
    }

    for (var i = 0, n = IDs.length; i < n; i++) {
        // item id from saved order
        var itemID = IDs[i];

        if (itemID in current) {
            // select the item according to the saved order and reappend it to the list
            $(sortable).append($(sortable).children("#" + itemID));
        }
    }
}

$("a[rel='group_url_check']").click(function(){

var url = $("a[rel='group_url_check']").attr('href')+$("input#group_url").val();

$.get(url,function(data){
	alert(data);
});

	return false;
})
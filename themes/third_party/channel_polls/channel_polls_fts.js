// ********************************************************************************* //
var ChannelPolls = ChannelPolls ? ChannelPolls : new Object();
ChannelPolls.prototype = {}; // Get Outline Going
//********************************************************************************* //

$(document).ready(function() {
	
	ChannelPolls.Field = $('#ft_channel_polls');
	
	ChannelPolls.ActivateColorPicker();

});

//********************************************************************************* //

ChannelPolls.ActivateColorPicker = function(){
	
	ChannelPolls.Field.find('.colorbutton').each(function(index, elem){
		
		$(elem).ColorPicker({
			color: $(elem).attr('rel'),
			onShow: function (colpkr) {
				$(colpkr).fadeIn(500);
				return false;
			},
			onHide: function (colpkr) {
				$(colpkr).fadeOut(500);
				return false;
			},
			onChange: function (hsb, hex, rgb) {
				$(elem).css('backgroundColor', '#' + hex);
				$(elem).siblings('.cpicker').val(hex);
			}

		});
		
	});
	
};
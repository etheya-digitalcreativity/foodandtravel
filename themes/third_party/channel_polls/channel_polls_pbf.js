// ********************************************************************************* //
var ChannelPolls = ChannelPolls ? ChannelPolls : new Object();
ChannelPolls.prototype = {}; // Get Outline Going
ChannelPolls.PollData = {};
//********************************************************************************* //

$(document).ready(function() {
	
	jQuery.getScript(ChannelPolls.THEME_URL+'jquery-ui-timepicker-addon.js', function(){
		// Activate DateTimePicker
		ChannelPolls.Fields.find('.datetimepicker').datetimepicker({dateFormat:'yy-mm-dd',timeFormat:'hh:mm'});
	});
	
	ChannelPolls.Fields = $('.CPField');
	ChannelPolls.Fields.find('.PollAnswers tbody').sortable({axis: 'y', cursor: 'move', opacity: 0.6, handle: '.MoveAnswer', update:ChannelPolls.SyncOrderNumbers});
	ChannelPolls.Fields.find('.AddAnswer').live('click', ChannelPolls.AddAnswer);
	ChannelPolls.Fields.find('.DeleteAnswer').live('click', ChannelPolls.DeleteAnswer);
	ChannelPolls.Fields.tabs({ event: 'click' });
	
	ChannelPolls.Fields.each(function(){
		var CPData = JSON.parse(jQuery(this).find('.CP_Data').val());
		CPData.default_colors = CPData.default_colors.split('|');
		ChannelPolls.PollData[CPData.field_id] = CPData;
	});
	
	ChannelPolls.SyncOrderNumbers();
	ChannelPolls.ActivateColorPicker();
	
	
});

//********************************************************************************* //

ChannelPolls.AddAnswer = function(Event){
	
	var Parent = $(Event.target).closest('.CPField');	
	var Cloned = Parent.find('.PollAnswers tbody tr:last').clone();
	Cloned.find('input').val('');
	
	Parent.find('.PollAnswers tbody').append(Cloned);
	
	ChannelPolls.SyncOrderNumbers();
	ChannelPolls.ActivateColorPicker();
	
	return false;
};

//********************************************************************************* //

ChannelPolls.DeleteAnswer = function(Event){
	
	var AnswerID = $(Event.target).attr('rel');
	
	if ( $(Event.target).closest('tbody').find('tr').length == 2 )
	{
		alert('Minimum Two Answers Needed');
		return false;
	}
	
	confirm_delete = confirm('Are you sure you want to delete this answer?');	
	if (confirm_delete == false) return false;
	
	$.post(ChannelPolls.AJAX_URL, {ajax_method: 'delete_answer', answer_id:AnswerID});
	
	$(Event.target).closest('tr').hide('slow', function(){ $(this).remove() });
	
	return false;
};

//********************************************************************************* //

ChannelPolls.SyncOrderNumbers = function(){
	
	// Loop over all Polls Fields
	ChannelPolls.Fields.each(function(index, CP_FIELD){
		
		// What field_id?
		var FIELDID = $(CP_FIELD).attr('rel');
		
		// Loop over all answer TR's
		$(CP_FIELD).find('.PollAnswers tbody tr').each(function(AnswerIndex, AnswerTR){
			
			jQuery(AnswerTR).find('input, textarea, select').each(function(){
				attr = jQuery(this).attr('name').replace(/\[answers\]\[.*?\]/, '[answers][' + (AnswerIndex+1) + ']');
				jQuery(this).attr('name', attr);
			});
			
			// Check for color
			if (jQuery(AnswerTR).find('input.cpicker').val() == false){
				if (typeof ChannelPolls.PollData[FIELDID].default_colors[AnswerIndex] != 'undefined'){
					jQuery(AnswerTR).find('input.cpicker').val(ChannelPolls.PollData[FIELDID].default_colors[AnswerIndex]);
					jQuery(AnswerTR).find('.colorbutton').css('background-color', '#'+ChannelPolls.PollData[FIELDID].default_colors[AnswerIndex]);
				}
				else {
					jQuery(AnswerTR).find('input.cpicker').val('');
					jQuery(AnswerTR).find('.colorbutton').css('background-color', '');
				}
			}
		});	
		
		$(CP_FIELD).find('.PollAnswers tbody tr').removeClass('odd');
		$(CP_FIELD).find('.PollAnswers tbody tr:odd').addClass('odd');
	});

		
};

//********************************************************************************* //

ChannelPolls.ActivateColorPicker = function(){
	
	ChannelPolls.Fields.find('.colorbutton').each(function(index, elem){
		
		$(elem).ColorPicker({
			color: $(elem).attr('rel'),
			onShow: function (colpkr) {
				$(colpkr).fadeIn(500);
				return false;
			},
			onHide: function (colpkr) {
				$(colpkr).fadeOut(500);
				return false;
			},
			onChange: function (hsb, hex, rgb) {
				$(elem).css('backgroundColor', '#' + hex);
				$(elem).siblings('.cpicker').val(hex);
			}

		});
		
	});
	
};
jQuery(function($) {

	var nl_map = function(existing) {

		var zoom, default_zoom;
		var is_select = false;
		
		var field_prefix = NLGEO.field_prefix;
		
		if (NLGEO.zoom_field != 0)
		{
			zoom = $("#"+field_prefix+NLGEO.zoom_field);
			if (zoom.length == 0)
			{
				zoom = $("select[name='"+field_prefix+NLGEO.zoom_field+"']");
				if (zoom.length > 0) is_select = true;
			}
		}
		
		// set default zoom
		if (NLGEO.zoom_field != 0 && zoom.val().length > 0)
		{
			if ( ! existing && is_select)
			{
				default_zoom = NLGEO.default_zoom;
				zoom.val(default_zoom);
			}else
			{
				default_zoom = parseInt(zoom.val());
			}
		}else
		{
			default_zoom = NLGEO.default_zoom;
		}
						
		// map options
	    var map_options = {
	      zoom: default_zoom,
	      mapTypeId: google.maps.MapTypeId.ROADMAP,
		  scrollwheel: false
	    }

		// geocoder
		var geocoder = new google.maps.Geocoder();
				
		// google map
		if (NLGEO.tag_mode == 'manual' || NLGEO.existing_entry == 1)
		{
			var map = new google.maps.Map(document.getElementById("geo-map-canvas"), map_options);		
			// fix for showing google map from initial hidden element
			google.maps.event.trigger(map, 'resize');			
		}
		
	    geocoder.geocode( { 'address': nl_geo_parse_address()}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				
				var current_point;
				// get lat and lng fields
				var current_lat = $("#"+field_prefix+NLGEO.lat_field);
				var current_lng = $("#"+field_prefix+NLGEO.lng_field);
				
				// get the point
				if ( ! NLGEO.override && current_lat.val().length > 0 && current_lng.val().length > 0)
				{
					current_point = new google.maps.LatLng(current_lat.val(), current_lng.val());
				}else
				{
					current_point = results[0].geometry.location;				
					// set lat/lng values
					current_lat.val(current_point.lat().toFixed(5));
					current_lng.val(current_point.lng().toFixed(5));
				}
				
				if (! existing )
				{	
					// update message
					$("#geo-messages").removeClass("geo-error").addClass("geo-success").html(NLGEO.msg_geo_location + "<em> " + nl_geo_parse_address() + "</em> " + NLGEO.msg_geo_success);							
				}
				
				if (NLGEO.tag_mode == 'manual' || NLGEO.existing_entry == 1)
				{							
					// set center of map
					map.setCenter(current_point);
				
					// add marker
					var marker = new google.maps.Marker({
					    map: map, 
					    position: current_point,
						draggable: true
					});				
				
					// add marker dragend listener
			        google.maps.event.addListener(marker, 'dragend', function() {
						var point = marker.getPosition();
						$("#"+field_prefix+NLGEO.lat_field).val(point.lat().toFixed(5));
						$("#"+field_prefix+NLGEO.lng_field).val(point.lng().toFixed(5));
						$("#geo-messages").removeClass("geo-error").addClass("geo-success").html(NLGEO.msg_geo_location + " " + NLGEO.msg_geo_success);
			        });
			
					// add zoom changed listener
					if (NLGEO.zoom_field != 0)
					{	
						zoom.val(map.getZoom());
					
						google.maps.event.addListener(map, 'zoom_changed', function() {
						    zoom.val(map.getZoom());
						});				
					}
				}
			} else {
			  $("#geo-messages").removeClass("geo-success").addClass("geo-error").html(NLGEO.msg_geo_error + status);
			}
			
			if (NLGEO.tag_mode == 'auto' && NLGEO.auto_status == 1)
			{
				if (NLGEO.is_cp)
				{
					$("form"+NLGEO.form_hook+" #submit_button").click();
				}else
				{
					$("form"+NLGEO.form_hook).trigger("submit");
				}
			}
	    });
		
	}
				
	$("#geo-tag").bind("click", function(e) {
		e.preventDefault();
		$('#geo-map-canvas').show();
		NLGEO.override = true;
		nl_map(false);
	});
	
	if (NLGEO.tag_mode == 'auto')
	{
		$(NLGEO.form_hook).submit(function(e) {
			var address_diff = ($("#nlgeo-address").val() == nl_geo_parse_address()) ? false : true;
			if (address_diff)
			{
				if (NLGEO.auto_status == 0)
				{
					NLGEO.auto_status = 1;
					NLGEO.override = true;
					e.preventDefault();
					$(this).unbind('submit');
					nl_map(false);				
				}				
			}	
		});
		if (NLGEO.existing_entry == 1)
		{
			// populate hidden address element for comparing later
			$("#nlgeo-address").val(nl_geo_parse_address());			
		}
	}

	if (NLGEO.existing_entry == 1 && NLGEO.lat_field != 0 && NLGEO.lng_field != 0)
	{
		nl_map(true);
	}
	
	var shell = $(".publish_geotagger").parent();
	if (shell.hasClass("main_tab"))
	{
		var shell_id = shell.attr("id");
		if (shell_id != "publish")
		{
			// add click event to resize map
			$(".menu_"+shell_id).bind("click", function(e) {
				if (NLGEO.existing_entry)
				{
					nl_map(NLGEO.existing_entry);
					$(this).unbind("click");					
				}
			})
		}
	}

});

var nl_geo_parse_address = function()
{
	var field_values = new Array();
	var field_prefix = NLGEO.field_prefix;

	// get the field values
	var addr_field = (NLGEO.address_field != 0) ? $("#"+field_prefix+NLGEO.address_field).val() : false;
	if ( ! addr_field) {
		addr_field =  $("select[name='"+field_prefix+NLGEO.address_field+"']").val();
		if (addr_field === undefined) {
			addr_field = false;
		}
	}else
	{
		addr_field = addr_field.replace(/(\r\n|\n|\r)/gm,",");
	}				

	var city_field = (NLGEO.city_field != 0) ? $("#"+field_prefix+NLGEO.city_field).val() : false;					
	if ( ! city_field) {
		city_field =  $("select[name='"+field_prefix+NLGEO.city_field+"']").val();					
		if (city_field === undefined) {
			city_field = false;
		}
	}

	var st_field = (NLGEO.state_field != 0) ? $("#"+field_prefix+NLGEO.state_field).val() : false;				
	if ( ! st_field) {
		st_field =  $("select[name='"+field_prefix+NLGEO.state_field+"']").val();
		if (st_field === undefined) {
			st_field = false;
		}
	}

	var zip_field = (NLGEO.zip_field != 0) ?  $("#"+field_prefix+NLGEO.zip_field).val() : false;
		
	field_values.push(addr_field);
	field_values.push(city_field);
	field_values.push(st_field);
	field_values.push(zip_field);
	
	var address = "";
	
	var field_val_length = field_values.length;
	
	for(var i=0;i<field_val_length;i++)
	{
		val = field_values[i];
		if (val)
		{
			address += val + ",";				
		}
	}
	var last_char = address.substr(address.length - 1, address.length);
	if (last_char == ',')
	{
		address = address.substr(0, address.length -1);
	}
	return address;
}